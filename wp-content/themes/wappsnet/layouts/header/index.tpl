<head>
    <!--app statistics settings-->
    <meta name="yandex-verification" content="2376a6f240de4d6c"/>

    <!--app seo settings-->
    <title>{$seo_data.title}</title>
    <meta charset="{$seo_data.chars}"/>
    <meta property="og:locale" content="ru_RU"/>
    <meta property="og:type" content="article"/>
    <meta property="og:title" content="{$seo_data.title}">
    <meta property="og:url" content="{$seo_data.link}">
    <meta property="og:image" content="{$seo_data.image}">
    <meta property="og:description" content="{$seo_data.text}">
    <meta name="description" content="{$seo_data.desc}">

    <!--app view settings-->
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, minimal-ui">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">

    {if $widgets}
        <meta name="mix-search/config/environment" content="%7B%22modulePrefix%22%3A%22mix-search%22%2C%22environment%22%3A%22production%22%2C%22rootURL%22%3A%22/%22%2C%22rootElement%22%3A%22%23mix-search%22%2C%22locationType%22%3A%22none%22%2C%22EmberENV%22%3A%7B%22FEATURES%22%3A%7B%7D%2C%22EXTEND_PROTOTYPES%22%3A%7B%22Date%22%3Afalse%7D%7D%2C%22APP%22%3A%7B%22name%22%3A%22mix-search%22%2C%22version%22%3A%220.0.0+63e0f8f7%22%7D%2C%22exportApplicationGlobal%22%3Afalse%7D" />
        <link rel="stylesheet" href="{$build_link}widgets/assets/vendor.css"/>
        <link rel="stylesheet" href="{$build_link}widgets/assets/mix-search.css"/>
    {/if}

    <!--app scripts links-->
    <link rel="stylesheet" href="{$build_link}wappsnet/app.min.css'"/>

    <!--app additional settings-->
</head>

<body class="{$body_class}">