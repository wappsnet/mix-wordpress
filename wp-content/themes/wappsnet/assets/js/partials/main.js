(function ($, PubSub, Materialize, noUiSlider, stickyScroll) {
    $(document).ready(function(e) {
        PubSub.publish('document.ready', e);
    });

    $(window).load(function (e) {
        PubSub.publish('window.load',e);
    });

    $(window).resize(function(e) {
        PubSub.publish('window.resize',e);
    });

    $(window).scroll(function(e) {
        PubSub.publish('window.scroll',e);
    });
}( jQuery, PubSub, Materialize, noUiSlider, stickyScroll));