PubSub.subscribe('document.ready', function() {
    $("#side-menu-toggle").sideNav({
        edge: 'left',
        menuWidth: 230,
        closeOnClick: true
    });
});