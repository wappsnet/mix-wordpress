<div class="module-popular">
    <div class="app-fill">
        <div class="popular-wrapper">
            <h3 class="popular-title">
                {$lang.subscribe_title}
            </h3>
            <p class="popular-header">
                {$lang.subscribe_description}
            </p>
            <div class="popular-items">
                <div class="app-row">
                    {foreach from=$items item=item}
                        <div class="app-column large-3 middle-6 small-12">
                            <div class="popular-item">
                                {$item}
                            </div>
                        </div>
                    {/foreach}
                </div>
            </div>
        </div>
    </div>
</div>