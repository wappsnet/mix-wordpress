<?php
namespace Modules;

use Wappsnet\Core\Module;
use Wappsnet\Core\Render;

class Popular extends Module
{
    protected function setData() {
        $posts = get_posts([
            "numberposts" => 8,
            "fields" => "ids"
        ]);

        $items = [];

        foreach ($posts as $postId) {
            $items[] = Render::get_plugin('Post', [
               'id' => $postId
            ]);
        }

        $this->data['items'] = $items;
    }
}