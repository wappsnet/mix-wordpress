<?php
namespace Modules;

use Wappsnet\Core\Module;
use Wappsnet\Core\Parser;
use Wappsnet\Core\Render;

class Service extends Module
{
    protected function setData() {
        global $post;


        $this->data['post'] = $post;
        $this->data['image'] = get_the_post_thumbnail_url($post->ID);
        $this->data['service_search_link'] = get_field('service_search_link', $post->ID);
    }
}