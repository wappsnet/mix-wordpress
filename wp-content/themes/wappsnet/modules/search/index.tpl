<div class="module-search">
    <div class="primary-content"
         style="background-image: url('{$primaryImage.url}')">
        <div class="app-fill">
            <h1 class="primary-title">{$primaryTitle}</h1>
            <h2 class="secondary-title">{$secondaryTitle}</h2>
            <div id="mix-search"
                 class="search-widget">
            </div>
        </div>
    </div>
</div>