<div class="module-subscribe">
    <h3 class="subscribe-title">
        {$lang.subscribe_title}
    </h3>
    <p class="subscribe-header">
        {$lang.subscribe_description}
    </p>
    <div class="subscribe-wrapper">
        <div class="subscribe-block">
            <input type="email"
                   data-type="email"
                   name="subscribe_email"
                   id="subscribe_email"
                   class="mix-input subscribe-input"
                   placeholder="{$lang.email_placeholder}"/>
            <button id="subscribe-submit"
                    class="waves-effect waves-light btn mix-button subscribe-button">
                {$lang.subscribe}
            </button>
        </div>
    </div>
    <p class="subscribe-footer">
        {$lang.subscribe_notice}
    </p>
</div>