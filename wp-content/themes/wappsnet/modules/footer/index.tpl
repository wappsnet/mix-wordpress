<div class="footer-module">
    <div class="footer-social">
        <div class="app-fill">
            <div class="footer-social-wrapper">
                {$socialPart}
            </div>
        </div>
    </div>

    <div class="footer-menu">
        <div class="app-fill">
            <div class="footer-menu-wrapper">
                {$navigationPart}
            </div>
        </div>
    </div>

    <div class="footer-apps">
        <div class="app-fill">
            <div class="footer-apps-wrapper">
                {$mobilePart}
            </div>
        </div>
    </div>

    <div class="footer-rights">
        <div class="app-fill">
            <div class="footer-rights-wrapper">
                {$rightsPart}
            </div>
        </div>
    </div>
</div>