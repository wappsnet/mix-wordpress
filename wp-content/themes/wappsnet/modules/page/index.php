<?php
namespace Modules;

use Wappsnet\Core\Module;

class Page extends Module
{
    protected function setData() {
        global $post;


        $this->data['page'] = $post;
        $this->data['image'] = get_the_post_thumbnail_url($post->ID);
    }
}