<?php
/* Smarty version 3.1.30, created on 2018-07-23 23:44:39
  from "C:\xampp\htdocs\shakti\wp-content\themes\wappsnet\modules\footer\index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b5668675ca109_08313869',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '539b84f26e3735c049337fd601da4aaca8a9f694' => 
    array (
      0 => 'C:\\xampp\\htdocs\\shakti\\wp-content\\themes\\wappsnet\\modules\\footer\\index.tpl',
      1 => 1530131386,
      2 => 'file',
    ),
  ),
  'cache_lifetime' => 86400,
),true)) {
function content_5b5668675ca109_08313869 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="footer-module">
    <div class="app-fill1200">
        <div class="app-row">
            <div class="app-column large-6 middle-12 small-12">
                <div class="footer-info">
                    <div class="footer-title">
                        СВЯЖИТЕСЬ С НАМИ
                    </div>
                    <div class="footer-content">
                        <p>Логотип является товарным знаком iHerb, Inc. *Дисклеймер: Продукты и заявления о них, представленные на этом сайте (или через него), не были оценены Управлением по контролю за продуктами и лекарственными препаратами США. Они не предназначены для диагностики, лечения и предотвращения болезней.</p>

                    </div>
                    <div class="footer-subscribe">
                        <div class="plugin-subscribe">
    <div class="subscribe-block">
        <div class="subscribe-section">
            <input type="email"
                   data-type="email"
                   name="subscribe_email"
                   id="subscribe_email"
                   class="subscribe-input"
                   placeholder="Емайл адрес"/>
        </div>
        <div class="subscribe-section">
            <button id="subscribe-submit"
                    class="waves-effect waves-light btn app-rel app-box subscribe-button">
                Подписаться
            </button>
        </div>
    </div>
</div>
                    </div>
                    <div class="footer-social">
                        <div class="plugin-social-icons">
            <a class="social-icon"
           style="background-color: #005397"
           href="">
            <span class="fa fa-facebook"></span>
        </a>
            <a class="social-icon"
           style="background-color: #dd9933"
           href="">
            <span class="fa fa-vk"></span>
        </a>
            <a class="social-icon"
           style="background-color: "
           href="">
            <span class="fa fa-ok"></span>
        </a>
            <a class="social-icon"
           style="background-color: #00aedd"
           href="">
            <span class="fa fa-twitter"></span>
        </a>
            <a class="social-icon"
           style="background-color: "
           href="">
            <span class="fa fa-facebook"></span>
        </a>
    
</div>
                    </div>
                </div>
            </div>

            <div class="app-column large-6 middle-12 small-12">
                <div class="footer-menu">
                    <div class="app-row">
                        <div class="app-column large-6 middle-12 small-12">
                            <div class="footer-menu-item">
                                <div class="plugin-navigation">
    <h4 class="navigation-title">
        О проекте
    </h4>

    <div class="navigation-childes">
                    <a class="navigation-child" href="http://localhost/shakti/%d0%bc%d0%b5%d1%80%d0%be%d0%bf%d1%80%d0%b8%d1%8f%d1%82%d0%b8%d1%8f/">
                О нас
            </a>
                    <a class="navigation-child" href="http://localhost/shakti/%d0%ba%d0%be%d0%bd%d1%82%d0%b0%d0%ba%d1%82%d1%8b/">
                Контакты
            </a>
                    <a class="navigation-child" href="http://localhost/shakti/%d0%ba%d0%be%d0%bd%d1%84%d0%b8%d0%b4%d0%b5%d0%bd%d1%86%d0%b8%d0%b0%d0%bb%d1%8c%d0%bd%d0%be%d1%81%d1%82%d1%8c/">
                Конфиденциальность
            </a>
                    <a class="navigation-child" href="http://localhost/shakti/%d0%bf%d1%80%d0%b0%d0%b2%d0%b8%d0%bb%d0%b0-%d0%b8%d1%81%d0%bf%d0%be%d0%bb%d1%8c%d0%b7%d0%be%d0%b2%d0%b0%d0%bd%d0%b8%d1%8f/">
                Правила использования
            </a>
                    <a class="navigation-child" href="http://localhost/shakti/archive/ioga/">
                Йога
            </a>
                    <a class="navigation-child" href="http://localhost/shakti/archive/raw%d0%b2%d0%b5%d0%b3%d0%b0%d0%bd-%d0%b2%d1%81%d1%82%d1%80%d0%b5%d1%87%d0%b8/">
                RawВеган встречи
            </a>
                    <a class="navigation-child" href="http://localhost/shakti/archive/%d0%b3%d0%b0%d0%bb%d0%b5%d1%80%d0%b5%d1%8f/">
                Галерея
            </a>
        
    </div>
</div>
                            </div>
                        </div>

                        <div class="app-column large-6 middle-12 small-12">
                            <div class="footer-menu-item">
                                <div class="plugin-navigation">
    <h4 class="navigation-title">
        Каталог
    </h4>

    <div class="navigation-childes">
                    <a class="navigation-child" href="http://localhost/shakti/category/catalog/shakty-food-%d0%bf%d1%80%d0%be%d0%b4%d1%83%d0%ba%d1%82%d1%8b/">
                Shakty-Food продукты
            </a>
                    <a class="navigation-child" href="http://localhost/shakti/category/catalog/dried-fruits/">
                Сушеные фрукты
            </a>
                    <a class="navigation-child" href="http://localhost/shakti/category/catalog/bads/">
                БАДы
            </a>
                    <a class="navigation-child" href="http://localhost/shakti/category/catalog/super-foods/">
                Суперфуды
            </a>
                    <a class="navigation-child" href="http://localhost/shakti/category/catalog/tea/">
                Чай
            </a>
                    <a class="navigation-child" href="http://localhost/shakti/category/catalog/creams/">
                Масла
            </a>
                    <a class="navigation-child" href="http://localhost/shakti/category/catalog/cosmetics/">
                Косметика
            </a>
        
    </div>
</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="app-row">
        <div class="footer-short-content">
            <p>eco-food® является зарегистрированным товарным знаком eco-food</p>

        </div>
    </div>

    <div class="app-row">
        <div class="footer-rights">
            eco-food.com © Copyright 1997-2018 iHerb Inc. All rights reserved.
        </div>
    </div>
</div><?php }
}
