<?php
/* Smarty version 3.1.30, created on 2018-06-27 20:50:04
  from "C:\xampp\htdocs\shakti\wp-content\themes\wappsnet\modules\archive\mobile.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b33f87c904570_88540439',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b267b792c7edab832c81c02c5dd85a1bbbd5b944' => 
    array (
      0 => 'C:\\xampp\\htdocs\\shakti\\wp-content\\themes\\wappsnet\\modules\\archive\\mobile.tpl',
      1 => 1530132599,
      2 => 'file',
    ),
  ),
  'cache_lifetime' => 86400,
),true)) {
function content_5b33f87c904570_88540439 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="module-archive">
    <div class="app-fill1200">
        <div class="archive-block-mobile">
            <div class="filters-container">
                <div class="filter-item"
                     id="archive-nav-toggle"
                     data-activates="archive-category-nav">
                    <span class="filter-item-icon"><i class="fa fa-bar-chart"></i></span>
                    <span class="filter-item-name">Категории</span>
                </div>
            </div>

            <div class="products-container"
                 id="products-container">
                
            </div>
        </div>
    </div>
</div>

<div id="archive-category-nav"
     class="side-nav app-side-nav filters-side-menu">
    <div class="app-side-logo">
        <img src="http://localhost/shakti/wp-content/uploads/2018/06/cropped-logo-16-1-1.png"/>
    </div>
    <div class="app-side-wrapper">
        <div class="filters-nav-title">
            <span class="search-tool-icon"><i class="fa fa-angle-right"></i></span>
            <span class="search-tool-name">Категории</span>
        </div>
        <div class="filters-filter-mobile">
            <div class="categories-list-wrapper">
                <ul class="categories-list"><li class="category-item"><a href="http://localhost/shakti/archive/ioga/"><span class="cat-name">Йога</span><span class="cat-count">(1)</span></a><ul class="categories-list"><li class="category-item active"><a href="http://localhost/shakti/archive/ioga/ioga-online/"><span class="cat-name">Онлайн йога</span><span class="cat-count">(1)</span></a></li></ul></li></ul>
            </div>
        </div>
    </div>
</div><?php }
}
