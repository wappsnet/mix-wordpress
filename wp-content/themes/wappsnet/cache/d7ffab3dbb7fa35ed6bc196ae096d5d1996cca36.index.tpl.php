<?php
/* Smarty version 3.1.30, created on 2018-07-30 01:38:19
  from "C:\xampp\htdocs\cordyceps1\wp-content\themes\wappsnet\modules\products\index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b5e6c0bd86c58_53148219',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '53e8109eb9ef777b0248050a6d6711e3c06658bc' => 
    array (
      0 => 'C:\\xampp\\htdocs\\cordyceps1\\wp-content\\themes\\wappsnet\\modules\\products\\index.tpl',
      1 => 1532914286,
      2 => 'file',
    ),
  ),
  'cache_lifetime' => 86400,
),true)) {
function content_5b5e6c0bd86c58_53148219 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="module-products">
    <div class="app-fill1200">
        <div class="products-container">
            <h1 class="products-title">
                Грибы кордиоцепса
            </h1>
            <div class="products-wrapper">
                                    <div class="product-item">
                        <div class="product-plugin">
    <a href="http://localhost/cordyceps1/products/%d0%b7%d0%be%d0%bb%d0%be%d1%82%d0%be%d0%b9-%d0%ba%d0%be%d1%80%d0%b4%d0%b8%d1%86%d0%b5%d0%bf%d1%81-3/"
       class="item-image">

        <div class="item-props">
                    </div>

        <div class="product-cover">
            <div class="product-image"
                 style="background-image: url('http://localhost/cordyceps1/wp-content/uploads/2018/07/front-2-2-1.jpg')">
            </div>
        </div>

        <h4 class="product-name">
            Золотой кордицепс
        </h4>
    </a>

    <div class="item-price">
        <div class="item-real-price">
                            <div class="price-block">
                    <span class="price-value">4 000</span>
                    <span class="price-key">RUB</span>
                </div>
            
                    </div>
    </div>

    <div class="item-tools">
        <div id="item-cart-35"
             data-animate-id="item-cart-35"
             data-item-id="35"
             class="simple-tools-item cart-tool item-tool">
            <i class="fa fa-shopping-cart"></i>
        </div>
    </div>
</div>
                    </div>
                                    <div class="product-item">
                        <div class="product-plugin">
    <a href="http://localhost/cordyceps1/products/%d0%b7%d0%be%d0%bb%d0%be%d1%82%d0%be%d0%b9-%d0%ba%d0%be%d1%80%d0%b4%d0%b8%d1%86%d0%b5%d0%bf%d1%81-2/"
       class="item-image">

        <div class="item-props">
                    </div>

        <div class="product-cover">
            <div class="product-image"
                 style="background-image: url('http://localhost/cordyceps1/wp-content/uploads/2018/07/front-2-2-1.jpg')">
            </div>
        </div>

        <h4 class="product-name">
            Серебряный кордицепс
        </h4>
    </a>

    <div class="item-price">
        <div class="item-real-price">
                            <div class="price-block">
                    <span class="price-value">4 500</span>
                    <span class="price-key">RUB</span>
                </div>
            
                    </div>
    </div>

    <div class="item-tools">
        <div id="item-cart-34"
             data-animate-id="item-cart-34"
             data-item-id="34"
             class="simple-tools-item cart-tool item-tool">
            <i class="fa fa-shopping-cart"></i>
        </div>
    </div>
</div>
                    </div>
                                    <div class="product-item">
                        <div class="product-plugin">
    <a href="http://localhost/cordyceps1/products/%d0%b7%d0%be%d0%bb%d0%be%d1%82%d0%be%d0%b9-%d0%ba%d0%be%d1%80%d0%b4%d0%b8%d1%86%d0%b5%d0%bf%d1%81/"
       class="item-image">

        <div class="item-props">
                    </div>

        <div class="product-cover">
            <div class="product-image"
                 style="background-image: url('http://localhost/cordyceps1/wp-content/uploads/2018/07/001.jpg')">
            </div>
        </div>

        <h4 class="product-name">
            Бронзовый кордицепс
        </h4>
    </a>

    <div class="item-price">
        <div class="item-real-price">
                            <div class="price-block">
                    <span class="price-value">5 000</span>
                    <span class="price-key">RUB</span>
                </div>
            
                    </div>
    </div>

    <div class="item-tools">
        <div id="item-cart-31"
             data-animate-id="item-cart-31"
             data-item-id="31"
             class="simple-tools-item cart-tool item-tool">
            <i class="fa fa-shopping-cart"></i>
        </div>
    </div>
</div>
                    </div>
                
            </div>
            <div class="products-content">
                <p>Кордицепс широко распространен в мире (ареал соответствует таковому насекомых-хозяев этого паразита). Тем не менее наибольшее видовое разнообразие род проявляет в тропических регионах. Особую известность некоторые представители рода получили в связи с влиянием на поведение насекомых, на которых паразитируют, наиболее известным примером является кордицепс однобокий, паразитирующий на муравьях.</p>
<p>Размножение происходит путём паразитирования на бабочках (точнее, гусеницах), мухах и муравьях. Споры, попадающие на волосистую поверхность насекомого, прорастают и внедряются в тело насекомого. Насекомое или личинка бабочки гибнет, и в теле развивается полноценные гифы мицелия кордицепса.</p>

            </div>
            <div class="products-pagination">
                <div class="plugin-pagination">
    
</div>
            </div>
        </div>
    </div>
</div><?php }
}
