<?php
/* Smarty version 3.1.30, created on 2018-07-21 20:34:26
  from "C:\xampp\htdocs\shakti\wp-content\themes\wappsnet\plugins\cabinet\index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b5398d269b8d4_40178939',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c796f9892d3df46ba1f48729e389bf8874974ac4' => 
    array (
      0 => 'C:\\xampp\\htdocs\\shakti\\wp-content\\themes\\wappsnet\\plugins\\cabinet\\index.tpl',
      1 => 1529891171,
      2 => 'file',
    ),
  ),
  'cache_lifetime' => 86400,
),true)) {
function content_5b5398d269b8d4_40178939 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="plugin-cabinet">
    <div class="user-info-desc">
        <div class="user-desc-item">
            <span class="user-desc-label">Дата регистрации</span>
            <span class="user-desc-value">2018-05-28 22:13:32</span>
        </div>
        <div class="user-desc-item">
            <span class="user-desc-label">Логин регистрации</span>
            <span class="user-desc-value">mike</span>
        </div>
    </div>

    <div class="auth-item">
        <div id="auth-info-messages"
             class="auth-info-messages">
        </div>

        <div class="auth-field">
            <input class="auth-input user_save"
                   id="user_save_first_name"
                   name="user_first"
                   data-type="name"
                   value="Mike"
                   placeholder="Имя"
                   type="text"/>
        </div>
        <div class="auth-field">
            <input class="auth-input user_save"
                   id="user_save_last_name"
                   name="user_last"
                   data-type="name"
                   value="Tevan"
                   placeholder="Фамилия"
                   type="text"/>
        </div>
        <div class="auth-field">
            <div class="auth-icon">
                <i class="fa fa-envelope"></i>
            </div>
            <input class="auth-input user_save"
                   id="user_save_email"
                   name="email"
                   data-type="email"
                   value="mike-tm555@mail.ru"
                   placeholder="Емайл адре"
                   type="email"/>
        </div>

        <div class="auth-field">
            <div class="auth-icon">
                <i class="fa fa-phone"></i>
            </div>
            <input class="auth-input user_save"
                   id="user_save_phone"
                   name="phone"
                   data-type="phone"
                   value="+1 83024148567"
                   placeholder="Телефон"
                   type="tel"/>
        </div>

        <input class="auth-input user_save"
               id="user_save_id"
               type="hidden"
               name="user_id"
               data-type="hidden"
               value="1"/>

        <div class="auth-footer">
            <button id="user-save"
                    class="auth-button waves-effect waves-light btn">
                Сохранить
            </button>
        </div>
    </div>
</div><?php }
}
