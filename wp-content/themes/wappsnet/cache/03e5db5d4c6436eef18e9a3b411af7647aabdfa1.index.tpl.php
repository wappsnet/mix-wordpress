<?php
/* Smarty version 3.1.30, created on 2018-07-12 21:08:31
  from "C:\xampp\htdocs\shakti\wp-content\themes\wappsnet\modules\singleproduct\index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b47c34f3366e4_86816554',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '3131aba5128e653d6cf15bf3285827268324cddd' => 
    array (
      0 => 'C:\\xampp\\htdocs\\shakti\\wp-content\\themes\\wappsnet\\modules\\singleproduct\\index.tpl',
      1 => 1530654142,
      2 => 'file',
    ),
  ),
  'cache_lifetime' => 86400,
),true)) {
function content_5b47c34f3366e4_86816554 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="module-single-product">
    <div class="app-fill1200">
        <div class="single-product-wrapper">
            <div class="app-row">
                <div class="app-column large-6 middle-6 small-12">
                    <div class="product-images">
                        <div class="plugin-scale-images">
            <div class="top-image materialboxed"
             style="background-image: url('http://localhost/shakti/wp-content/uploads/2018/07/6.jpg')">
        </div>
    
    </div>
                    </div>
                </div>

                <div class="app-column large-6 middle-6 small-12">
                    <div class="product-info">
                        <div class="single-product-header">
                            <h2 class="product-title">
                                Nature's Answer, Грибы рейши
                            </h2>
                        </div>

                        
                        
                        <div class="product-info-section product-new-price">
                            <span class="app-right-5">3400</span>
                            <span class="app-right-5">RUB</span>
                        </div>

                        <div class="product-info-section product-cart">
                            <input class="product-cart-count"
                                   value="1"
                                   min="1"
                                   max="100"
                                   id="product-cart-count"
                                   name="product_count"
                                   type="number"/>
                            <button id="product-cart-button"
                                    data-animate-id="product-cart-count"
                                    data-item-id="324"
                                    class="product-cart-button btn btn-flat waves-effect">
                                <span class="app-right-5">В корзину</span>
                                <span><i class="fa fa-shopping-cart"></i></span>
                            </button>
                        </div>

                        <div class="product-info-section product-category">
                            <div class="label">
                                Категория:
                            </div>
                            <a href=""
                               class="value">
                                Каталог
                            </a>
                        </div>

                        <div class="product-info-section product-brand">
                            <div class="label">
                                Бренд:
                            </div>
                            <div class="value">
                                Dragon Herbs
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="product-additional-info">
                <div class="tab-header">
                    Описание
                </div>
                <div class="tab-content">
                    <div class="product-description">
                        Ягоды Годжи Heaven Mountain имеют насыщенный вкус ,они сочные ,естественно сладкие и очень вкусные .
В отличие от других ягод ,представленных на рынке, Heaven Mountain слаще ,мягче и имеют меньше семян .
Уровень влажности у этих ягод выше ,чем у иных, что способствует их сочности и мягкости …
Именно эти ягоды растут в Экологически чистом районе, в горах Китая , в радиусе 100 км . отсутствует промышленность и какие либо заводы .
Вода ,которая насыщает эти ягоды - это вода чистого ледника с горных вершин .
Идеальные ягоды Годжи Heaven Mountain от "Dragon Herb» .

Ягоды Годжи Heaven Mountain имеют насыщенный вкус ,они сочные ,естественно сладкие и очень вкусные .
В отличие от других ягод ,представленных на рынке, Heaven Mountain слаще ,мягче и имеют меньше семян .
Уровень влажности у этих ягод выше ,чем у иных, что способствует их сочности и мягкости …
Именно эти ягоды растут в Экологически чистом районе, в горах Китая , в радиусе 100 км . отсутствует промышленность и какие либо заводы .
Вода ,которая насыщает эти ягоды - это вода чистого ледника с горных вершин .
Идеальные ягоды Годжи Heaven Mountain от "Dragon Herb» .
                    </div>
                </div>

                <div class="tab-header">
                    Характеристики
                </div>
                <div class="tab-content">
                    <div class="product-characters">
                                                    <div class="app-row character-item">
                                <div class="app-column large-4 middle-4 small-12">
                                    <div class="character-label">
                                        Срок действия
                                    </div>
                                </div>
                                <div class="app-column large-8 middle-8 small-12">
                                    <div class="character-value">
                                        октября 2019
                                    </div>
                                </div>
                            </div>
                                                    <div class="app-row character-item">
                                <div class="app-column large-4 middle-4 small-12">
                                    <div class="character-label">
                                        Вес
                                    </div>
                                </div>
                                <div class="app-column large-8 middle-8 small-12">
                                    <div class="character-value">
                                        650
                                    </div>
                                </div>
                            </div>
                                                    <div class="app-row character-item">
                                <div class="app-column large-4 middle-4 small-12">
                                    <div class="character-label">
                                        Код Товара
                                    </div>
                                </div>
                                <div class="app-column large-8 middle-8 small-12">
                                    <div class="character-value">
                                        SNS-01635
                                    </div>
                                </div>
                            </div>
                                                    <div class="app-row character-item">
                                <div class="app-column large-4 middle-4 small-12">
                                    <div class="character-label">
                                        Количество в Упаковке
                                    </div>
                                </div>
                                <div class="app-column large-8 middle-8 small-12">
                                    <div class="character-value">
                                        32 жидкая унция
                                    </div>
                                </div>
                            </div>
                        
                    </div>
                </div>

                <div class="tab-header">
                    Коментарии
                </div>
                <div class="tab-content">
                    <div class="product-comments">
                        <div class="comments-plugin">
    <div class="create-comment">
        <div class="auth-item">
            <div id="auth-info-messages"
                 class="auth-info-messages">
            </div>

            <div class="auth-field">
                <div class="auth-icon">
                    <i class="fa fa-user"></i>
                </div>
                <input class="auth-input user_comment"
                       id="user_comment_name"
                       name="name"
                       data-type="name"
                       placeholder="Имя"
                       type="text"/>
            </div>

            <div class="auth-field">
                <div class="auth-icon">
                    <i class="fa fa-envelope"></i>
                </div>
                <input class="auth-input user_comment"
                       id="user_comment_email"
                       name="email"
                       data-type="email"
                       placeholder="Емайл адре"
                       type="email"/>
            </div>

            <div class="auth-field auth-text">
                <div class="auth-icon">
                    <i class="fa fa-book"></i>
                </div>
                <textarea class="auth-input user_comment"
                          id="user_comment_text"
                          name="text"
                          data-type="text"
                          placeholder="Текст коментарии">
                </textarea>
            </div>

            <input class="auth-input user_comment"
                   id="user_comment_post_id"
                   type="hidden"
                   data-type="hidden"
                   value="324"
                   name="post_id">

            <div class="auth-footer">
                <button id="user-comment"
                        class="auth-button waves-effect waves-light btn">
                    Добавить комментарий
                </button>
            </div>
        </div>
    </div>

            <div class="app-rel app-box empty-comments">
            
        </div>
    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><?php }
}
