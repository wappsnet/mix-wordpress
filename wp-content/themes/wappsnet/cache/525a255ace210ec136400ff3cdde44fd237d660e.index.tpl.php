<?php
/* Smarty version 3.1.30, created on 2018-07-01 21:39:49
  from "C:\xampp\htdocs\shakti\wp-content\themes\wappsnet\modules\filter\index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b394a253457d0_64697680',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'a605d562e7a33028ee8dcc98014bedbcbea0bade' => 
    array (
      0 => 'C:\\xampp\\htdocs\\shakti\\wp-content\\themes\\wappsnet\\modules\\filter\\index.tpl',
      1 => 1497465706,
      2 => 'file',
    ),
  ),
  'cache_lifetime' => 86400,
),true)) {
function content_5b394a253457d0_64697680 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="module-filter">
    <div class="app-rel app-box filters-wrapper">

        <div class="app-rel app-box filter-block">
            <div class="app-rel app-box app-vector-title filter-title">
                <span>Цена</span>
            </div>
            <div class="app-rel app-box price-input-block">
                <label for="price-filter-from"
                       class="app-rel app-box price-filter-title">
                    <span class="price-label-text"></span>
                </label>
                <input id="price-filter-from"
                       class="app-rel app-box price-input"
                       type="number"
                       min="500"
                       max="100000"
                       step="50"
                       value="500"/>
                <div class="price-label-key">RUB</div>
            </div>
            <div class="app-rel app-box price-input-block">
                <label for="price-filter-to"
                       class="app-rel app-box price-filter-title">
                    <span class="price-label-text"></span>
                </label>
                <input id="price-filter-to"
                       class="app-rel app-box price-input"
                       type="number"
                       min="500"
                       max="100000"
                       step="50"
                       value="100000"/>
                <div class="price-label-key">RUB</div>
            </div>

            
            <input type="hidden" id="category-filter-id" value="25">
            
        </div>

                    <div class="app-rel app-box filter-block">
                <div class="app-rel app-box app-vector-title filter-title">
                    <span>Бренды</span>
                </div>
                <select id="select-brand"
                        name="brand"
                        title="Бренды"
                        data-type="brand"
                        data-show="true"
                        class="select-filter"
                        multiple="multiple">
                                            <option value="154">
                            Dragon Herbs
                        </option>
                    
                </select>
            </div>
        
    </div>
</div><?php }
}
