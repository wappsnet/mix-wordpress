<?php
/* Smarty version 3.1.30, created on 2018-07-02 00:59:49
  from "C:\xampp\htdocs\shakti\wp-content\themes\wappsnet\plugins\forgot\index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b397905397634_37819997',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '7e31c82904a9608e8f642572393902e56e3936ba' => 
    array (
      0 => 'C:\\xampp\\htdocs\\shakti\\wp-content\\themes\\wappsnet\\plugins\\forgot\\index.tpl',
      1 => 1529893003,
      2 => 'file',
    ),
  ),
  'cache_lifetime' => 86400,
),true)) {
function content_5b397905397634_37819997 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="plugin-forgot">
    <div class="auth-item">
        <div id="auth-info-messages"
             class="auth-info-messages">
        </div>

        <div class="auth-field">
            <div class="auth-icon">
                <i class="fa fa-key"></i>
            </div>
            <input class="auth-input user_forgot"
                   id="user_forgot_email"
                   name="email"
                   data-type="email"
                   placeholder="Емайл адре"
                   type="email"/>
        </div>
        <div class="auth-footer">
            <button id="user-forgot"
                    class="auth-button waves-effect waves-light btn">
                Вастановить
            </button>
        </div>
    </div>
</div><?php }
}
