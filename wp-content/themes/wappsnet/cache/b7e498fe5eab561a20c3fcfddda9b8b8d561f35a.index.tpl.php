<?php
/* Smarty version 3.1.30, created on 2018-12-25 23:43:39
  from "C:\xampp\htdocs\mix\wp-content\themes\wappsnet\modules\profile\index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5c22c0ab332ad0_11638186',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b574de5f2f44aae0ead1ad468322cdaf2e9df2cd' => 
    array (
      0 => 'C:\\xampp\\htdocs\\mix\\wp-content\\themes\\wappsnet\\modules\\profile\\index.tpl',
      1 => 1545777642,
      2 => 'file',
    ),
  ),
  'cache_lifetime' => 86400,
),true)) {
function content_5c22c0ab332ad0_11638186 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="module-profile">
    <div class="app-fill">
        <div class="profile-content">
                            <h1 class="profile-title">
                    Личные данные
                </h1>
            
                    </div>

        <div class="profile-plugins">
                            <div class="profile-plugin">
                    <div class="plugin-register">
    <div class="auth-item">
        <div id="auth-info-messages"
             class="auth-info-messages">
        </div>

        <div class="auth-field">
            <div class="auth-icon">
                <i class="fa fa-user"></i>
            </div>
            <input class="auth-input user_register"
                   id="user_register_email"
                   name="email"
                   data-type="email"
                   placeholder="Логин"
                   type="email"/>
        </div>
        <div class="auth-field">
            <div class="auth-icon">
                <i class="fa fa-key"></i>
            </div>
            <input class="auth-input user_register"
                   id="user_register_password"
                   name="password"
                   data-type="password"
                   placeholder="Пароль"
                   type="password"/>
        </div>
        <div class="auth-field">
            <input class="auth-input user_register"
                   id="user_register_first_name"
                   name="first_name"
                   data-type="name"
                   placeholder="Имя"
                   type="text"/>
        </div>
        <div class="auth-field">
            <input class="auth-input user_register"
                   id="user_register_last_name"
                   name="last_name"
                   data-type="name"
                   placeholder="Фамилия"
                   type="text"/>
        </div>
        <div class="auth-field">
            <div class="auth-icon">
                <i class="fa fa-phone"></i>
            </div>
            <input class="auth-input user_register"
                   id="user_register_phone"
                   name="phone"
                   data-type="phone"
                   placeholder="Тел: +7 985 999 99 99"
                   type="tel"/>
        </div>
        <div class="auth-footer">
            <a class="auth-button action-button waves-effect waves-light btn"
               data-action="user_register">
                Регистрация
            </a>
            <a class="auth-button waves-effect waves-light btn"
               href="http://localhost/mix/profile/%d0%b2%d0%be%d0%b9%d1%82%d0%b8/">
                Войти
            </a>
        </div>
    </div>
</div>
                </div>
            
        </div>
    </div>
</div><?php }
}
