<?php
/* Smarty version 3.1.30, created on 2018-07-12 21:14:15
  from "C:\xampp\htdocs\shakti\wp-content\themes\wappsnet\modules\sidebar\index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b47c4a7d4e2b2_80559960',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '701ea9e08c6f7846bda7b32d5f03fdd689101482' => 
    array (
      0 => 'C:\\xampp\\htdocs\\shakti\\wp-content\\themes\\wappsnet\\modules\\sidebar\\index.tpl',
      1 => 1530497056,
      2 => 'file',
    ),
  ),
  'cache_lifetime' => 86400,
),true)) {
function content_5b47c4a7d4e2b2_80559960 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="module-sidebar">
            <div class="sidebar-item">
            <div class="plugin-categories">
    <div class="categories-title">
        <span class="title-icon"><i class="fa fa-cogs"></i></span>
        <span class="title-name">Категории</span>
    </div>
    <div class="categories-wrapper">
        <ul class="categories-list"><li class="category-item active"><a href="http://localhost/shakti/category/catalog/"><span class="cat-name">Каталог</span><span class="cat-count">(8)</span></a><ul class="categories-list"><li class="category-item"><a href="http://localhost/shakti/category/catalog/shakty-food-%d0%bf%d1%80%d0%be%d0%b4%d1%83%d0%ba%d1%82%d1%8b/"><span class="cat-name">Shakty-Food продукты</span><span class="cat-count">(3)</span></a></li><li class="category-item"><a href="http://localhost/shakti/category/catalog/bads/"><span class="cat-name">БАДы</span><span class="cat-count">(2)</span></a></li><li class="category-item"><a href="http://localhost/shakti/category/catalog/cosmetics/"><span class="cat-name">Косметика</span><span class="cat-count">(5)</span></a></li><li class="category-item"><a href="http://localhost/shakti/category/catalog/creams/"><span class="cat-name">Масла</span><span class="cat-count">(2)</span></a></li><li class="category-item"><a href="http://localhost/shakti/category/catalog/news-items/"><span class="cat-name">Новинки</span><span class="cat-count">(2)</span></a></li><li class="category-item"><a href="http://localhost/shakti/category/catalog/bestsellers/"><span class="cat-name">Распродажа</span><span class="cat-count">(2)</span></a></li><li class="category-item"><a href="http://localhost/shakti/category/catalog/super-foods/"><span class="cat-name">Суперфуды</span><span class="cat-count">(3)</span></a></li><li class="category-item"><a href="http://localhost/shakti/category/catalog/dried-fruits/"><span class="cat-name">Сушеные фрукты</span><span class="cat-count">(2)</span></a></li><li class="category-item"><a href="http://localhost/shakti/category/catalog/tea/"><span class="cat-name">Чай</span><span class="cat-count">(2)</span></a></li></ul></li></ul>
    </div>
</div>
        </div>
            <div class="sidebar-item">
            <div class="plugin-filters">
    <div class="filters-title">
        <span class="title-icon"><i class="fa fa-cogs"></i></span>
        <span class="title-name">Фильтры</span>
    </div>
    <div class="filters-wrapper">
        <form action="http://localhost/shakti/category/catalog/" type="get">
            <div class="filter-block">
                <div class="filter-title">
                    <span>Цена</span>
                </div>
                <div class="price-input-block">
                    <label for="price-filter-from"
                           class="price-filter-title">
                        <span class="price-label-text">От:</span>
                    </label>
                    <input id="price-filter-from"
                           class="price-input"
                           type="number"
                           name="filter_price_min"
                           min=""
                           max=""
                           step="50"
                           value="500"/>
                    <div class="price-label-key">RUB</div>
                </div>
                <div class="price-input-block">
                    <label for="price-filter-to"
                           class="price-filter-title">
                        <span class="price-label-text">До:</span>
                    </label>
                    <input id="price-filter-to"
                           class="price-input"
                           type="number"
                           name="filter_price_max"
                           min=""
                           max=""
                           step="50"
                           value="100000"/>
                    <div class="price-label-key">RUB</div>
                </div>

                
                <input type="hidden"
                       name="filter_category"
                       id="category-filter-id"
                       value="25"/>
                
            </div>

                            <div class="filter-block">
                    <div class="filter-title">
                        <span>Бренды</span>
                    </div>
                                            <div class="filter-item-wrapper">
                            <label>
                                                                    <input type="checkbox"
                                           class="filled-in"
                                           name="brand[]"
                                           value="154"/>
                                                                <span>Dragon Herbs</span>
                            </label>
                        </div>
                                            <div class="filter-item-wrapper">
                            <label>
                                                                    <input type="checkbox"
                                           class="filled-in"
                                           name="brand[]"
                                           value="311"/>
                                                                <span>White Egret Personal Care</span>
                            </label>
                        </div>
                    
                </div>
            

            <div class="filters-footer">
                <button type="submit"
                        class="submit-filters btn btn-flat waves-effect">
                    Применить фильтры
                </button>

                            </div>
        </form>
    </div>
</div>
        </div>
    
</div><?php }
}
