<?php
/* Smarty version 3.1.30, created on 2018-06-26 18:21:28
  from "C:\xampp\htdocs\shakti\wp-content\themes\wappsnet\modules\product\index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b32842859af79_26938313',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '94fbecd20574e3eba2c10fd3529106d7368b6510' => 
    array (
      0 => 'C:\\xampp\\htdocs\\shakti\\wp-content\\themes\\wappsnet\\modules\\product\\index.tpl',
      1 => 1529901037,
      2 => 'file',
    ),
  ),
  'cache_lifetime' => 86400,
),true)) {
function content_5b32842859af79_26938313 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="module-product-single">
    <div class="app-fill1200">
        <div class="app-rel app-box single-item-block">
            <div class="app-rel app-box single-item-title">
                <h2 class="app-rel app-box app-center app-vector-title">
                    <span>Ягоды Годжи Heaven Mountain</span>
                </h2>
            </div>

            <div class="app-rel app-box single-item-header">
                <div class="app-rel app-box single-header-child">
                    <a href="http://localhost/shakti/brands/sadsadasdsad/"
                       class="app-rel app-box item-tool item-brand-icon">
                    </a>
                </div>
                <div class="app-rel app-box single-header-child">
                    <a href="http://localhost/shakti/category/catalog/shakty-food-%d0%bf%d1%80%d0%be%d0%b4%d1%83%d0%ba%d1%82%d1%8b/"
                       class="app-rel app-box item-tool item-selection-type">
                        Shakty-Food продукты
                    </a>
                </div>
                <div class="app-rel app-box single-header-child">
                    <div class="app-rel app-box item-tool">
                        <div data-item-id="194"
                             class="app-rel app-box like-tool ">
                                                            <span id="item-like-194" class="icon"><i class="fa fa-heart"></i></span>
                                                    </div>
                    </div>
                </div>
            </div>

            <div class="app-rel app-box single-present">
                                    <div class="app-rel app-box single-item-images">
                        <div class="app-rel app-box single-item-image"
                             style="background-image: url()">
                        </div>
                    </div>
                
                <div class="app-rel app-box fast-order-box">
                    <a class="waves-effect waves-light btn fast-order-button"
                       data-target="fast-order-modal">
                        <span class="icon"><i class="fa fa-hand-pointer-o"></i></span>
                        <span class="title"></span>
                    </a>
                </div>

                <div class="app-rel app-box single-share-box">
                    <div class="share-plugin">
    <div class="app-rel app-box share-block">
        <div class="pluso"
             data-background="transparent"
             data-options="medium,square,line,horizontal,counter,theme=04"
             data-services="vkontakte,facebook,odnoklassniki">
        </div>
    </div>
</div>
                </div>
            </div>

            <div class="app-rel app-box single-item-prices">

                
                                    <div class="app-rel app-box single-item-prices">
                        <div class="app-rel app-box single-not-enough">
                            <div class="sold-icon"></div>
                            <div class="sold-title"></div>
                        </div>
                    </div>
                            </div>

            <div class="app-rel app-box single-item-title">
                <h2 class="app-rel app-box app-center app-vector-title">
                    <span></span>
                </h2>
            </div>

            <div class="app-rel app-box single-item-info">
                <div class="app-rel app-box info-item">
                    <div class="app-rel app-box info-label">
                        <span class="info-label-name"></span>
                        <span class="info-label-icon">
                            <i class="fa fa-angle-right"></i>
                        </span>
                    </div>
                    <div class="app-rel app-box info-value">
                        <span class="info-value-item">
                            194
                        </span>
                    </div>
                </div>

                <div class="app-rel app-box info-item">
                    <div class="app-rel app-box info-label">
                        <span class="info-label-name"></span>
                        <span class="info-label-icon">
                            <i class="fa fa-angle-right"></i>
                        </span>
                    </div>
                    <div class="app-rel app-box info-value">
                        <a href="http://localhost/shakti/brands/sadsadasdsad/"
                           class="info-value-item">
                            Dragon Herbs
                        </a>
                    </div>
                </div>

                <div class="app-rel app-box info-item">
                    <div class="app-rel app-box info-label">
                        <span class="info-label-name">Категории</span>
                        <span class="info-label-icon">
                            <i class="fa fa-angle-right"></i>
                        </span>
                    </div>
                    <div class="app-rel app-box info-value">
                                                    <a href="http://localhost/shakti/category/catalog/shakty-food-%d0%bf%d1%80%d0%be%d0%b4%d1%83%d0%ba%d1%82%d1%8b/"
                               class="info-value-item">
                                Shakty-Food продукты
                            </a>
                                                    <a href="http://localhost/shakti/category/catalog/bads/"
                               class="info-value-item">
                                БАДы
                            </a>
                                                    <a href="http://localhost/shakti/category/catalog/"
                               class="info-value-item">
                                Каталог
                            </a>
                                                    <a href="http://localhost/shakti/category/catalog/cosmetics/"
                               class="info-value-item">
                                Косметика
                            </a>
                                                    <a href="http://localhost/shakti/category/catalog/creams/"
                               class="info-value-item">
                                Масла
                            </a>
                                                    <a href="http://localhost/shakti/category/news-items/"
                               class="info-value-item">
                                Новинки
                            </a>
                                                    <a href="http://localhost/shakti/category/bestsellers/"
                               class="info-value-item">
                                Распродажа
                            </a>
                                                    <a href="http://localhost/shakti/category/catalog/super-foods/"
                               class="info-value-item">
                                Суперфуды
                            </a>
                                                    <a href="http://localhost/shakti/category/catalog/dried-fruits/"
                               class="info-value-item">
                                Сушеные фрукты
                            </a>
                                                    <a href="http://localhost/shakti/category/catalog/tea/"
                               class="info-value-item">
                                Чай
                            </a>
                        
                    </div>
                </div>
            </div>


            <div class="app-rel app-box single-item-title">
                <h2 class="app-rel app-box app-center app-vector-title">
                    <span></span>
                </h2>
            </div>

            <div class="app-rel app-box single-item-characters">
                <div class="app-rel app-box item-tabs">
                    <ul class="app-rel app-box app-center tabs" id="single-item-tabs">
                        <li class="tab col s3"><a class="tab-link" href="#chars"></a></li>
                        <li class="tab col s3"><a class="tab-link" href="#desc"></a></li>
                    </ul>
                </div>

                <div class="app-rel app-box tab-content">
                    <div id="chars"
                         class="app-rel app-box tab-info">

                        

                    </div>
                    <div id="desc"
                         class="app-rel app-box tab-info">
                        <div class="app-rel app-box single-description">
                            Ягоды Годжи Heaven Mountain имеют насыщенный вкус ,они сочные ,естественно сладкие и очень вкусные .
В отличие от других ягод ,представленных на рынке, Heaven Mountain слаще ,мягче и имеют меньше семян .
Уровень влажности у этих ягод выше ,чем у иных, что способствует их сочности и мягкости …
Именно эти ягоды растут в Экологически чистом районе, в горах Китая , в радиусе 100 км . отсутствует промышленность и какие либо заводы .
Вода ,которая насыщает эти ягоды - это вода чистого ледника с горных вершин .
Идеальные ягоды Годжи Heaven Mountain от "Dragon Herb» .
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><?php }
}
