<?php
/* Smarty version 3.1.30, created on 2018-07-12 21:14:37
  from "C:\xampp\htdocs\shakti\wp-content\themes\wappsnet\modules\profile\mobile.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b47c4bdf293a0_59178072',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '5f64e826dd6d22aaaae3a530cafee2a64cd2be1e' => 
    array (
      0 => 'C:\\xampp\\htdocs\\shakti\\wp-content\\themes\\wappsnet\\modules\\profile\\mobile.tpl',
      1 => 1530492533,
      2 => 'file',
    ),
  ),
  'cache_lifetime' => 86400,
),true)) {
function content_5b47c4bdf293a0_59178072 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="module-profile">
    <div class="app-fill1200">
        <div class="profile-wrapper mobile-wrapper">
            <div class="profile-header-wrapper">
                <div class="profile-menu-toggle"
                     id="profile-menu-toggle"
                     data-activates="profile-menu-nav">
                    <span class="toggle-icon"><i class="fa fa-bars"></i></span>
                    <span class="toggle-name">Меню пользователя</span>
                </div>
            </div>
            <div class="profile-content-wrapper">
                <div class="profile-title">
                    Авторизация
                </div>
                                    <div class="profile-plugin-wrapper">
                        <div class="plugin-login">
    <div class="auth-item">
        <div id="auth-info-messages"
             class="auth-info-messages">
        </div>

        <div class="auth-field">
            <div class="auth-icon">
                <i class="fa fa-user"></i>
            </div>
            <input class="auth-input user_login"
                   id="user_login_email"
                   name="email"
                   data-type="email"
                   placeholder="Емайл адре"
                   type="email"/>
        </div>
        <div class="auth-field">
            <div class="auth-icon">
                <i class="fa fa-key"></i>
            </div>
            <input class="auth-input user_login"
                   id="user_login_password"
                   name="password"
                   data-type="password"
                   placeholder="Пароль"
                   type="password"/>
        </div>
        <div class="auth-footer">
            <button id="user-login"
                    class="auth-button waves-effect waves-light btn">
                Войти
            </button>
            <a href="http://localhost/shakti/profile/forgot"
               id="user-open-forgot"
               class="auth-button pink waves-effect waves-light btn">
                Забыли пароль?
            </a>
        </div>
    </div>
</div>
                    </div>
                
            </div>
        </div>
    </div>
</div>


<div id="profile-menu-nav"
     class="side-nav app-side-nav profile-side-menu">
    <div class="app-side-logo">
        <img src="http://localhost/shakti/wp-content/uploads/2018/06/cropped-logo-16-1-1.png"/>
    </div>
    <div class="app-side-wrapper">
        <div class="profile-menu">
                            <div class="profile-menu-item">
                    <a href="http://localhost/shakti/profile/likes/"
                       data-action=""
                       class="profile-menu-link ">
                        <span class="icon"><i class="fa fa-heart"></i></span>
                        <span class="title">Список нравится</span>
                    </a>
                </div>
                            <div class="profile-menu-item">
                    <a href="http://localhost/shakti/profile/basket/"
                       data-action=""
                       class="profile-menu-link ">
                        <span class="icon"><i class="fa fa-shopping-cart"></i></span>
                        <span class="title">Корзина покупок</span>
                    </a>
                </div>
                            <div class="profile-menu-item">
                    <a href="http://localhost/shakti/profile/cabinet/"
                       data-action=""
                       class="profile-menu-link ">
                        <span class="icon"><i class="fa fa-user-circle"></i></span>
                        <span class="title">Мои данные</span>
                    </a>
                </div>
                            <div class="profile-menu-item">
                    <a href="http://localhost/shakti/profile/%d0%b8%d0%b7%d0%bc%d0%b5%d0%bd%d0%b8%d1%82%d1%8c-%d0%bf%d0%b0%d1%80%d0%be%d0%bb%d1%8c/"
                       data-action=""
                       class="profile-menu-link ">
                        <span class="icon"><i class="fa fa-key"></i></span>
                        <span class="title">Изменить пароль</span>
                    </a>
                </div>
                            <div class="profile-menu-item">
                    <a href="http://localhost/shakti/profile/%d0%b8%d1%81%d1%82%d0%be%d1%80%d0%b8%d1%8f-%d0%b7%d0%b0%d0%ba%d0%b0%d0%b7%d0%be%d0%b2/"
                       data-action=""
                       class="profile-menu-link ">
                        <span class="icon"><i class="fa fa-history"></i></span>
                        <span class="title">История заказов</span>
                    </a>
                </div>
                            <div class="profile-menu-item">
                    <a href="http://localhost/shakti/profile/logout/"
                       data-action="user_logout"
                       class="profile-menu-link ">
                        <span class="icon"><i class="fa fa-sign-out"></i></span>
                        <span class="title">Выйти</span>
                    </a>
                </div>
            
        </div>
    </div>
</div><?php }
}
