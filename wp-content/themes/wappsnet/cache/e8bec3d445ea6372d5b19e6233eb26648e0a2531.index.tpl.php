<?php
/* Smarty version 3.1.30, created on 2018-06-10 22:31:54
  from "C:\xampp\htdocs\shakti\wp-content\themes\wappsnet\modules\napper\index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b1da6da5efa92_46831128',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '1177e0902f08460cc6f3eff54578686e6419535f' => 
    array (
      0 => 'C:\\xampp\\htdocs\\shakti\\wp-content\\themes\\wappsnet\\modules\\napper\\index.tpl',
      1 => 1497465658,
      2 => 'file',
    ),
  ),
  'cache_lifetime' => 86400,
),true)) {
function content_5b1da6da5efa92_46831128 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="napper-module">
    <div class="app-fill1200">
        <div class="app-rel app-box napper-wrapper">
            <div class="app-rel app-box napper-section napper-small-section napper-logo-section">
                <div class="app-rel app-box napper-section-wrapper">
                    <div class="app-rel app-box app-middle-center napper-section-module napper-logo-module">
                        <div class="module-logo">
    <div class="app-rel app-box logo-block">
        <a href=""
           class="app-rel app-box logo-black logo-button">
        </a>
    </div>
</div>
                    </div>
                </div>
            </div>

            <div class="app-rel app-box napper-section napper-small-section napper-badges-section">
                <div class="app-rel app-box napper-section-wrapper">
                    <div class="app-rel app-box app-middle-center napper-section-module napper-badges-module">
                        <div class="module-badges">
    <div class="app-rel app-box badges-items">
        
    </div>
</div>
                    </div>
                </div>
            </div>

            <div class="app-rel app-box napper-section napper-middle-section napper-search-section">
                <div class="app-rel app-box napper-section-wrapper">
                    <div class="app-rel app-box app-middle-center napper-section-module napper-search-module">
                        <div class="module-search">
    <div class="app-rel app-box search-block">
        <div id="global-search-box"
             class="app-rel app-box search-box">
            <label for="global-search"
                   class="app-abs app-box search-label">
                <i class="fa fa-search"></i>
            </label>
            <input id="global-search-input"
                   class="app-rel app-box search-input"
                   type="search"/>
            <div id="search-results-wrapper"
                 class="app-abs app-box search-results-wrapper">
                <div id="search-results-box"
                     class="app-rel app-box search-results-box">
                </div>
            </div>
        </div>
    </div>
</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><?php }
}
