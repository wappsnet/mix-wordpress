<?php
/* Smarty version 3.1.30, created on 2018-07-12 21:14:15
  from "C:\xampp\htdocs\shakti\wp-content\themes\wappsnet\plugins\filters\index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b47c4a7c68635_08056736',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '93b40b2c7609eed461a1fad9a78ff463b02fb9de' => 
    array (
      0 => 'C:\\xampp\\htdocs\\shakti\\wp-content\\themes\\wappsnet\\plugins\\filters\\index.tpl',
      1 => 1530778214,
      2 => 'file',
    ),
  ),
  'cache_lifetime' => 86400,
),true)) {
function content_5b47c4a7c68635_08056736 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="plugin-filters">
    <div class="filters-title">
        <span class="title-icon"><i class="fa fa-cogs"></i></span>
        <span class="title-name">Фильтры</span>
    </div>
    <div class="filters-wrapper">
        <form action="http://localhost/shakti/category/catalog/" type="get">
            <div class="filter-block">
                <div class="filter-title">
                    <span>Цена</span>
                </div>
                <div class="price-input-block">
                    <label for="price-filter-from"
                           class="price-filter-title">
                        <span class="price-label-text">От:</span>
                    </label>
                    <input id="price-filter-from"
                           class="price-input"
                           type="number"
                           name="filter_price_min"
                           min=""
                           max=""
                           step="50"
                           value="500"/>
                    <div class="price-label-key">RUB</div>
                </div>
                <div class="price-input-block">
                    <label for="price-filter-to"
                           class="price-filter-title">
                        <span class="price-label-text">До:</span>
                    </label>
                    <input id="price-filter-to"
                           class="price-input"
                           type="number"
                           name="filter_price_max"
                           min=""
                           max=""
                           step="50"
                           value="100000"/>
                    <div class="price-label-key">RUB</div>
                </div>

                
                <input type="hidden"
                       name="filter_category"
                       id="category-filter-id"
                       value="25"/>
                
            </div>

                            <div class="filter-block">
                    <div class="filter-title">
                        <span>Бренды</span>
                    </div>
                                            <div class="filter-item-wrapper">
                            <label>
                                                                    <input type="checkbox"
                                           class="filled-in"
                                           name="brand[]"
                                           value="154"/>
                                                                <span>Dragon Herbs</span>
                            </label>
                        </div>
                                            <div class="filter-item-wrapper">
                            <label>
                                                                    <input type="checkbox"
                                           class="filled-in"
                                           name="brand[]"
                                           value="311"/>
                                                                <span>White Egret Personal Care</span>
                            </label>
                        </div>
                    
                </div>
            

            <div class="filters-footer">
                <button type="submit"
                        class="submit-filters btn btn-flat waves-effect">
                    Применить фильтры
                </button>

                            </div>
        </form>
    </div>
</div><?php }
}
