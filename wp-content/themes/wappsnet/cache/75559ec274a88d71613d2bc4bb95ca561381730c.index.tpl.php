<?php
/* Smarty version 3.1.30, created on 2018-06-10 22:31:54
  from "C:\xampp\htdocs\shakti\wp-content\themes\wappsnet\modules\search\index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b1da6da41f3b3_55767049',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c7c7e4f6e439704975b1156f271b8d259a7532b9' => 
    array (
      0 => 'C:\\xampp\\htdocs\\shakti\\wp-content\\themes\\wappsnet\\modules\\search\\index.tpl',
      1 => 1497465730,
      2 => 'file',
    ),
  ),
  'cache_lifetime' => 86400,
),true)) {
function content_5b1da6da41f3b3_55767049 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="module-search">
    <div class="app-rel app-box search-block">
        <div id="global-search-box"
             class="app-rel app-box search-box">
            <label for="global-search"
                   class="app-abs app-box search-label">
                <i class="fa fa-search"></i>
            </label>
            <input id="global-search-input"
                   class="app-rel app-box search-input"
                   type="search"/>
            <div id="search-results-wrapper"
                 class="app-abs app-box search-results-wrapper">
                <div id="search-results-box"
                     class="app-rel app-box search-results-box">
                </div>
            </div>
        </div>
    </div>
</div><?php }
}
