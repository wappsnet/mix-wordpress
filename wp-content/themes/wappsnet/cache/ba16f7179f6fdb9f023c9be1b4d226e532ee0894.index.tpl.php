<?php
/* Smarty version 3.1.30, created on 2018-07-24 01:21:32
  from "C:\xampp\htdocs\cordyceps1\wp-content\themes\wappsnet\plugins\search\index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b567f1c7d8332_97287996',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '06f8df30852c02f0f61d0cf01e08522f55a54163' => 
    array (
      0 => 'C:\\xampp\\htdocs\\cordyceps1\\wp-content\\themes\\wappsnet\\plugins\\search\\index.tpl',
      1 => 1530793912,
      2 => 'file',
    ),
  ),
  'cache_lifetime' => 86400,
),true)) {
function content_5b567f1c7d8332_97287996 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div id="global-search-plugin"
     class="plugin-search">
    <input id="global-search"
           placeholder="Поиск"
           class="search-input"
           name="search"
           type="search"/>
    <div id="global-search-label"
         class="search-label">
        <i class="fa fa-search"></i>
    </div>
</div>

<div id="search-results-wrapper"
     class="search-results-wrapper">
    <div class="app-fill1200">
        <div class="search-results-container">
            <div class="search-results-header">
                <div class="results-title">
                    
                </div>
                <div id="close-search-results"
                     class="results-close">
                    <i class="fa fa-times"></i>
                </div>
            </div>
            <div id="search-results-box"
                 class="search-results-box">
            </div>
        </div>
    </div>
</div><?php }
}
