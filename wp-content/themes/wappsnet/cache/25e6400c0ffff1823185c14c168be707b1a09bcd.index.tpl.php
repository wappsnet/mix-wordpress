<?php
/* Smarty version 3.1.30, created on 2018-07-30 02:13:42
  from "C:\xampp\htdocs\cordyceps1\wp-content\themes\wappsnet\modules\advantages\index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b5e74562ea2b3_80927592',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '6523aeb64e0debe6cd0aa17f800ac5f6c50d6c57' => 
    array (
      0 => 'C:\\xampp\\htdocs\\cordyceps1\\wp-content\\themes\\wappsnet\\modules\\advantages\\index.tpl',
      1 => 1532736311,
      2 => 'file',
    ),
  ),
  'cache_lifetime' => 86400,
),true)) {
function content_5b5e74562ea2b3_80927592 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="module-advantages">
    <div class="app-fill1200">
        <div class="advantages-wrapper">
                            <div class="advantage-item">
                    <h2 class="advantage-title">Преимушества</h2>
                    <div class="advantage-content">
                        <ul>
<li>Надоело быть вялым каждый день и чувствовать себя слабым?</li>
<li>Устал тратить бешеные деньги на препараты, которые убивают твой организм и ослабляют иммунитет?</li>
<li>Хочешь быть бодрым и не знать проблем со здоровьем?</li>
<li>Открой для себя ворота в новую жизнь, наполненную яркими красками и позитивными эмоциями.</li>
<li>Начни сразу жить в высшей гильдии, получая наслаждение от состояния своего тела и разума.</li>
</ul>
<p>Самый лучший продукт в мире не имеющий аналогов – высший гриб Кордицепс.</p>

                    </div>
                                            <div class="plugin-buy-button">
    <a class="btn waves-effect buy-button"
       href="/products">
        Купить сейчас
    </a>
</div>
                                    </div>
                            <div class="advantage-item">
                    <h2 class="advantage-title">Лечебные свойства Кордицепса</h2>
                    <div class="advantage-content">
                        <ol>
<li>поднимает иммунитет</li>
<li>повышает выносливость организма</li>
<li>выводит токсины</li>
<li>натуральный афродизиак(кордицепс еще называют “гималайская виагра”)</li>
<li>поддерживает уровень сахара в норме</li>
<li>снижает уровень холестерина</li>
<li>нормализует обмен веществ</li>
<li>чистит сосуды</li>
<li>выводит тромбы</li>
<li>нормализует давление</li>
<li>останавливает размножение раковых клеток</li>
<li>работает как натуральный антибиотик при воспалениях</li>
<li>сокращает время заживления ран и переломов</li>
<li>убирает мужские и женские болезни</li>
<li>омолаживает организм</li>
<li>сокращает время сна и устрает сонливость</li>
<li>убирает аллергию</li>
<li>не имеет побочных действий</li>
<li>не имеет ограничений по возрасту</li>
<li>совместим со всем</li>
</ol>

                    </div>
                                    </div>
                            <div class="advantage-item">
                    <h2 class="advantage-title">Кто мы, что мыделаем</h2>
                    <div class="advantage-content">
                        <p>Кордицепс препятсвует возникновению всех основных заболеваний, т.к. работет на уровне клетки. Клетка получает необходимое ей питание, начинает правильно функционировать, что приводит к саморегуляции и очистке организма.</p>
<p>Мы на рынке с 2009 года и знаем, что такое настоящий Кордицепс. Мы самые крупные поставщики гриба Кордицепс на поссоветском пространстве! Довертесь нам!</p>
<p>В случае если Вас что то не устроит, мы вернем Вам деньги!</p>

                    </div>
                                    </div>
            
        </div>
    </div>
</div><?php }
}
