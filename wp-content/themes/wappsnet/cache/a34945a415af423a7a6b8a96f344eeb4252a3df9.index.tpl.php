<?php
/* Smarty version 3.1.30, created on 2018-06-25 02:55:20
  from "C:\xampp\htdocs\shakti\wp-content\themes\wappsnet\modules\commerce\index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b30599832cfd5_47353821',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '18a3fb5aad56ed398a81b6bb77cadfb3d135d2c7' => 
    array (
      0 => 'C:\\xampp\\htdocs\\shakti\\wp-content\\themes\\wappsnet\\modules\\commerce\\index.tpl',
      1 => 1529894625,
      2 => 'file',
    ),
  ),
  'cache_lifetime' => 86400,
),true)) {
function content_5b30599832cfd5_47353821 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="module-commerce">
    <div class="app-fill1200">
        <div class="products-block-app">
            <div class="filters-container">
                <div class="filter-item">
                    <div class="filter-item-title">
                        <span class="filter-item-icon"><i class="fa fa-angle-right"></i></span>
                        <span class="filter-item-name">Категории</span>
                    </div>
                    <div class="filter-item-content">
                        <div class="module-category">
    <div class="app-rel app-box products-menu-block">
        <ul class="app-rel app-box products-menu">
                            <li class="menu-item">
                    <a href="http://localhost/shakti/category/catalog/%d1%81%d1%83%d0%bf%d0%b5%d1%80%d1%84%d1%83%d0%b4%d1%8b/"
                       class="app-rel app-box menu-name has-childes ">
                        <span class="name">Суперфуды</span>
                        <span class="count">(2)</span>
                        <div class="menu-icon"
                             id="toggle-12">
                            <i class="fa fa-angle-right"></i>
                        </div>
                    </a>
                </li>
                            <li class="menu-item">
                    <a href="http://localhost/shakti/category/catalog/%d0%b1%d0%b0%d0%b4%d1%8b/"
                       class="app-rel app-box menu-name has-childes ">
                        <span class="name">БАДы</span>
                        <span class="count">(0)</span>
                        <div class="menu-icon"
                             id="toggle-13">
                            <i class="fa fa-angle-right"></i>
                        </div>
                    </a>
                </li>
                            <li class="menu-item">
                    <a href="http://localhost/shakti/category/catalog/%d1%81%d1%83%d1%88%d0%b5%d0%bd%d1%8b%d0%b5-%d1%84%d1%80%d1%83%d0%ba%d1%82%d1%8b/"
                       class="app-rel app-box menu-name has-childes ">
                        <span class="name">Сушеные фрукты</span>
                        <span class="count">(0)</span>
                        <div class="menu-icon"
                             id="toggle-14">
                            <i class="fa fa-angle-right"></i>
                        </div>
                    </a>
                </li>
                            <li class="menu-item">
                    <a href="http://localhost/shakti/category/catalog/%d1%87%d0%b0%d0%b9/"
                       class="app-rel app-box menu-name has-childes ">
                        <span class="name">Чай</span>
                        <span class="count">(0)</span>
                        <div class="menu-icon"
                             id="toggle-15">
                            <i class="fa fa-angle-right"></i>
                        </div>
                    </a>
                </li>
                            <li class="menu-item">
                    <a href="http://localhost/shakti/category/catalog/%d0%bc%d0%b0%d1%81%d0%bb%d0%b0/"
                       class="app-rel app-box menu-name has-childes ">
                        <span class="name">Масла</span>
                        <span class="count">(0)</span>
                        <div class="menu-icon"
                             id="toggle-16">
                            <i class="fa fa-angle-right"></i>
                        </div>
                    </a>
                </li>
                            <li class="menu-item">
                    <a href="http://localhost/shakti/category/catalog/%d0%ba%d0%be%d1%81%d0%bc%d0%b5%d1%82%d0%b8%d0%ba%d0%b0/"
                       class="app-rel app-box menu-name has-childes ">
                        <span class="name">Косметика</span>
                        <span class="count">(0)</span>
                        <div class="menu-icon"
                             id="toggle-17">
                            <i class="fa fa-angle-right"></i>
                        </div>
                    </a>
                </li>
                            <li class="menu-item">
                    <a href="http://localhost/shakti/category/catalog/shakty-food-%d0%bf%d1%80%d0%be%d0%b4%d1%83%d0%ba%d1%82%d1%8b/"
                       class="app-rel app-box menu-name has-childes ">
                        <span class="name">Shakty-Food продукты</span>
                        <span class="count">(0)</span>
                        <div class="menu-icon"
                             id="toggle-20">
                            <i class="fa fa-angle-right"></i>
                        </div>
                    </a>
                </li>
            
        </ul>
    </div>
</div>
                    </div>
                </div>
                <div class="filter-item">
                    <div class="filter-item-title">
                        <span class="filter-item-icon"><i class="fa fa-cog"></i></span>
                        <span class="filter-item-name">Фильтры</span>
                    </div>
                    <div class="filter-item-content">
                        <div class="module-filter">
    <div class="app-rel app-box filters-wrapper">

        <div class="app-rel app-box filter-block">
            <div class="app-rel app-box app-vector-title filter-title">
                <span>Цена</span>
            </div>
            <div class="app-rel app-box price-input-block">
                <label for="price-filter-from"
                       class="app-rel app-box price-filter-title">
                    <span class="price-label-text"></span>
                </label>
                <input id="price-filter-from"
                       class="app-rel app-box price-input"
                       type="number"
                       min="500"
                       max="100000"
                       step="50"
                       value="500"/>
                <div class="price-label-key">RUB</div>
            </div>
            <div class="app-rel app-box price-input-block">
                <label for="price-filter-to"
                       class="app-rel app-box price-filter-title">
                    <span class="price-label-text"></span>
                </label>
                <input id="price-filter-to"
                       class="app-rel app-box price-input"
                       type="number"
                       min="500"
                       max="100000"
                       step="50"
                       value="100000"/>
                <div class="price-label-key">RUB</div>
            </div>

            
            <input type="hidden" id="category-filter-id" value="25">
            
        </div>

                    <div class="app-rel app-box filter-block">
                <div class="app-rel app-box app-vector-title filter-title">
                    <span>Бренды</span>
                </div>
                <select id="select-brand"
                        name="brand"
                        title="Бренды"
                        data-type="brand"
                        data-show="true"
                        class="select-filter"
                        multiple="multiple">
                                            <option value="154">
                            Dragon Herbs
                        </option>
                    
                </select>
            </div>
        
    </div>
</div>
                    </div>
                </div>
            </div>

            <div class="products-container"
                 id="products-container">
                <div class="module-products">
    <div class="app-rel app-box products-wrapper">
                    <div class="app-rel app-box product-item">
                <div class="product-plugin">
    <a href="http://localhost/shakti/products/dsfdsfdsf/"
       class="item-image">

        <div class="item-props">
                    </div>

        <div class="product-cover">
            <div class="product-image"
                 style="background-image: url('http://localhost/shakti/wp-content/uploads/2018/06/7.jpg')">
            </div>
        </div>

        <h4 class="product-name">
            Ягоды Годжи Heaven Mountain
        </h4>
    </a>

    <div class="item-price">
        <div class="item-real-price">
                            <div class="price-block">
                    <span class="price-value">4 000</span>
                    <span class="price-key">RUB</span>
                </div>
            
                    </div>
    </div>

    <div class="item-tools">
        <div id="item-like-194"
             data-item-id="194"
             class="like-tool item-tool ">
            <i class="fa fa-heart"></i>
        </div>
        <div id="item-cart-194"
             data-item-id="194"
             class="cart-tool item-tool">
            <i class="fa fa-shopping-cart"></i>
        </div>
    </div>
</div>
            </div>
                    <div class="app-rel app-box product-item">
                <div class="product-plugin">
    <a href="http://localhost/shakti/products/adsadsadasd/"
       class="item-image">

        <div class="item-props">
                            <div class="item-sale">
                    10
                </div>
                    </div>

        <div class="product-cover">
            <div class="product-image"
                 style="background-image: url('http://localhost/shakti/wp-content/uploads/2018/06/11.jpg')">
            </div>
        </div>

        <h4 class="product-name">
            Ягоды Годжи капли Heaven Mountain
        </h4>
    </a>

    <div class="item-price">
        <div class="item-real-price">
                            <div class="price-block">
                    <span class="price-value">1 440</span>
                    <span class="price-key">RUB</span>
                </div>
            
                            <div class="sale-block">
                    <span class="price-value">1 600</span>
                    <span class="price-key">RUB</span>
                </div>
                    </div>
    </div>

    <div class="item-tools">
        <div id="item-like-192"
             data-item-id="192"
             class="like-tool item-tool ">
            <i class="fa fa-heart"></i>
        </div>
        <div id="item-cart-192"
             data-item-id="192"
             class="cart-tool item-tool">
            <i class="fa fa-shopping-cart"></i>
        </div>
    </div>
</div>
            </div>
        
    </div>
    <div class="app-rel app-box products-pagination">
        <div class="module-pagination">
    <div class="app-rel app-box pagination-wrapper">
        <div class="app-rel app-box app-left app-center pagination-pages">
            
        </div>
    </div>
</div>
    </div>
</div>
            </div>
        </div>
    </div>
</div><?php }
}
