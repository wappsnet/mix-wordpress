<?php
/* Smarty version 3.1.30, created on 2018-07-30 02:10:22
  from "C:\xampp\htdocs\cordyceps1\wp-content\themes\wappsnet\plugins\comments\index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b5e738e4d5b04_46690978',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '7ebb510fbeea6923f3e7800e6321f7c8904cc53d' => 
    array (
      0 => 'C:\\xampp\\htdocs\\cordyceps1\\wp-content\\themes\\wappsnet\\plugins\\comments\\index.tpl',
      1 => 1530660809,
      2 => 'file',
    ),
  ),
  'cache_lifetime' => 86400,
),true)) {
function content_5b5e738e4d5b04_46690978 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="comments-plugin">
    <div class="create-comment">
        <div class="auth-item">
            <div id="auth-info-messages"
                 class="auth-info-messages">
            </div>

            <div class="auth-field">
                <div class="auth-icon">
                    <i class="fa fa-user"></i>
                </div>
                <input class="auth-input user_comment"
                       id="user_comment_name"
                       name="name"
                       data-type="name"
                       placeholder=""
                       type="text"/>
            </div>

            <div class="auth-field">
                <div class="auth-icon">
                    <i class="fa fa-envelope"></i>
                </div>
                <input class="auth-input user_comment"
                       id="user_comment_email"
                       name="email"
                       data-type="email"
                       placeholder=""
                       type="email"/>
            </div>

            <div class="auth-field auth-text">
                <div class="auth-icon">
                    <i class="fa fa-book"></i>
                </div>
                <textarea class="auth-input user_comment"
                          id="user_comment_text"
                          name="text"
                          data-type="text"
                          placeholder="">
                </textarea>
            </div>

            <input class="auth-input user_comment"
                   id="user_comment_post_id"
                   type="hidden"
                   data-type="hidden"
                   value="34"
                   name="post_id">

            <div class="auth-footer">
                <button id="user-comment"
                        class="auth-button waves-effect waves-light btn">
                    
                </button>
            </div>
        </div>
    </div>

            <div class="app-rel app-box empty-comments">
            
        </div>
    </div><?php }
}
