<?php
/* Smarty version 3.1.30, created on 2018-07-28 12:54:28
  from "C:\xampp\htdocs\cordyceps1\wp-content\themes\wappsnet\plugins\filters\index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b5c6784724704_36005649',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '53ad0a6a8221ede79a60a3a5cc5818b62be55966' => 
    array (
      0 => 'C:\\xampp\\htdocs\\cordyceps1\\wp-content\\themes\\wappsnet\\plugins\\filters\\index.tpl',
      1 => 1530778214,
      2 => 'file',
    ),
  ),
  'cache_lifetime' => 86400,
),true)) {
function content_5b5c6784724704_36005649 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="plugin-filters">
    <div class="filters-title">
        <span class="title-icon"><i class="fa fa-cogs"></i></span>
        <span class="title-name"></span>
    </div>
    <div class="filters-wrapper">
        <form action="" type="get">
            <div class="filter-block">
                <div class="filter-title">
                    <span></span>
                </div>
                <div class="price-input-block">
                    <label for="price-filter-from"
                           class="price-filter-title">
                        <span class="price-label-text"></span>
                    </label>
                    <input id="price-filter-from"
                           class="price-input"
                           type="number"
                           name="filter_price_min"
                           min=""
                           max=""
                           step="50"
                           value="500"/>
                    <div class="price-label-key"></div>
                </div>
                <div class="price-input-block">
                    <label for="price-filter-to"
                           class="price-filter-title">
                        <span class="price-label-text"></span>
                    </label>
                    <input id="price-filter-to"
                           class="price-input"
                           type="number"
                           name="filter_price_max"
                           min=""
                           max=""
                           step="50"
                           value="100000"/>
                    <div class="price-label-key"></div>
                </div>

                
                <input type="hidden"
                       name="filter_category"
                       id="category-filter-id"
                       value=""/>
                
            </div>

                            <div class="filter-block">
                    <div class="filter-title">
                        <span></span>
                    </div>
                    
                </div>
            

            <div class="filters-footer">
                <button type="submit"
                        class="submit-filters btn btn-flat waves-effect">
                    
                </button>

                            </div>
        </form>
    </div>
</div><?php }
}
