<?php
/* Smarty version 3.1.30, created on 2018-07-01 21:39:49
  from "C:\xampp\htdocs\shakti\wp-content\themes\wappsnet\modules\category\index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b394a253fb2b6_37460214',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '98987db914178778167e6c785889cd1d273155fd' => 
    array (
      0 => 'C:\\xampp\\htdocs\\shakti\\wp-content\\themes\\wappsnet\\modules\\category\\index.tpl',
      1 => 1530132828,
      2 => 'file',
    ),
  ),
  'cache_lifetime' => 86400,
),true)) {
function content_5b394a253fb2b6_37460214 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="module-category">
    <div class="app-fill1200">
        <div class="products-block-app">
            <div class="filters-container">
                <div class="filter-item">
                    <div class="filter-item-title">
                        <span class="filter-item-icon"><i class="fa fa-bars"></i></span>
                        <span class="filter-item-name">Категории</span>
                    </div>
                    <div class="filter-item-content">
                        <div class="categories-list-wrapper">
                            <ul class="categories-list"><li class="category-item active"><a href="http://localhost/shakti/category/catalog/"><span class="cat-name">Каталог</span><span class="cat-count">(2)</span></a><ul class="categories-list"><li class="category-item"><a href="http://localhost/shakti/category/catalog/shakty-food-%d0%bf%d1%80%d0%be%d0%b4%d1%83%d0%ba%d1%82%d1%8b/"><span class="cat-name">Shakty-Food продукты</span><span class="cat-count">(1)</span></a></li><li class="category-item"><a href="http://localhost/shakti/category/catalog/bads/"><span class="cat-name">БАДы</span><span class="cat-count">(2)</span></a></li><li class="category-item"><a href="http://localhost/shakti/category/catalog/cosmetics/"><span class="cat-name">Косметика</span><span class="cat-count">(2)</span></a></li><li class="category-item"><a href="http://localhost/shakti/category/catalog/creams/"><span class="cat-name">Масла</span><span class="cat-count">(2)</span></a></li><li class="category-item"><a href="http://localhost/shakti/category/catalog/news-items/"><span class="cat-name">Новинки</span><span class="cat-count">(2)</span></a></li><li class="category-item"><a href="http://localhost/shakti/category/catalog/bestsellers/"><span class="cat-name">Распродажа</span><span class="cat-count">(2)</span></a></li><li class="category-item"><a href="http://localhost/shakti/category/catalog/super-foods/"><span class="cat-name">Суперфуды</span><span class="cat-count">(2)</span></a></li><li class="category-item"><a href="http://localhost/shakti/category/catalog/dried-fruits/"><span class="cat-name">Сушеные фрукты</span><span class="cat-count">(2)</span></a></li><li class="category-item"><a href="http://localhost/shakti/category/catalog/tea/"><span class="cat-name">Чай</span><span class="cat-count">(2)</span></a></li></ul></li></ul>
                        </div>
                    </div>
                </div>
                <div class="filter-item">
                    <div class="filter-item-title">
                        <span class="filter-item-icon"><i class="fa fa-cog"></i></span>
                        <span class="filter-item-name">Фильтры</span>
                    </div>
                    <div class="filter-item-content">
                        <div class="filters-list-wrapper">
                            <div class="module-filter">
    <div class="app-rel app-box filters-wrapper">

        <div class="app-rel app-box filter-block">
            <div class="app-rel app-box app-vector-title filter-title">
                <span>Цена</span>
            </div>
            <div class="app-rel app-box price-input-block">
                <label for="price-filter-from"
                       class="app-rel app-box price-filter-title">
                    <span class="price-label-text"></span>
                </label>
                <input id="price-filter-from"
                       class="app-rel app-box price-input"
                       type="number"
                       min="500"
                       max="100000"
                       step="50"
                       value="500"/>
                <div class="price-label-key">RUB</div>
            </div>
            <div class="app-rel app-box price-input-block">
                <label for="price-filter-to"
                       class="app-rel app-box price-filter-title">
                    <span class="price-label-text"></span>
                </label>
                <input id="price-filter-to"
                       class="app-rel app-box price-input"
                       type="number"
                       min="500"
                       max="100000"
                       step="50"
                       value="100000"/>
                <div class="price-label-key">RUB</div>
            </div>

            
            <input type="hidden" id="category-filter-id" value="25">
            
        </div>

                    <div class="app-rel app-box filter-block">
                <div class="app-rel app-box app-vector-title filter-title">
                    <span>Бренды</span>
                </div>
                <select id="select-brand"
                        name="brand"
                        title="Бренды"
                        data-type="brand"
                        data-show="true"
                        class="select-filter"
                        multiple="multiple">
                                            <option value="154">
                            Dragon Herbs
                        </option>
                    
                </select>
            </div>
        
    </div>
</div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="products-container"
                 id="products-container">
                <div class="module-products">
    <div class="products-wrapper">
                    <div class="product-item">
                <div class="product-plugin">
    <a href="http://localhost/shakti/products/dsfdsfdsf/"
       class="item-image">

        <div class="item-props">
                    </div>

        <div class="product-cover">
            <div class="product-image"
                 style="background-image: url('http://localhost/shakti/wp-content/uploads/2018/06/7.jpg')">
            </div>
        </div>

        <h4 class="product-name">
            Ягоды Годжи Heaven Mountain
        </h4>
    </a>

    <div class="item-price">
        <div class="item-real-price">
                            <div class="price-block">
                    <span class="price-value">4 000</span>
                    <span class="price-key">RUB</span>
                </div>
            
                    </div>
    </div>

    <div class="item-tools">
        <div id="item-like-194"
             data-animate-id="item-like-194"
             data-item-id="194"
             class="like-tool item-tool ">
            <i class="fa fa-heart"></i>
        </div>
        <div id="item-cart-194"
             data-animate-id="item-cart-194"
             data-item-id="194"
             class="cart-tool item-tool">
            <i class="fa fa-shopping-cart"></i>
        </div>
    </div>
</div>
            </div>
                    <div class="product-item">
                <div class="product-plugin">
    <a href="http://localhost/shakti/products/adsadsadasd/"
       class="item-image">

        <div class="item-props">
                            <div class="item-sale">
                    10
                </div>
                    </div>

        <div class="product-cover">
            <div class="product-image"
                 style="background-image: url('http://localhost/shakti/wp-content/uploads/2018/06/11.jpg')">
            </div>
        </div>

        <h4 class="product-name">
            Ягоды Годжи капли Heaven Mountain
        </h4>
    </a>

    <div class="item-price">
        <div class="item-real-price">
                            <div class="price-block">
                    <span class="price-value">1 440</span>
                    <span class="price-key">RUB</span>
                </div>
            
                            <div class="sale-block">
                    <span class="price-value">1 600</span>
                    <span class="price-key">RUB</span>
                </div>
                    </div>
    </div>

    <div class="item-tools">
        <div id="item-like-192"
             data-animate-id="item-like-192"
             data-item-id="192"
             class="like-tool item-tool ">
            <i class="fa fa-heart"></i>
        </div>
        <div id="item-cart-192"
             data-animate-id="item-cart-192"
             data-item-id="192"
             class="cart-tool item-tool">
            <i class="fa fa-shopping-cart"></i>
        </div>
    </div>
</div>
            </div>
        
    </div>
    <div class="products-pagination">
        <div class="module-pagination">
    <div class="app-rel app-box pagination-wrapper">
        <div class="app-rel app-box app-left app-center pagination-pages">
            
        </div>
    </div>
</div>
    </div>
</div>
            </div>
        </div>
    </div>
</div><?php }
}
