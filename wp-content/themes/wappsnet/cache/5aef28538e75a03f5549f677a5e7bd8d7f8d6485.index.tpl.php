<?php
/* Smarty version 3.1.30, created on 2018-07-12 21:14:17
  from "C:\xampp\htdocs\shakti\wp-content\themes\wappsnet\modules\products\index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b47c4a94cffb3_35729474',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '24ab87d18b1638d733e1b145f1167fe171b21136' => 
    array (
      0 => 'C:\\xampp\\htdocs\\shakti\\wp-content\\themes\\wappsnet\\modules\\products\\index.tpl',
      1 => 1530138668,
      2 => 'file',
    ),
  ),
  'cache_lifetime' => 86400,
),true)) {
function content_5b47c4a94cffb3_35729474 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="module-products">
    <div class="products-wrapper">
                    <div class="product-item">
                <div class="product-plugin">
    <a href="http://localhost/shakti/products/natures-answer-%d0%b3%d1%80%d0%b8%d0%b1%d1%8b-%d1%80%d0%b5%d0%b9%d1%88%d0%b8/"
       class="item-image">

        <div class="item-props">
                    </div>

        <div class="product-cover">
            <div class="product-image"
                 style="background-image: url('http://localhost/shakti/wp-content/uploads/2018/07/6.jpg')">
            </div>
        </div>

        <h4 class="product-name">
            Nature's Answer, Грибы рейши
        </h4>
    </a>

    <div class="item-price">
        <div class="item-real-price">
                            <div class="price-block">
                    <span class="price-value">3 400</span>
                    <span class="price-key">RUB</span>
                </div>
            
                    </div>
    </div>

    <div class="item-tools">
        <div id="item-like-324"
             data-animate-id="item-like-324"
             data-item-id="324"
             class="simple-tools-item like-tool item-tool ">
            <i class="fa fa-heart"></i>
        </div>
        <div id="item-cart-324"
             data-animate-id="item-cart-324"
             data-item-id="324"
             class="simple-tools-item cart-tool item-tool">
            <i class="fa fa-shopping-cart"></i>
        </div>
    </div>
</div>
            </div>
                    <div class="product-item">
                <div class="product-plugin">
    <a href="http://localhost/shakti/products/321/"
       class="item-image">

        <div class="item-props">
                    </div>

        <div class="product-cover">
            <div class="product-image"
                 style="background-image: url('http://localhost/shakti/wp-content/uploads/2018/07/4.jpg')">
            </div>
        </div>

        <h4 class="product-name">
            Nature's Way, Корень Fo-Ti, 610 мг
        </h4>
    </a>

    <div class="item-price">
        <div class="item-real-price">
                            <div class="price-block">
                    <span class="price-value">3 800</span>
                    <span class="price-key">RUB</span>
                </div>
            
                    </div>
    </div>

    <div class="item-tools">
        <div id="item-like-321"
             data-animate-id="item-like-321"
             data-item-id="321"
             class="simple-tools-item like-tool item-tool ">
            <i class="fa fa-heart"></i>
        </div>
        <div id="item-cart-321"
             data-animate-id="item-cart-321"
             data-item-id="321"
             class="simple-tools-item cart-tool item-tool">
            <i class="fa fa-shopping-cart"></i>
        </div>
    </div>
</div>
            </div>
                    <div class="product-item">
                <div class="product-plugin">
    <a href="http://localhost/shakti/products/mrm-%d0%ba%d0%be%d1%80%d0%b4%d0%b8%d1%86%d0%b5%d0%bf%d1%81-%d1%88%d1%82%d0%b0%d0%bc%d0%bc-cs-4/"
       class="item-image">

        <div class="item-props">
                    </div>

        <div class="product-cover">
            <div class="product-image"
                 style="background-image: url('http://localhost/shakti/wp-content/uploads/2018/07/16.jpg')">
            </div>
        </div>

        <h4 class="product-name">
            MRM, Кордицепс, штамм CS-4
        </h4>
    </a>

    <div class="item-price">
        <div class="item-real-price">
                            <div class="price-block">
                    <span class="price-value">3 500</span>
                    <span class="price-key">RUB</span>
                </div>
            
                    </div>
    </div>

    <div class="item-tools">
        <div id="item-like-318"
             data-animate-id="item-like-318"
             data-item-id="318"
             class="simple-tools-item like-tool item-tool ">
            <i class="fa fa-heart"></i>
        </div>
        <div id="item-cart-318"
             data-animate-id="item-cart-318"
             data-item-id="318"
             class="simple-tools-item cart-tool item-tool">
            <i class="fa fa-shopping-cart"></i>
        </div>
    </div>
</div>
            </div>
                    <div class="product-item">
                <div class="product-plugin">
    <a href="http://localhost/shakti/products/source-naturals-%d0%ba%d1%80%d0%b5%d0%bc-skin-eternal/"
       class="item-image">

        <div class="item-props">
                    </div>

        <div class="product-cover">
            <div class="product-image"
                 style="background-image: url('http://localhost/shakti/wp-content/uploads/2018/07/1.jpg')">
            </div>
        </div>

        <h4 class="product-name">
            Source Naturals, Крем Skin Eternal
        </h4>
    </a>

    <div class="item-price">
        <div class="item-real-price">
                            <div class="price-block">
                    <span class="price-value">4 200</span>
                    <span class="price-key">RUB</span>
                </div>
            
                    </div>
    </div>

    <div class="item-tools">
        <div id="item-like-315"
             data-animate-id="item-like-315"
             data-item-id="315"
             class="simple-tools-item like-tool item-tool ">
            <i class="fa fa-heart"></i>
        </div>
        <div id="item-cart-315"
             data-animate-id="item-cart-315"
             data-item-id="315"
             class="simple-tools-item cart-tool item-tool">
            <i class="fa fa-shopping-cart"></i>
        </div>
    </div>
</div>
            </div>
                    <div class="product-item">
                <div class="product-plugin">
    <a href="http://localhost/shakti/products/white-egret-personal-care-%d0%b3%d0%b8%d0%bb%d0%b0%d1%83%d1%80%d0%be%d0%bd%d0%be%d0%b2%d0%b0%d1%8f-%d0%ba%d0%b8%d1%81%d0%bb%d0%be%d1%82%d0%b0/"
       class="item-image">

        <div class="item-props">
                    </div>

        <div class="product-cover">
            <div class="product-image"
                 style="background-image: url('http://localhost/shakti/wp-content/uploads/2018/07/8.jpg')">
            </div>
        </div>

        <h4 class="product-name">
            White Egret Personal Care, Гилауроновая кислота
        </h4>
    </a>

    <div class="item-price">
        <div class="item-real-price">
                            <div class="price-block">
                    <span class="price-value">3 200</span>
                    <span class="price-key">RUB</span>
                </div>
            
                    </div>
    </div>

    <div class="item-tools">
        <div id="item-like-314"
             data-animate-id="item-like-314"
             data-item-id="314"
             class="simple-tools-item like-tool item-tool ">
            <i class="fa fa-heart"></i>
        </div>
        <div id="item-cart-314"
             data-animate-id="item-cart-314"
             data-item-id="314"
             class="simple-tools-item cart-tool item-tool">
            <i class="fa fa-shopping-cart"></i>
        </div>
    </div>
</div>
            </div>
                    <div class="product-item">
                <div class="product-plugin">
    <a href="http://localhost/shakti/products/310/"
       class="item-image">

        <div class="item-props">
                    </div>

        <div class="product-cover">
            <div class="product-image"
                 style="background-image: url('http://localhost/shakti/wp-content/uploads/2018/07/7.jpg')">
            </div>
        </div>

        <h4 class="product-name">
            White Egret Personal Care, Гилауроновая кислота
        </h4>
    </a>

    <div class="item-price">
        <div class="item-real-price">
                            <div class="price-block">
                    <span class="price-value">2 500</span>
                    <span class="price-key">RUB</span>
                </div>
            
                    </div>
    </div>

    <div class="item-tools">
        <div id="item-like-310"
             data-animate-id="item-like-310"
             data-item-id="310"
             class="simple-tools-item like-tool item-tool ">
            <i class="fa fa-heart"></i>
        </div>
        <div id="item-cart-310"
             data-animate-id="item-cart-310"
             data-item-id="310"
             class="simple-tools-item cart-tool item-tool">
            <i class="fa fa-shopping-cart"></i>
        </div>
    </div>
</div>
            </div>
                    <div class="product-item">
                <div class="product-plugin">
    <a href="http://localhost/shakti/products/dsfdsfdsf/"
       class="item-image">

        <div class="item-props">
                    </div>

        <div class="product-cover">
            <div class="product-image"
                 style="background-image: url('http://localhost/shakti/wp-content/uploads/2018/06/7.jpg')">
            </div>
        </div>

        <h4 class="product-name">
            Ягоды Годжи Heaven Mountain
        </h4>
    </a>

    <div class="item-price">
        <div class="item-real-price">
                            <div class="price-block">
                    <span class="price-value">4 000</span>
                    <span class="price-key">RUB</span>
                </div>
            
                    </div>
    </div>

    <div class="item-tools">
        <div id="item-like-194"
             data-animate-id="item-like-194"
             data-item-id="194"
             class="simple-tools-item like-tool item-tool ">
            <i class="fa fa-heart"></i>
        </div>
        <div id="item-cart-194"
             data-animate-id="item-cart-194"
             data-item-id="194"
             class="simple-tools-item cart-tool item-tool">
            <i class="fa fa-shopping-cart"></i>
        </div>
    </div>
</div>
            </div>
                    <div class="product-item">
                <div class="product-plugin">
    <a href="http://localhost/shakti/products/adsadsadasd/"
       class="item-image">

        <div class="item-props">
                            <div class="item-sale">
                    <span class="label"></span>
                    <span class="value">20%</span>
                </div>
                    </div>

        <div class="product-cover">
            <div class="product-image"
                 style="background-image: url('http://localhost/shakti/wp-content/uploads/2018/06/11.jpg')">
            </div>
        </div>

        <h4 class="product-name">
            Ягоды Годжи капли Heaven Mountain
        </h4>
    </a>

    <div class="item-price">
        <div class="item-real-price">
                            <div class="price-block">
                    <span class="price-value">1 280</span>
                    <span class="price-key">RUB</span>
                </div>
            
                            <div class="sale-block">
                    <span class="price-value">1 600</span>
                    <span class="price-key">RUB</span>
                </div>
                    </div>
    </div>

    <div class="item-tools">
        <div id="item-like-192"
             data-animate-id="item-like-192"
             data-item-id="192"
             class="simple-tools-item like-tool item-tool ">
            <i class="fa fa-heart"></i>
        </div>
        <div id="item-cart-192"
             data-animate-id="item-cart-192"
             data-item-id="192"
             class="simple-tools-item cart-tool item-tool">
            <i class="fa fa-shopping-cart"></i>
        </div>
    </div>
</div>
            </div>
        
    </div>
    <div class="products-pagination">
        <div class="plugin-pagination">
    
</div>
    </div>
</div><?php }
}
