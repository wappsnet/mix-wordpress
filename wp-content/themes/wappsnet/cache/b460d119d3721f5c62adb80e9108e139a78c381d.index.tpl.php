<?php
/* Smarty version 3.1.30, created on 2018-06-23 00:51:51
  from "C:\xampp\htdocs\shakti\wp-content\themes\wappsnet\plugins\register\index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b2d99a785d074_17142520',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '09477d318f71fa2f3517c7b5c380de0075bb83e6' => 
    array (
      0 => 'C:\\xampp\\htdocs\\shakti\\wp-content\\themes\\wappsnet\\plugins\\register\\index.tpl',
      1 => 1529715061,
      2 => 'file',
    ),
  ),
  'cache_lifetime' => 86400,
),true)) {
function content_5b2d99a785d074_17142520 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="plugin-register">
    <div class="auth-item">
        <div class="auth-field">
            <div class="auth-icon">
                <i class="fa fa-user"></i>
            </div>
            <input class="auth-input register-input"
                   id="register-email"
                   name="email"
                   data-type="email"
                   placeholder="Логин email-адрес"
                   type="email"/>
        </div>
        <div class="auth-field">
            <div class="auth-icon">
                <i class="fa fa-key"></i>
            </div>
            <input class="auth-input register-input"
                   id="register-password"
                   name="password"
                   data-type="password"
                   placeholder="Пароль"
                   type="password"/>
        </div>
        <div class="auth-field">
            <input class="auth-input register-input"
                   id="register-f-name"
                   name="fName"
                   data-type="name"
                   placeholder="Имя"
                   type="text"/>
        </div>
        <div class="auth-field">
            <input class="auth-input register-input"
                   id="register-l-name"
                   name="lName"
                   data-type="name"
                   placeholder="Фамилия"
                   type="text"/>
        </div>
        <div class="auth-field">
            <div class="auth-icon">
                <i class="fa fa-phone"></i>
            </div>
            <input class="auth-input register-input phone-input"
                   id="register-phone"
                   name="phone"
                   data-type="phone"
                   placeholder="+7 (777) 777-7777"
                   type="tel"/>
        </div>
        <div class="auth-footer">
            <button id="user-register"
                    class="auth-button waves-effect waves-light btn">
                Регистрация
            </button>
        </div>
    </div>
</div><?php }
}
