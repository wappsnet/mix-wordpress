<?php
/* Smarty version 3.1.30, created on 2018-07-30 01:17:28
  from "C:\xampp\htdocs\cordyceps1\wp-content\themes\wappsnet\modules\singlepage\index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b5e6728ca4118_06688567',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'f1e6eebe4a097cd6e833ba9ad9f53d3f0d8b7daf' => 
    array (
      0 => 'C:\\xampp\\htdocs\\cordyceps1\\wp-content\\themes\\wappsnet\\modules\\singlepage\\index.tpl',
      1 => 1530506775,
      2 => 'file',
    ),
  ),
  'cache_lifetime' => 86400,
),true)) {
function content_5b5e6728ca4118_06688567 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="module-single-page">
    <div class="app-fill1200">
        <div class="single-page-wrapper">
            <h1 class="page-title">
                Контакты
            </h1>
            <div class="page-content">
                <strong><span class="text-warning">График работы магазина: круглосуточно без выходных.</span></strong>

Часы приема звонков:
<div class="typography">
<ul>
 	<li>с 08:00 до 22:00 пн-пт</li>
 	<li>с 08:00 до 21:00 сб-вс</li>
</ul>
<strong>Телефоны для справок и заказов</strong>

<hr />

<h4><strong>+7 (985)</strong> <strong>997-59-00</strong></h4>

<hr />

<p>
Звоните нам, мы всегда ответим и проконсультируем Вас!
</p>
<p>
С нами также можно связаться по электронной почте:
</p>
<p>
<strong>Отдел тех. поддержки</strong> <strong>→</strong> <strong><a href="info@cordiceps1.ru">info@cordiceps1.ru</a></strong>
</p>
<p>
<strong>Отдел закупок</strong> <strong>→</strong> <strong><a href="mailto:partners@batiste-parfume.ru">partners@</a><a href="info@cordiceps1.ru">cordiceps1.ru</a></strong>
</p>
</div>
<iframe style="width: 100%;" src="https://api-maps.yandex.ru/frame/v1/-/CZTUaJ2f" width="100%" height="400" frameborder="0"></iframe>
            </div>
        </div>
    </div>
</div><?php }
}
