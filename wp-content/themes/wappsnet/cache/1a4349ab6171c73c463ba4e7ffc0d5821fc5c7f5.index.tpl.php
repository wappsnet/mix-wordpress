<?php
/* Smarty version 3.1.30, created on 2018-07-23 23:44:38
  from "C:\xampp\htdocs\shakti\wp-content\themes\wappsnet\plugins\createorder\index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b566866a51a06_42283199',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '1170572754a2ae7dd6578b76c7d5c5e8cfa6fdb8' => 
    array (
      0 => 'C:\\xampp\\htdocs\\shakti\\wp-content\\themes\\wappsnet\\plugins\\createorder\\index.tpl',
      1 => 1532386059,
      2 => 'file',
    ),
  ),
  'cache_lifetime' => 86400,
),true)) {
function content_5b566866a51a06_42283199 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="plugin-create-order">
    <div class="create-order-section"
         id="order-create-form">
        <div class="auth-item">
            <div id="auth-info-messages"
                 class="auth-info-messages">
            </div>

            <label for="user_create_order_email"
                   class="auth-label">
                <span>Емайл адрес</span>
            </label>
            <div class="auth-field">
                <input class="auth-input user_create_order"
                       id="user_create_order_email"
                       name="user_email"
                       data-type="email"
                       value="mike-tm555@mail.ru"
                       type="email"/>
            </div>

            <label for="user_create_order_name"
                   class="auth-label">
                <span>Имя Фамилия</span>
            </label>
            <div class="auth-field">
                <input class="auth-input user_create_order"
                       id="user_create_order_name"
                       name="user_name"
                       data-type="text"
                       value="Mike Tevan"
                       type="text"/>
            </div>

            <label for="user_create_order_name"
                   class="auth-label">
                <span>Адрес доставки</span>
            </label>
            <div class="auth-field">
                <input class="auth-input user_create_order"
                       id="user_create_order_delivery_address"
                       name="delivery_address"
                       value="950 Ridge RD C25, P2763"
                       data-type="text"
                       type="text"/>
            </div>

            <label for="user_create_order_name"
                   class="auth-label">
                <span>Регион</span>
            </label>
            <div class="auth-field">
                <select class="auth-input user_create_order"
                        id="user_create_order_delivery_region"
                        name="delivery_region"
                        data-type="select">
                                                                        <option value="48">
                                Москва
                            </option>
                                                                                                <option value="52">
                                Санкт-Петербург
                            </option>
                                                                                                <option value="53">
                                Другие регионы Россия
                            </option>
                                            
                </select>
            </div>

            <label for="user_create_order_name"
                   class="auth-label">
                <span>Почтовый индекс</span>
            </label>
            <div class="auth-field">
                <input class="auth-input user_create_order"
                       id="user_create_order_delivery_post_index"
                       name="delivery_post_index"
                       data-type="text"
                       type="text"/>
            </div>

            <label for="user_create_order_name"
                   class="auth-label">
                <span>Телефон</span>
            </label>
            <div class="auth-field">
                <input class="auth-input user_create_order"
                       id="user_create_order_phone"
                       name="user_phone"
                       data-type="phone"
                       value="+1 83024148567"
                       type="tel"/>
            </div>

            <input class="auth-input user_create_order"
                   id="user_create_order_user_id"
                   type="hidden"
                   name="user_id"
                   data-type="hidden"
                   value="1"/>

            <div class="auth-footer">
                <button id="user-create-order"
                        class="auth-button create-order waves-effect waves-light btn">
                    Продолжить
                </button>
            </div>
        </div>
    </div>

    <div class="create-order-section hidden"
         id="order-confirm-form">
    </div>
</div><?php }
}
