<?php
/* Smarty version 3.1.30, created on 2018-12-25 23:43:39
  from "C:\xampp\htdocs\mix\wp-content\themes\wappsnet\modules\footer\index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5c22c0ab8f6a29_01064534',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '412a625b756e777f0dd9f6f559fbaed34b952468' => 
    array (
      0 => 'C:\\xampp\\htdocs\\mix\\wp-content\\themes\\wappsnet\\modules\\footer\\index.tpl',
      1 => 1544371732,
      2 => 'file',
    ),
  ),
  'cache_lifetime' => 86400,
),true)) {
function content_5c22c0ab8f6a29_01064534 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="footer-module">
    <div class="footer-social">
        <div class="app-fill">
            <div class="footer-social-wrapper">
                <div class="plugin-social-icons">
            <a class="social-link"
           href="https://www.facebook.com/Mix-Trip-519573611846732/">
            <i class="fab fa-facebook"></i>
        </a>
            <a class="social-link"
           href="https://vk.com/mix.trip">
            <i class="fab fa-vk"></i>
        </a>
            <a class="social-link"
           href="https://www.instagram.com/mix_trip_/">
            <i class="fab fa-instagram"></i>
        </a>
    
</div>
            </div>
        </div>
    </div>

    <div class="footer-menu">
        <div class="app-fill">
            <div class="footer-menu-wrapper">
                <div class="plugin-navigation">
    <ul id="menu-secondary" class="menu"><li id="menu-item-92" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-has-children menu-item-92"><a href="http://localhost/mix/category/%d0%bf%d0%bb%d1%8f%d0%b6/">Пляж</a>
<ul class="sub-menu">
	<li id="menu-item-100" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-100"><a href="http://localhost/mix/services/flights/">Авиабилеты</a></li>
	<li id="menu-item-98" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-98"><a href="http://localhost/mix/services/hotels/">Отели</a></li>
</ul>
</li>
<li id="menu-item-69" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-69"><a href="http://localhost/mix/%d0%be-%d0%bf%d1%80%d0%be%d0%b5%d0%ba%d1%82%d0%b5/">О проекте</a>
<ul class="sub-menu">
	<li id="menu-item-75" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-75"><a href="http://localhost/mix/%d0%be-%d0%bf%d1%80%d0%be%d0%b5%d0%ba%d1%82%d0%b5/%d0%be-%d0%bd%d0%b0%d1%81/">О нас</a></li>
	<li id="menu-item-74" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-74"><a href="http://localhost/mix/%d0%be-%d0%bf%d1%80%d0%be%d0%b5%d0%ba%d1%82%d0%b5/%d1%80%d0%b5%d0%ba%d0%b2%d0%b8%d0%b7%d0%b8%d1%82%d1%8b/">Реквизиты</a></li>
	<li id="menu-item-72" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-72"><a href="http://localhost/mix/%d0%be-%d0%bf%d1%80%d0%be%d0%b5%d0%ba%d1%82%d0%b5/%d0%bd%d0%b0%d1%88%d0%b8-%d0%bf%d0%b0%d1%80%d1%82%d0%bd%d0%b5%d1%80%d1%8b/">Наши партнеры</a></li>
	<li id="menu-item-70" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-70"><a href="http://localhost/mix/%d0%be-%d0%bf%d1%80%d0%be%d0%b5%d0%ba%d1%82%d0%b5/%d1%80%d0%b5%d0%ba%d0%bb%d0%b0%d0%bc%d0%be%d0%b4%d0%b0%d1%82%d0%b5%d0%bb%d1%8f%d0%bc/">Рекламодателям</a></li>
</ul>
</li>
<li id="menu-item-65" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-65"><a href="http://localhost/mix/%d0%bf%d0%be%d0%bc%d0%be%d1%89%d1%8c/">Помощь</a>
<ul class="sub-menu">
	<li id="menu-item-73" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-73"><a href="http://localhost/mix/%d0%bf%d0%be%d0%bc%d0%be%d1%89%d1%8c/%d0%bf%d1%80%d0%b0%d0%b2%d0%b8%d0%bb%d0%b0-%d0%b8%d1%81%d0%bf%d0%be%d0%bb%d1%8c%d0%b7%d0%be%d0%b2%d0%b0%d0%bd%d0%b8%d1%8f/">Правила использования</a></li>
	<li id="menu-item-111" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-111"><a href="http://localhost/mix/%d0%bf%d0%be%d0%bc%d0%be%d1%89%d1%8c/%d0%b2%d0%be%d0%bf%d1%80%d0%be%d1%81%d1%8b-%d0%b8-%d0%be%d1%82%d0%b2%d0%b5%d1%82%d1%8b/">Вопросы и ответы</a></li>
	<li id="menu-item-110" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-110"><a href="http://localhost/mix/%d0%bf%d0%be%d0%bc%d0%be%d1%89%d1%8c/%d0%b1%d0%bb%d0%be%d0%b3/">Блог</a></li>
	<li id="menu-item-109" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-109"><a href="http://localhost/mix/%d0%bf%d0%be%d0%bc%d0%be%d1%89%d1%8c/%d1%82%d0%b0%d0%b1%d0%bb%d0%b8%d1%86%d0%b0-%d1%86%d0%b5%d0%bd/">Таблица цен</a></li>
</ul>
</li>
<li id="menu-item-66" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-66"><a href="http://localhost/mix/%d0%bf%d0%be%d0%bb%d1%8c%d0%b7%d0%be%d0%b2%d0%b0%d1%82%d0%b5%d0%bb%d1%8f%d0%bc/">Пользователям</a>
<ul class="sub-menu">
	<li id="menu-item-67" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-67"><a href="http://localhost/mix/%d0%bf%d0%be%d0%bb%d1%8c%d0%b7%d0%be%d0%b2%d0%b0%d1%82%d0%b5%d0%bb%d1%8f%d0%bc/%d0%bf%d0%be%d0%bb%d1%8c%d0%b7%d0%be%d0%b2%d0%b0%d1%82%d0%b5%d0%bb%d1%8c%d1%81%d0%ba%d0%be%d0%b5-%d1%81%d0%be%d0%b3%d0%bb%d0%b0%d1%88%d0%b5%d0%bd%d0%b8%d0%b5/">Пользовательское соглашение</a></li>
	<li id="menu-item-71" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-71"><a href="http://localhost/mix/%d0%be-%d0%bf%d1%80%d0%be%d0%b5%d0%ba%d1%82%d0%b5/%d0%ba%d0%be%d0%bd%d1%84%d0%b8%d0%b4%d0%b5%d0%bd%d1%86%d0%b8%d0%b0%d0%bb%d1%8c%d0%bd%d0%be%d1%81%d1%82%d1%8c/">Конфиденциальность</a></li>
	<li id="menu-item-68" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-68"><a href="http://localhost/mix/%d0%bf%d0%be%d0%bb%d1%8c%d0%b7%d0%be%d0%b2%d0%b0%d1%82%d0%b5%d0%bb%d1%8f%d0%bc/%d0%b1%d0%be%d0%bd%d1%83%d1%81%d0%bd%d0%b0%d1%8f-%d0%bf%d1%80%d0%be%d0%b3%d1%80%d0%b0%d0%bc%d0%bc%d0%b0/">Бонусная программа</a></li>
	<li id="menu-item-112" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-112"><a href="http://localhost/mix/%d0%bf%d0%be%d0%bb%d1%8c%d0%b7%d0%be%d0%b2%d0%b0%d1%82%d0%b5%d0%bb%d1%8f%d0%bc/%d0%b4%d0%be%d0%b3%d0%be%d0%b2%d0%be%d1%80-%d0%be%d1%84%d0%b5%d1%80%d1%82%d1%8b/">Договор оферты</a></li>
</ul>
</li>
</ul>
</div>
            </div>
        </div>
    </div>

    <div class="footer-apps">
        <div class="app-fill">
            <div class="footer-apps-wrapper">
                <div class="plugin-mobile-icons">
            <a class="mobile-link"
           href=""
           style="background-image: url(http://localhost/mix/wp-content/uploads/2018/10/google-play.png)">
        </a>
            <a class="mobile-link"
           href=""
           style="background-image: url(http://localhost/mix/wp-content/uploads/2018/10/app-store.png)">
        </a>
    
</div>
            </div>
        </div>
    </div>

    <div class="footer-rights">
        <div class="app-fill">
            <div class="footer-rights-wrapper">
                <div class="plugin-copy-rights">
    <p>© 215 – 2018, Mix-Trip — дешевые авиабилеты</p>
</div>
            </div>
        </div>
    </div>
</div><?php }
}
