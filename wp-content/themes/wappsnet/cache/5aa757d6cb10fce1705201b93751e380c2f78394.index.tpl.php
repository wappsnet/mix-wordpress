<?php
/* Smarty version 3.1.30, created on 2018-07-30 02:13:43
  from "C:\xampp\htdocs\cordyceps1\wp-content\themes\wappsnet\modules\popularposts\index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b5e7457377659_18663937',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '3fb5bea35bd9ce824b5c9dbe8cff02bd4646ebfa' => 
    array (
      0 => 'C:\\xampp\\htdocs\\cordyceps1\\wp-content\\themes\\wappsnet\\modules\\popularposts\\index.tpl',
      1 => 1532384797,
      2 => 'file',
    ),
  ),
  'cache_lifetime' => 86400,
),true)) {
function content_5b5e7457377659_18663937 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="module-popular-posts useful">
    <div class="app-fill1200">
        <div class="popular-block">
            <h2 class="popular-title">
                <span>Полезные статьи</span>
            </h2>
            <div class="popular-content">
                <div class="plugin-carousel">
    <div class="carousel-block">
                    <div class="carousel-item">
                <div class="plugin-post">
    <a href="http://localhost/cordyceps1/2018/07/30/%d1%87%d1%82%d0%be-%d1%82%d0%b0%d0%ba%d0%be%d0%b5-%d0%ba%d0%be%d1%80%d0%b4%d0%b8%d1%86%d0%b5%d0%bf%d1%81/"
       class="post-link">
        <div class="post-media">
                            <img src="http://localhost/cordyceps1/wp-content/uploads/2018/07/53a0350180a49_middle.png"/>
                    </div>
    </a>

    <a href="http://localhost/cordyceps1/2018/07/30/%d1%87%d1%82%d0%be-%d1%82%d0%b0%d0%ba%d0%be%d0%b5-%d0%ba%d0%be%d1%80%d0%b4%d0%b8%d1%86%d0%b5%d0%bf%d1%81/"
       class="post-link">
        <div class="post-title post-info-wrapper">
            Что такое кордицепс?
        </div>

        <div class="post-date post-info-wrapper">
            2018-07-30 03:33:11
        </div>

        <div class="post-content post-info-wrapper">
            В современном мире экология оставляет желать лучшего. Каждый час в небо поднимается огромное колличество тяжелых металлов, выпускаемых машинами, каждые сутки в воду выбрасываются десятки, а порой и сотни...
        </div>
    </a>
</div>
            </div>
                    <div class="carousel-item">
                <div class="plugin-post">
    <a href="http://localhost/cordyceps1/2018/07/30/%d0%b8%d1%81%d1%82%d0%be%d1%80%d0%b8%d1%87%d0%b5%d1%81%d0%ba%d0%b0%d1%8f-%d1%81%d0%bf%d1%80%d0%b0%d0%b2%d0%ba%d0%b0-%d0%be-%d0%ba%d0%be%d1%80%d0%b4%d0%b8%d1%86%d0%b5%d0%bf%d1%81%d0%b5/"
       class="post-link">
        <div class="post-media">
                            <img src="http://localhost/cordyceps1/wp-content/uploads/2018/07/54317879d74ba_middle.png"/>
                    </div>
    </a>

    <a href="http://localhost/cordyceps1/2018/07/30/%d0%b8%d1%81%d1%82%d0%be%d1%80%d0%b8%d1%87%d0%b5%d1%81%d0%ba%d0%b0%d1%8f-%d1%81%d0%bf%d1%80%d0%b0%d0%b2%d0%ba%d0%b0-%d0%be-%d0%ba%d0%be%d1%80%d0%b4%d0%b8%d1%86%d0%b5%d0%bf%d1%81%d0%b5/"
       class="post-link">
        <div class="post-title post-info-wrapper">
            Историческая справка о кордицепсе
        </div>

        <div class="post-date post-info-wrapper">
            2018-07-30 03:25:26
        </div>

        <div class="post-content post-info-wrapper">
            Китай – страна древнейшей культуры, истоки которой прослеживаются от 2852 г. До нашей эры. С тех пор сменилось 12 крупных династий и много малоизвестных королевских семейств. В течении этого длительного...
        </div>
    </a>
</div>
            </div>
                    <div class="carousel-item">
                <div class="plugin-post">
    <a href="http://localhost/cordyceps1/2018/07/30/%d0%bd%d0%b0%d1%83%d1%87%d0%bd%d0%be-%d0%bf%d1%80%d0%b0%d0%ba%d1%82%d0%b8%d1%87%d0%b5%d1%81%d0%ba%d0%b8%d0%b9-%d0%be%d1%82%d1%87%d0%b5%d1%82/"
       class="post-link">
        <div class="post-media">
                            <img src="http://localhost/cordyceps1/wp-content/uploads/2018/07/53a18f5359805_middle.png"/>
                    </div>
    </a>

    <a href="http://localhost/cordyceps1/2018/07/30/%d0%bd%d0%b0%d1%83%d1%87%d0%bd%d0%be-%d0%bf%d1%80%d0%b0%d0%ba%d1%82%d0%b8%d1%87%d0%b5%d1%81%d0%ba%d0%b8%d0%b9-%d0%be%d1%82%d1%87%d0%b5%d1%82/"
       class="post-link">
        <div class="post-title post-info-wrapper">
            Научно-практический отчет
        </div>

        <div class="post-date post-info-wrapper">
            2018-07-30 03:23:57
        </div>

        <div class="post-content post-info-wrapper">
            «Король-кордицепс Ликэ» стал «знаменитой маркой Китая» и современным оздоровительным средством, перешагнувшим рубеж тысячелетий. В его основе лежи ; теория и опыт 5000-летней китайской медицины, «учение о...
        </div>
    </a>
</div>
            </div>
                    <div class="carousel-item">
                <div class="plugin-post">
    <a href="http://localhost/cordyceps1/2018/07/28/%d0%b7%d0%b0%d0%b1%d0%be%d0%bb%d0%b5%d0%b2%d0%b0%d0%bd%d0%b8%d1%8f-%d1%81%d0%b5%d1%80%d0%b4%d1%86%d0%b0-%d0%bb%d0%b5%d1%87%d0%b5%d0%bd%d0%b8%d0%b5/"
       class="post-link">
        <div class="post-media">
                            <img src="http://localhost/cordyceps1/wp-content/uploads/2018/07/u3d0sCb41Ug.jpg"/>
                    </div>
    </a>

    <a href="http://localhost/cordyceps1/2018/07/28/%d0%b7%d0%b0%d0%b1%d0%be%d0%bb%d0%b5%d0%b2%d0%b0%d0%bd%d0%b8%d1%8f-%d1%81%d0%b5%d1%80%d0%b4%d1%86%d0%b0-%d0%bb%d0%b5%d1%87%d0%b5%d0%bd%d0%b8%d0%b5/"
       class="post-link">
        <div class="post-title post-info-wrapper">
            ЗАБОЛЕВАНИЯ СЕРДЦА ЛЕЧЕНИЕ
        </div>

        <div class="post-date post-info-wrapper">
            2018-07-28 16:55:33
        </div>

        <div class="post-content post-info-wrapper">
            В традиционной китайской медицине Кордицепс называют лечебным растением, которое может поступать в меридианы почек и легких. Считается, что Кордицепс защищает почки и восстанавливает легкие. Китайские ученые...
        </div>
    </a>
</div>
            </div>
        
    </div>
</div>
            </div>
        </div>
    </div>
</div><?php }
}
