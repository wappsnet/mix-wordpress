<?php
/* Smarty version 3.1.30, created on 2018-07-12 22:06:58
  from "C:\xampp\htdocs\shakti\wp-content\themes\wappsnet\modules\menu\mobile.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b47d1021a9aa9_85651974',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'dfe5dd6fd777c5049a2e41945ba8be3036909878' => 
    array (
      0 => 'C:\\xampp\\htdocs\\shakti\\wp-content\\themes\\wappsnet\\modules\\menu\\mobile.tpl',
      1 => 1529895679,
      2 => 'file',
    ),
  ),
  'cache_lifetime' => 86400,
),true)) {
function content_5b47d1021a9aa9_85651974 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="module-menu mobile-menu">
    <div class="menu-box">
        <div class="app-fill1200">
            <div class="menu-wrapper">
                <div id="side-menu-toggle"
                     data-activates="mobile-side-menu"
                     class="menu-tool-item">
                    <span class="tool-icon"><i class="fa fa-bars"></i></span>
                </div>

                <ul class="menu">
                                            <li class="menu-item">
                            <a href="http://shakti-food.esy.es"
                               class="meu-item-link">
                                <span class="menu-name">Главная</span>
                            </a>
                        </li>
                                            <li class="menu-item">
                            <a href="http://localhost/shakti/category/catalog/"
                               class="meu-item-link">
                                <span class="menu-name">Каталог</span>
                            </a>
                        </li>
                                            <li class="menu-item">
                            <a href="http://localhost/shakti/category/catalog/news-items/"
                               class="meu-item-link">
                                <span class="menu-name">Новинки</span>
                            </a>
                        </li>
                                            <li class="menu-item">
                            <a href="http://localhost/shakti/category/catalog/bestsellers/"
                               class="meu-item-link">
                                <span class="menu-name">Распродажа</span>
                            </a>
                        </li>
                                            <li class="menu-item">
                            <a href="http://localhost/shakti/archive/ioga/"
                               class="meu-item-link">
                                <span class="menu-name">Йога</span>
                            </a>
                        </li>
                                            <li class="menu-item">
                            <a href="http://localhost/shakti/archive/raw%d0%b2%d0%b5%d0%b3%d0%b0%d0%bd-%d0%b2%d1%81%d1%82%d1%80%d0%b5%d1%87%d0%b8/"
                               class="meu-item-link">
                                <span class="menu-name">RawВеган встречи</span>
                            </a>
                        </li>
                                            <li class="menu-item">
                            <a href="http://localhost/shakti/archive/%d0%b3%d0%b0%d0%bb%d0%b5%d1%80%d0%b5%d1%8f/"
                               class="meu-item-link">
                                <span class="menu-name">Галерея</span>
                            </a>
                        </li>
                                            <li class="menu-item">
                            <a href="http://localhost/shakti/%d0%ba%d0%be%d0%bd%d1%82%d0%b0%d0%ba%d1%82%d1%8b/"
                               class="meu-item-link">
                                <span class="menu-name">Контакты</span>
                            </a>
                        </li>
                                            <li class="menu-item">
                            <a href="http://localhost/shakti/%d0%bc%d0%b5%d1%80%d0%be%d0%bf%d1%80%d0%b8%d1%8f%d1%82%d0%b8%d1%8f/"
                               class="meu-item-link">
                                <span class="menu-name">О нас</span>
                            </a>
                        </li>
                    
                </ul>

            </div>
        </div>
    </div>
</div>

<div id="mobile-side-menu"
     class="side-nav app-side-nav side-menu-box">
    <div class="app-side-logo">
        <img src="http://localhost/shakti/wp-content/uploads/2018/07/logo-for-site2.png"/>
    </div>
    <div class="app-side-wrapper">
        <div class="side-menu-list">
            <ul id="menu-collapsible"
                class="collapsible side-menu-items">
                                    <li class="side-menu-item">
                                                    <a href="http://shakti-food.esy.es"
                               class="collapsible-header side-menu-link">
                                <span class="menu-icon"><i class=""></i></span>
                                <span class="menu-name">Главная</span>
                            </a>
                                            </li>
                                    <li class="side-menu-item">
                                                    <a href="http://localhost/shakti/category/catalog/"
                               class="collapsible-header side-menu-link">
                                <span class="menu-icon"><i class=""></i></span>
                                <span class="menu-name">Каталог</span>
                            </a>
                                            </li>
                                    <li class="side-menu-item">
                                                    <a href="http://localhost/shakti/category/catalog/news-items/"
                               class="collapsible-header side-menu-link">
                                <span class="menu-icon"><i class=""></i></span>
                                <span class="menu-name">Новинки</span>
                            </a>
                                            </li>
                                    <li class="side-menu-item">
                                                    <a href="http://localhost/shakti/category/catalog/bestsellers/"
                               class="collapsible-header side-menu-link">
                                <span class="menu-icon"><i class=""></i></span>
                                <span class="menu-name">Распродажа</span>
                            </a>
                                            </li>
                                    <li class="side-menu-item">
                                                    <a href="http://localhost/shakti/archive/ioga/"
                               class="collapsible-header side-menu-link">
                                <span class="menu-icon"><i class=""></i></span>
                                <span class="menu-name">Йога</span>
                            </a>
                                            </li>
                                    <li class="side-menu-item">
                                                    <a href="http://localhost/shakti/archive/raw%d0%b2%d0%b5%d0%b3%d0%b0%d0%bd-%d0%b2%d1%81%d1%82%d1%80%d0%b5%d1%87%d0%b8/"
                               class="collapsible-header side-menu-link">
                                <span class="menu-icon"><i class=""></i></span>
                                <span class="menu-name">RawВеган встречи</span>
                            </a>
                                            </li>
                                    <li class="side-menu-item">
                                                    <a href="http://localhost/shakti/archive/%d0%b3%d0%b0%d0%bb%d0%b5%d1%80%d0%b5%d1%8f/"
                               class="collapsible-header side-menu-link">
                                <span class="menu-icon"><i class=""></i></span>
                                <span class="menu-name">Галерея</span>
                            </a>
                                            </li>
                                    <li class="side-menu-item">
                                                    <a href="http://localhost/shakti/%d0%ba%d0%be%d0%bd%d1%82%d0%b0%d0%ba%d1%82%d1%8b/"
                               class="collapsible-header side-menu-link">
                                <span class="menu-icon"><i class=""></i></span>
                                <span class="menu-name">Контакты</span>
                            </a>
                                            </li>
                                    <li class="side-menu-item">
                                                    <a href="http://localhost/shakti/%d0%bc%d0%b5%d1%80%d0%be%d0%bf%d1%80%d0%b8%d1%8f%d1%82%d0%b8%d1%8f/"
                               class="collapsible-header side-menu-link">
                                <span class="menu-icon"><i class=""></i></span>
                                <span class="menu-name">О нас</span>
                            </a>
                                            </li>
                
            </ul>
        </div>
    </div>
</div>
<?php }
}
