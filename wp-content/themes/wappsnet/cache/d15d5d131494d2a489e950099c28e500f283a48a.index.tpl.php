<?php
/* Smarty version 3.1.30, created on 2018-07-23 20:57:33
  from "C:\xampp\htdocs\shakti\wp-content\themes\wappsnet\plugins\cart\index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b56413d32b937_70325038',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '7073a3284c46ea1a5d3908669ea5b70d0cd0d307' => 
    array (
      0 => 'C:\\xampp\\htdocs\\shakti\\wp-content\\themes\\wappsnet\\plugins\\cart\\index.tpl',
      1 => 1532206882,
      2 => 'file',
    ),
  ),
  'cache_lifetime' => 86400,
),true)) {
function content_5b56413d32b937_70325038 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="cart-plugin">
            <div class="cart-items-wrapper">
            <table class="cart-items-table">
                <thead class="cart-items-header">
                <tr>
                    <th class="cart-item-info">
                        Продукт
                    </th>
                    <th class="cart-item-count">
                        Количество
                    </th>
                    <th class="cart-item-price">
                        Цена
                    </th>
                    <th class="cart-item-amount">
                        Сумма
                    </th>
                </tr>
                </thead>

                <tbody class="cart-items-body">
                                    <tr>
                        <td class="cart-item-info">
                            <div class="cart-item-info-wrapper">
                                <button data-item-id="321"
                                        class="cart-item-delete">
                                    <i class="fa fa-trash"></i>
                                </button>

                                <img class="cart-item-image"
                                     src="http://localhost/shakti/wp-content/uploads/2018/07/4.jpg"/>

                                <a href="http://localhost/shakti/products/321/"
                                   class="cart-item-title">
                                    Nature's Way, Корень Fo-Ti, 610 мг
                                </a>
                            </div>
                        </td>

                        <td class="cart-item-count">
                            <input type="number"
                                   min="1"
                                   max="100"
                                   value="1"
                                   data-item-id="321"
                                   class="cart-item-count-value"/>
                        </td>

                        <td class="cart-item-price">
                            <div class="cart-item-new-price">
                                <span>3800</span>
                                <span>RUB</span>
                            </div>

                                                    </td>

                        <td class="cart-item-amount">
                            <div class="cart-item-amount-value">
                                <span>3800</span>
                                <span>RUB</span>
                            </div>
                        </td>
                    </tr>
                                    <tr>
                        <td class="cart-item-info">
                            <div class="cart-item-info-wrapper">
                                <button data-item-id="318"
                                        class="cart-item-delete">
                                    <i class="fa fa-trash"></i>
                                </button>

                                <img class="cart-item-image"
                                     src="http://localhost/shakti/wp-content/uploads/2018/07/16.jpg"/>

                                <a href="http://localhost/shakti/products/mrm-%d0%ba%d0%be%d1%80%d0%b4%d0%b8%d1%86%d0%b5%d0%bf%d1%81-%d1%88%d1%82%d0%b0%d0%bc%d0%bc-cs-4/"
                                   class="cart-item-title">
                                    MRM, Кордицепс, штамм CS-4
                                </a>
                            </div>
                        </td>

                        <td class="cart-item-count">
                            <input type="number"
                                   min="1"
                                   max="100"
                                   value="1"
                                   data-item-id="318"
                                   class="cart-item-count-value"/>
                        </td>

                        <td class="cart-item-price">
                            <div class="cart-item-new-price">
                                <span>3500</span>
                                <span>RUB</span>
                            </div>

                                                    </td>

                        <td class="cart-item-amount">
                            <div class="cart-item-amount-value">
                                <span>3500</span>
                                <span>RUB</span>
                            </div>
                        </td>
                    </tr>
                
                </tbody>
            </table>
        </div>

        <div class="cart-items-footer">
            <span>Сумма покупок:</span>
            <span>7300</span>
            <span>RUB</span>
        </div>

        <div class="cart-buttons-box">
            <a href="http://localhost/shakti/profile/order"
               class="waves-effect waves-light btn cart-button cart-get-order">
                Оформить заказ
            </a>
            <a href="http://localhost/shakti/"
               class="waves-effect waves-light btn-flat cart-button cart-return-site">
                Обновить корзину
            </a>
        </div>
    </div><?php }
}
