<?php
/* Smarty version 3.1.30, created on 2018-07-01 23:24:21
  from "C:\xampp\htdocs\shakti\wp-content\themes\wappsnet\plugins\changepassword\index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b3962a57b5f19_38299589',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '441d4405fceceeeedf9391f13603115223beb8f5' => 
    array (
      0 => 'C:\\xampp\\htdocs\\shakti\\wp-content\\themes\\wappsnet\\plugins\\changepassword\\index.tpl',
      1 => 1529892462,
      2 => 'file',
    ),
  ),
  'cache_lifetime' => 86400,
),true)) {
function content_5b3962a57b5f19_38299589 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="plugin-change-password">
    <div class="auth-item">
        <div id="auth-info-messages"
             class="auth-info-messages">
        </div>

        <div class="auth-field">
            <div class="auth-icon">
                <i class="fa fa-key"></i>
            </div>
            <input class="auth-input user_change_password"
                   id="user_change_password_old_password"
                   name="old_password"
                   data-type="password"
                   value=""
                   placeholder="Старый пароль"
                   type="password"/>
        </div>
        <div class="auth-field">
            <div class="auth-icon">
                <i class="fa fa-key"></i>
            </div>
            <input class="auth-input user_change_password"
                   id="user_change_password_last_name"
                   name="new_password"
                   data-type="password"
                   value=""
                   placeholder="Новый пароль"
                   type="password"/>
        </div>

        <input class="auth-input user_change_password"
               id="user_save_id"
               type="hidden"
               name="user_id"
               data-type="hidden"
               value="1"/>

        <div class="auth-footer">
            <button id="user-change-password"
                    class="auth-button waves-effect waves-light btn">
                Сохранить
            </button>
        </div>
    </div>
</div><?php }
}
