<?php
/* Smarty version 3.1.30, created on 2018-07-30 02:10:22
  from "C:\xampp\htdocs\cordyceps1\wp-content\themes\wappsnet\modules\singleproduct\index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b5e738e6e7559_31874920',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '3099c020e95eadc6de336baa07467bd5c6e000d8' => 
    array (
      0 => 'C:\\xampp\\htdocs\\cordyceps1\\wp-content\\themes\\wappsnet\\modules\\singleproduct\\index.tpl',
      1 => 1530654142,
      2 => 'file',
    ),
  ),
  'cache_lifetime' => 86400,
),true)) {
function content_5b5e738e6e7559_31874920 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="module-single-product">
    <div class="app-fill1200">
        <div class="single-product-wrapper">
            <div class="app-row">
                <div class="app-column large-6 middle-6 small-12">
                    <div class="product-images">
                        <div class="plugin-scale-images">
            <div class="top-image materialboxed"
             style="background-image: url('http://localhost/cordyceps1/wp-content/uploads/2018/07/front-2-2-1.jpg')">
        </div>
    
            <div class="thumbnail-images">
                            <div class="thumbnail-item"
                     data-src="http://localhost/cordyceps1/wp-content/uploads/2018/07/front-2-2-1.jpg"
                     style="background-image: url('http://localhost/cordyceps1/wp-content/uploads/2018/07/front-2-2-1.jpg')">
                </div>
                            <div class="thumbnail-item"
                     data-src="http://localhost/cordyceps1/wp-content/uploads/2018/07/001.jpg"
                     style="background-image: url('http://localhost/cordyceps1/wp-content/uploads/2018/07/001.jpg')">
                </div>
            
        </div>
    </div>
                    </div>
                </div>

                <div class="app-column large-6 middle-6 small-12">
                    <div class="product-info">
                        <div class="single-product-header">
                            <h2 class="product-title">
                                Серебряный кордицепс
                            </h2>
                        </div>

                        
                        
                        <div class="product-info-section product-new-price">
                            <span class="app-right-5">4500</span>
                            <span class="app-right-5">RUB</span>
                        </div>

                        <div class="product-info-section product-cart">
                            <input class="product-cart-count"
                                   value="1"
                                   min="1"
                                   max="100"
                                   id="product-cart-count"
                                   name="product_count"
                                   type="number"/>
                            <button id="product-cart-button"
                                    data-animate-id="product-cart-count"
                                    data-item-id="34"
                                    class="product-cart-button btn btn-flat waves-effect">
                                <span class="app-right-5"></span>
                                <span><i class="fa fa-shopping-cart"></i></span>
                            </button>
                        </div>

                        <div class="product-info-section product-category">
                            <div class="label">
                                
                            </div>
                            <a href="http://localhost/cordyceps1/%d0%ba%d0%be%d0%bd%d1%82%d0%b0%d0%ba%d1%82%d1%8b/"
                               class="value">
                                Каталог
                            </a>
                        </div>

                        <div class="product-info-section product-brand">
                            <div class="label">
                                
                            </div>
                            <div class="value">
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="product-additional-info">
                <div class="tab-header">
                    
                </div>
                <div class="tab-content">
                    <div class="product-description">
                        В Тибете и Китае Кордицепс Синенсис является неотъемлемой частью традиционной медицины уже на протяжении более 5000 лет. <a href="http://cordycepsinensis.ru/cordycepsinensis.ru/chto-takoe-korditseps/istoriya-izucheniya-korditsepsa/">История изучения кордицепса</a> начинается с 620 г.н.э.
<a href="http://cordycepsinensis.ru/cordycepsinensis.ru/chto-takoe-korditseps/">Что такое Кордицепс</a>? Это ценнейший гриб, <a href="http://cordycepsinensis.ru/cordycepsinensis.ru/chto-takoe-korditseps/ne-imeet-analogov/">не имеет аналогов</a> по своей форме и свойствам в природе! Поражает <a href="http://cordycepsinensis.ru/cordycepsinensis.ru/chto-takoe-korditseps/biohimicheskiy-sostav-korditsepsa/">биохимический состав Кордицепса</a>. В нем содержится 80 видов ферментов. Это самое огромное количество, которое может существовать в растении или животном! Кордицепс растет всего в некоторых районах гималайских гор, на высотах 3500-6000 метров. <a href="http://cordycepsinensis.ru/cordycepsinensis.ru/chto-takoe-korditseps/sbor-korditsepsa/">Сбор натурального гриба</a> проходит каждую весну. Этот очень трудоемкий и тяжелый процесс порой осуществляется ценой жизни!
В последнее десятилетие Кордицепс стал широко известен во всем мире и приобрел <a href="http://cordycepsinensis.ru/cordycepsinensis.ru/chto-takoe-korditseps/populyarnost-sredi-sportsmenov/">популярность среди спортсменов</a>. Рост такой популярности усилил спекулятивный эффект на рынке! В аптеках появились Био-Активные Добавки с кордицепсом. Но в чем же <a href="http://cordycepsinensis.ru/cordycepsinensis.ru/chto-takoe-korditseps/otlichie-naturalnogo-griba-ot-badov/">отличие натурального гриба от БАДов</a>? Все БАДы имеют мизерное массовое содержание самого Кордицепса. Почти все БАДы производятся промышленным способом в фабричных, лабораторных условиях, а действие искусственного кордицепса на человека пока не изучены. <a href="http://cordycepsinensis.ru/cordycepsinensis.ru/chto-takoe-korditseps/kak-otlichit-korditseps/">Отличается Кордицепс от подделки</a> по внешнему виду.
Проведенные научные исследования показывают, что гриб Кордицепс оказывает на человека всестороннее лечебное и восстановительное действие! У натурального гриба Кордицепса нет побочных действий и он является гипоаллергенным! Кордицепс эффективен в лечении заболеваний: Рак, ВИЧ, Сахарный диабет, Туберкулез, Псориаз, Ожирение, Бесплодие, Генетические заболевания, Аллергия. Особую значимость имеет <a href="http://cordycepsinensis.ru/cordycepsinensis.ru/chto-takoe-korditseps/protivoopuholevoe-deystvie-korditsepsa/">противоопухолевое действие натурального гриба</a>, так как он имеет несколько механизмов, влияющих на опухоль! Кордицепс также является сильнейшим иммуномодулятором! Достаточно всего одного курса Кордицепса, чтобы повысить иммунитет на год вперед! <a href="http://cordycepsinensis.ru/cordycepsinensis.ru/lechebnyie-kursyi/">Лечебные курсы</a> составляют от 21 до 63 дней. <a href="http://cordycepsinensis.ru/cordycepsinensis.ru/sposobyi-primeneniya/">Способы применения</a> Кордицепса зависят от массы тела и от тяжести заболевания. На 21-дневный курс необходимо около 10 грамм.
Кордицепс Синенсис непревзойдённое оздоровительное средство, которое приводит к <a href="http://cordycepsinensis.ru/cordycepsinensis.ru/chto-takoe-korditseps/vosstanovlenie-zhiznennoy-energii/">восстановлению жизненной энергии</a>!
                    </div>
                </div>

                <div class="tab-header">
                    
                </div>
                <div class="tab-content">
                    <div class="product-characters">
                        
                    </div>
                </div>

                <div class="tab-header">
                    
                </div>
                <div class="tab-content">
                    <div class="product-comments">
                        <div class="comments-plugin">
    <div class="create-comment">
        <div class="auth-item">
            <div id="auth-info-messages"
                 class="auth-info-messages">
            </div>

            <div class="auth-field">
                <div class="auth-icon">
                    <i class="fa fa-user"></i>
                </div>
                <input class="auth-input user_comment"
                       id="user_comment_name"
                       name="name"
                       data-type="name"
                       placeholder=""
                       type="text"/>
            </div>

            <div class="auth-field">
                <div class="auth-icon">
                    <i class="fa fa-envelope"></i>
                </div>
                <input class="auth-input user_comment"
                       id="user_comment_email"
                       name="email"
                       data-type="email"
                       placeholder=""
                       type="email"/>
            </div>

            <div class="auth-field auth-text">
                <div class="auth-icon">
                    <i class="fa fa-book"></i>
                </div>
                <textarea class="auth-input user_comment"
                          id="user_comment_text"
                          name="text"
                          data-type="text"
                          placeholder="">
                </textarea>
            </div>

            <input class="auth-input user_comment"
                   id="user_comment_post_id"
                   type="hidden"
                   data-type="hidden"
                   value="34"
                   name="post_id">

            <div class="auth-footer">
                <button id="user-comment"
                        class="auth-button waves-effect waves-light btn">
                    
                </button>
            </div>
        </div>
    </div>

            <div class="app-rel app-box empty-comments">
            
        </div>
    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><?php }
}
