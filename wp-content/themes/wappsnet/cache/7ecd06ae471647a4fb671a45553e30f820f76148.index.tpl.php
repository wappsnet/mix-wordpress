<?php
/* Smarty version 3.1.30, created on 2018-07-23 23:44:38
  from "C:\xampp\htdocs\shakti\wp-content\themes\wappsnet\modules\profile\index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b566866c3eab6_24862770',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '2305ff5c29703e2884569e1cbeac8f71e43b0dd4' => 
    array (
      0 => 'C:\\xampp\\htdocs\\shakti\\wp-content\\themes\\wappsnet\\modules\\profile\\index.tpl',
      1 => 1530492304,
      2 => 'file',
    ),
  ),
  'cache_lifetime' => 86400,
),true)) {
function content_5b566866c3eab6_24862770 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="module-profile">
    <div class="app-fill1200">
        <div class="profile-wrapper app-wrapper">
            <div class="app-row">
                <div class="app-column large-3 middle-3 small-12">
                    <div class="profile-menu">
                                                    <div class="profile-menu-item">
                                <a href="http://localhost/shakti/profile/likes/"
                                   data-action=""
                                   class="profile-menu-link ">
                                    <span class="icon"><i class="fa fa-heart"></i></span>
                                    <span class="title">Список нравится</span>
                                </a>
                            </div>
                                                    <div class="profile-menu-item">
                                <a href="http://localhost/shakti/profile/basket/"
                                   data-action=""
                                   class="profile-menu-link ">
                                    <span class="icon"><i class="fa fa-shopping-cart"></i></span>
                                    <span class="title">Корзина покупок</span>
                                </a>
                            </div>
                                                    <div class="profile-menu-item">
                                <a href="http://localhost/shakti/profile/cabinet/"
                                   data-action=""
                                   class="profile-menu-link ">
                                    <span class="icon"><i class="fa fa-user-circle"></i></span>
                                    <span class="title">Мои данные</span>
                                </a>
                            </div>
                                                    <div class="profile-menu-item">
                                <a href="http://localhost/shakti/profile/%d0%b8%d0%b7%d0%bc%d0%b5%d0%bd%d0%b8%d1%82%d1%8c-%d0%bf%d0%b0%d1%80%d0%be%d0%bb%d1%8c/"
                                   data-action=""
                                   class="profile-menu-link ">
                                    <span class="icon"><i class="fa fa-key"></i></span>
                                    <span class="title">Изменить пароль</span>
                                </a>
                            </div>
                                                    <div class="profile-menu-item">
                                <a href="http://localhost/shakti/profile/%d0%b8%d1%81%d1%82%d0%be%d1%80%d0%b8%d1%8f-%d0%b7%d0%b0%d0%ba%d0%b0%d0%b7%d0%be%d0%b2/"
                                   data-action=""
                                   class="profile-menu-link ">
                                    <span class="icon"><i class="fa fa-history"></i></span>
                                    <span class="title">История заказов</span>
                                </a>
                            </div>
                                                    <div class="profile-menu-item">
                                <a href="http://localhost/shakti/profile/logout/"
                                   data-action="user_logout"
                                   class="profile-menu-link ">
                                    <span class="icon"><i class="fa fa-sign-out"></i></span>
                                    <span class="title">Выйти</span>
                                </a>
                            </div>
                        
                    </div>
                </div>

                <div class="app-column large-9 middle-9 small-12">
                    <div class="profile-content-wrapper">
                        <div class="profile-title">
                            Оформление заказа
                        </div>
                                                    <div class="profile-plugin-wrapper">
                                <div class="plugin-create-order">
    <div class="create-order-section"
         id="order-create-form">
        <div class="auth-item">
            <div id="auth-info-messages"
                 class="auth-info-messages">
            </div>

            <label for="user_create_order_email"
                   class="auth-label">
                <span>Емайл адрес</span>
            </label>
            <div class="auth-field">
                <input class="auth-input user_create_order"
                       id="user_create_order_email"
                       name="user_email"
                       data-type="email"
                       value="mike-tm555@mail.ru"
                       type="email"/>
            </div>

            <label for="user_create_order_name"
                   class="auth-label">
                <span>Имя Фамилия</span>
            </label>
            <div class="auth-field">
                <input class="auth-input user_create_order"
                       id="user_create_order_name"
                       name="user_name"
                       data-type="text"
                       value="Mike Tevan"
                       type="text"/>
            </div>

            <label for="user_create_order_name"
                   class="auth-label">
                <span>Адрес доставки</span>
            </label>
            <div class="auth-field">
                <input class="auth-input user_create_order"
                       id="user_create_order_delivery_address"
                       name="delivery_address"
                       value="950 Ridge RD C25, P2763"
                       data-type="text"
                       type="text"/>
            </div>

            <label for="user_create_order_name"
                   class="auth-label">
                <span>Регион</span>
            </label>
            <div class="auth-field">
                <select class="auth-input user_create_order"
                        id="user_create_order_delivery_region"
                        name="delivery_region"
                        data-type="select">
                                                                        <option value="48">
                                Москва
                            </option>
                                                                                                <option value="52">
                                Санкт-Петербург
                            </option>
                                                                                                <option value="53">
                                Другие регионы Россия
                            </option>
                                            
                </select>
            </div>

            <label for="user_create_order_name"
                   class="auth-label">
                <span>Почтовый индекс</span>
            </label>
            <div class="auth-field">
                <input class="auth-input user_create_order"
                       id="user_create_order_delivery_post_index"
                       name="delivery_post_index"
                       data-type="text"
                       type="text"/>
            </div>

            <label for="user_create_order_name"
                   class="auth-label">
                <span>Телефон</span>
            </label>
            <div class="auth-field">
                <input class="auth-input user_create_order"
                       id="user_create_order_phone"
                       name="user_phone"
                       data-type="phone"
                       value="+1 83024148567"
                       type="tel"/>
            </div>

            <input class="auth-input user_create_order"
                   id="user_create_order_user_id"
                   type="hidden"
                   name="user_id"
                   data-type="hidden"
                   value="1"/>

            <div class="auth-footer">
                <button id="user-create-order"
                        class="auth-button create-order waves-effect waves-light btn">
                    Продолжить
                </button>
            </div>
        </div>
    </div>

    <div class="create-order-section hidden"
         id="order-confirm-form">
    </div>
</div>
                            </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><?php }
}
