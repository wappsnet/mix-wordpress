<?php
/* Smarty version 3.1.30, created on 2018-07-30 02:13:42
  from "C:\xampp\htdocs\cordyceps1\wp-content\themes\wappsnet\modules\popularproducts\index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b5e7456b9d285_86483153',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c8afe7b88ea838783a058854faf57b1f3061290f' => 
    array (
      0 => 'C:\\xampp\\htdocs\\cordyceps1\\wp-content\\themes\\wappsnet\\modules\\popularproducts\\index.tpl',
      1 => 1532384797,
      2 => 'file',
    ),
  ),
  'cache_lifetime' => 86400,
),true)) {
function content_5b5e7456b9d285_86483153 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="module-popular-products popular">
    <div class="app-fill1200">
        <div class="popular-block">
            <h2 class="popular-title">
                <span>Грибы кордицепс</span>
            </h2>
            <div class="popular-content">
                <div class="plugin-carousel">
    <div class="carousel-block">
                    <div class="carousel-item">
                <div class="product-plugin">
    <a href="http://localhost/cordyceps1/products/%d0%b7%d0%be%d0%bb%d0%be%d1%82%d0%be%d0%b9-%d0%ba%d0%be%d1%80%d0%b4%d0%b8%d1%86%d0%b5%d0%bf%d1%81-3/"
       class="item-image">

        <div class="item-props">
                    </div>

        <div class="product-cover">
            <div class="product-image"
                 style="background-image: url('http://localhost/cordyceps1/wp-content/uploads/2018/07/front-2-2-1.jpg')">
            </div>
        </div>

        <h4 class="product-name">
            Золотой кордицепс
        </h4>
    </a>

    <div class="item-price">
        <div class="item-real-price">
                            <div class="price-block">
                    <span class="price-value">4 000</span>
                    <span class="price-key">RUB</span>
                </div>
            
                    </div>
    </div>

    <div class="item-tools">
        <div id="item-cart-35"
             data-animate-id="item-cart-35"
             data-item-id="35"
             class="simple-tools-item cart-tool item-tool">
            <i class="fa fa-shopping-cart"></i>
        </div>
    </div>
</div>
            </div>
                    <div class="carousel-item">
                <div class="product-plugin">
    <a href="http://localhost/cordyceps1/products/%d0%b7%d0%be%d0%bb%d0%be%d1%82%d0%be%d0%b9-%d0%ba%d0%be%d1%80%d0%b4%d0%b8%d1%86%d0%b5%d0%bf%d1%81-2/"
       class="item-image">

        <div class="item-props">
                    </div>

        <div class="product-cover">
            <div class="product-image"
                 style="background-image: url('http://localhost/cordyceps1/wp-content/uploads/2018/07/front-2-2-1.jpg')">
            </div>
        </div>

        <h4 class="product-name">
            Серебряный кордицепс
        </h4>
    </a>

    <div class="item-price">
        <div class="item-real-price">
                            <div class="price-block">
                    <span class="price-value">4 500</span>
                    <span class="price-key">RUB</span>
                </div>
            
                    </div>
    </div>

    <div class="item-tools">
        <div id="item-cart-34"
             data-animate-id="item-cart-34"
             data-item-id="34"
             class="simple-tools-item cart-tool item-tool">
            <i class="fa fa-shopping-cart"></i>
        </div>
    </div>
</div>
            </div>
                    <div class="carousel-item">
                <div class="product-plugin">
    <a href="http://localhost/cordyceps1/products/%d0%b7%d0%be%d0%bb%d0%be%d1%82%d0%be%d0%b9-%d0%ba%d0%be%d1%80%d0%b4%d0%b8%d1%86%d0%b5%d0%bf%d1%81/"
       class="item-image">

        <div class="item-props">
                    </div>

        <div class="product-cover">
            <div class="product-image"
                 style="background-image: url('http://localhost/cordyceps1/wp-content/uploads/2018/07/001.jpg')">
            </div>
        </div>

        <h4 class="product-name">
            Бронзовый кордицепс
        </h4>
    </a>

    <div class="item-price">
        <div class="item-real-price">
                            <div class="price-block">
                    <span class="price-value">5 000</span>
                    <span class="price-key">RUB</span>
                </div>
            
                    </div>
    </div>

    <div class="item-tools">
        <div id="item-cart-31"
             data-animate-id="item-cart-31"
             data-item-id="31"
             class="simple-tools-item cart-tool item-tool">
            <i class="fa fa-shopping-cart"></i>
        </div>
    </div>
</div>
            </div>
        
    </div>
</div>
            </div>
        </div>
    </div>
</div><?php }
}
