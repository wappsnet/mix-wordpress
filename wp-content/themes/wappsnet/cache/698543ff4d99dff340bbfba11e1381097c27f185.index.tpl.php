<?php
/* Smarty version 3.1.30, created on 2018-12-25 23:10:34
  from "C:\xampp\htdocs\mix\wp-content\themes\wappsnet\modules\subscribe\index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5c22b8ea42b4a2_51419655',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '8edb3a69b1e74f8021f5b26a1bb0ca76224ff629' => 
    array (
      0 => 'C:\\xampp\\htdocs\\mix\\wp-content\\themes\\wappsnet\\modules\\subscribe\\index.tpl',
      1 => 1544387565,
      2 => 'file',
    ),
  ),
  'cache_lifetime' => 86400,
),true)) {
function content_5c22b8ea42b4a2_51419655 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="module-subscribe">
    <h3 class="subscribe-title">
        Новостная рассылка
    </h3>
    <p class="subscribe-header">
        Новости, скидки, распродажи, конкурсы и немного искусства:
    </p>
    <div class="subscribe-wrapper">
        <div class="subscribe-block">
            <input type="email"
                   data-type="email"
                   name="subscribe_email"
                   id="subscribe_email"
                   class="mix-input subscribe-input"
                   placeholder="Email адрес"/>
            <button id="subscribe-submit"
                    class="waves-effect waves-light btn mix-button subscribe-button">
                Подписаться
            </button>
        </div>
    </div>
    <p class="subscribe-footer">
        Нажимая «Подписаться», вы соглашаетесь с правилами использования сервиса и обработки персональных данных.
    </p>
</div><?php }
}
