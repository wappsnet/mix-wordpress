<?php
/* Smarty version 3.1.30, created on 2018-12-25 22:52:30
  from "C:\xampp\htdocs\mix\wp-content\themes\wappsnet\plugins\login\index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5c22b4ae4c4ac9_22317714',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c4ff013d5ab9cd55f2f615f4904c06871f1d1cd2' => 
    array (
      0 => 'C:\\xampp\\htdocs\\mix\\wp-content\\themes\\wappsnet\\plugins\\login\\index.tpl',
      1 => 1545685484,
      2 => 'file',
    ),
  ),
  'cache_lifetime' => 86400,
),true)) {
function content_5c22b4ae4c4ac9_22317714 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="plugin-login">
    <div class="auth-item">
        <div id="auth-info-messages"
             class="auth-info-messages">
        </div>

        <div class="auth-field">
            <div class="auth-icon">
                <i class="fa fa-user"></i>
            </div>
            <input class="auth-input user_login"
                   id="user_login_email"
                   name="email"
                   data-type="email"
                   placeholder="Логин"
                   type="email"/>
        </div>
        <div class="auth-field">
            <div class="auth-icon">
                <i class="fa fa-key"></i>
            </div>
            <input class="auth-input user_login"
                   id="user_login_password"
                   name="password"
                   data-type="password"
                   placeholder="Пароль"
                   type="password"/>
        </div>
        <div class="auth-footer">
            <a class="auth-button action-button waves-effect waves-light btn"
               data-action="user_login">
                Войти
            </a>
            <a class="auth-button waves-effect waves-light btn"
               href="http://localhost/mix/profile/%d1%80%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b0%d1%86%d0%b8%d1%8f/">
                Регистрация
            </a>
        </div>
    </div>
</div><?php }
}
