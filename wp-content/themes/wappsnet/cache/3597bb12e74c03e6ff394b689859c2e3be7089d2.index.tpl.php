<?php
/* Smarty version 3.1.30, created on 2018-07-21 21:01:51
  from "C:\xampp\htdocs\shakti\wp-content\themes\wappsnet\plugins\login\index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b539f3f1d9f10_33071343',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '28bef4efb01556ee905ca8e738a4d4669e52e36c' => 
    array (
      0 => 'C:\\xampp\\htdocs\\shakti\\wp-content\\themes\\wappsnet\\plugins\\login\\index.tpl',
      1 => 1529892979,
      2 => 'file',
    ),
  ),
  'cache_lifetime' => 86400,
),true)) {
function content_5b539f3f1d9f10_33071343 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="plugin-login">
    <div class="auth-item">
        <div id="auth-info-messages"
             class="auth-info-messages">
        </div>

        <div class="auth-field">
            <div class="auth-icon">
                <i class="fa fa-user"></i>
            </div>
            <input class="auth-input user_login"
                   id="user_login_email"
                   name="email"
                   data-type="email"
                   placeholder="Емайл адре"
                   type="email"/>
        </div>
        <div class="auth-field">
            <div class="auth-icon">
                <i class="fa fa-key"></i>
            </div>
            <input class="auth-input user_login"
                   id="user_login_password"
                   name="password"
                   data-type="password"
                   placeholder="Пароль"
                   type="password"/>
        </div>
        <div class="auth-footer">
            <button id="user-login"
                    class="auth-button waves-effect waves-light btn">
                Войти
            </button>
            <a href="http://localhost/shakti/profile/forgot"
               id="user-open-forgot"
               class="auth-button pink waves-effect waves-light btn">
                Забыли пароль?
            </a>
        </div>
    </div>
</div><?php }
}
