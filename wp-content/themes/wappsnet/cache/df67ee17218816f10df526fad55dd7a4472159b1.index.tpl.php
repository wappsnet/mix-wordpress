<?php
/* Smarty version 3.1.30, created on 2018-12-25 22:44:19
  from "C:\xampp\htdocs\mix\wp-content\themes\wappsnet\plugins\register\index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5c22b2c365cc03_30598670',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '9396c5e5b2a07547e6a6737d68dee1efcc09f413' => 
    array (
      0 => 'C:\\xampp\\htdocs\\mix\\wp-content\\themes\\wappsnet\\plugins\\register\\index.tpl',
      1 => 1545692411,
      2 => 'file',
    ),
  ),
  'cache_lifetime' => 86400,
),true)) {
function content_5c22b2c365cc03_30598670 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="plugin-register">
    <div class="auth-item">
        <div id="auth-info-messages"
             class="auth-info-messages">
        </div>

        <div class="auth-field">
            <div class="auth-icon">
                <i class="fa fa-user"></i>
            </div>
            <input class="auth-input user_register"
                   id="user_register_email"
                   name="email"
                   data-type="email"
                   placeholder="Логин"
                   type="email"/>
        </div>
        <div class="auth-field">
            <div class="auth-icon">
                <i class="fa fa-key"></i>
            </div>
            <input class="auth-input user_register"
                   id="user_register_password"
                   name="password"
                   data-type="password"
                   placeholder="Пароль"
                   type="password"/>
        </div>
        <div class="auth-field">
            <input class="auth-input user_register"
                   id="user_register_first_name"
                   name="first_name"
                   data-type="name"
                   placeholder="Имя"
                   type="text"/>
        </div>
        <div class="auth-field">
            <input class="auth-input user_register"
                   id="user_register_last_name"
                   name="last_name"
                   data-type="name"
                   placeholder="Фамилия"
                   type="text"/>
        </div>
        <div class="auth-field">
            <div class="auth-icon">
                <i class="fa fa-phone"></i>
            </div>
            <input class="auth-input user_register"
                   id="user_register_phone"
                   name="phone"
                   data-type="phone"
                   placeholder="Тел: +7 985 999 99 99"
                   type="tel"/>
        </div>
        <div class="auth-footer">
            <a class="auth-button action-button waves-effect waves-light btn"
               data-action="user_register">
                Регистрация
            </a>
            <a class="auth-button waves-effect waves-light btn"
               href="http://localhost/mix/profile/%d0%b2%d0%be%d0%b9%d1%82%d0%b8/">
                Войти
            </a>
        </div>
    </div>
</div><?php }
}
