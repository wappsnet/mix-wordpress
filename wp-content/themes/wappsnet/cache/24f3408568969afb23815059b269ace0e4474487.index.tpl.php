<?php
/* Smarty version 3.1.30, created on 2018-12-25 23:10:37
  from "C:\xampp\htdocs\mix\wp-content\themes\wappsnet\modules\post\index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5c22b8ed0c6b97_95276322',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c349b4eba7d0cea85182c6054ddd32fa22aed776' => 
    array (
      0 => 'C:\\xampp\\htdocs\\mix\\wp-content\\themes\\wappsnet\\modules\\post\\index.tpl',
      1 => 1545167000,
      2 => 'file',
    ),
  ),
  'cache_lifetime' => 86400,
),true)) {
function content_5c22b8ed0c6b97_95276322 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="module-post">
    <div class="post-header">
        <div class="post-header-wrapper">
            <h1 class="post-title">
                Все о Канарах: отдых на Тенерифе – природа, еда и культура
            </h1>
        </div>

                    <img class="post-image" src="http://localhost/mix/wp-content/uploads/2018/12/a3cfcfff-ea72-3c15-b18b-ab101a114077.jpg"/>
            </div>

    <div class="post-body">
        <div class="app-fill">
            <div class="post-date">
                <span class="label">Опубликован:</span>
                <span class="value">2018-12-13 00:37:37</span>
            </div>

            <div class="post-share">
                <span class="label">Поделиться записью:</span>
                <div class="value">
                    <div class="plugin-social-share">
            <a class="social-share"
           href="http://www.facebook.com/sharer.php?u=http://localhost/mix/2018/12/13/%d0%b2%d1%81%d0%b5-%d0%be-%d0%ba%d0%b0%d0%bd%d0%b0%d1%80%d0%b0%d1%85-%d0%be%d1%82%d0%b4%d1%8b%d1%85-%d0%bd%d0%b0-%d1%82%d0%b5%d0%bd%d0%b5%d1%80%d0%b8%d1%84%d0%b5-%d0%bf%d1%80%d0%b8%d1%80-2/"
           target="_blank">
            <i class="fab fa-facebook-f"></i>
        </a>
            <a class="social-share"
           href="http://www.facebook.com/sharer.php?u=http://localhost/mix/2018/12/13/%d0%b2%d1%81%d0%b5-%d0%be-%d0%ba%d0%b0%d0%bd%d0%b0%d1%80%d0%b0%d1%85-%d0%be%d1%82%d0%b4%d1%8b%d1%85-%d0%bd%d0%b0-%d1%82%d0%b5%d0%bd%d0%b5%d1%80%d0%b8%d1%84%d0%b5-%d0%bf%d1%80%d0%b8%d1%80-2/"
           target="_blank">
            <i class="fab fa-vk"></i>
        </a>
            <a class="social-share"
           href="https://plus.google.com/share?url=http://localhost/mix/2018/12/13/%d0%b2%d1%81%d0%b5-%d0%be-%d0%ba%d0%b0%d0%bd%d0%b0%d1%80%d0%b0%d1%85-%d0%be%d1%82%d0%b4%d1%8b%d1%85-%d0%bd%d0%b0-%d1%82%d0%b5%d0%bd%d0%b5%d1%80%d0%b8%d1%84%d0%b5-%d0%bf%d1%80%d0%b8%d1%80-2/"
           target="_blank">
            <i class="fab fa-google-plus-g"></i>
        </a>
    
</div>
                </div>
            </div>

            <div class="post-content">
                <!-- wp:paragraph -->
<p>Нет человека, который не слышал бы о Канарских островах. Ни разу не бывали там? Но ведь наверняка представляете, чем богат этот архипелаг. Переполненные пляжи, курортные отели и туристические ловушки… Тем не менее, как и в большинстве мест, на Канарах до сих пор остались тайные места, о которых знают единицы.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Вместе с опытным писателем-путешественником и экспертом по данному направлению, Андреа Монтгомери, мы подготовили серию из пяти статей о скрытых сокровищах райских островов. Живущий на Канарах Андреа знает все о хайкинге и ресторанах, известных только местным. Он пишет обо всем этом на своем сайте&nbsp;<a href="http://buzztrips.co.uk/" target="_blank" rel="noreferrer noopener">Buzztrips</a>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>В первой части мы расскажем о Тенерифе, самом большом и самом населенном из семи островов архипелага. Ежегодно сюда приезжают пять миллионов человек. Нужно точно знать, куда направиться, чтобы избежать толп.</p>
<!-- /wp:paragraph -->

<!-- wp:heading -->
<h2>Рай для любителей хайкинга</h2>
<!-- /wp:heading -->

<!-- wp:cover {"url":"http://localhost/mix/wp-content/uploads/2018/12/6bfc79b7-c251-3172-94e5-6bfb11f8b9cb.jpg","align":"center","id":199,"hasParallax":true} -->
<div class="wp-block-cover has-background-dim has-parallax aligncenter" style="background-image:url(http://localhost/mix/wp-content/uploads/2018/12/6bfc79b7-c251-3172-94e5-6bfb11f8b9cb.jpg)"><p class="wp-block-cover-text">На Тенерифе множество троп для хайкинга с красивыми пейзажами </p></div>
<!-- /wp:cover -->

<!-- wp:paragraph -->
<p>Отдых на Тенерифе знаменит не только соблазнительными пляжами и солнцем, которое светит круглый год. Остров быстро завоевывает популярность среди любителей хайкинга. И не зря.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Национальный парк Тейде расположен в самом сердце острова. Его сюрреалистический, лунный лавовый ландшафт у подножия самой высокой горы Испании расчерчен тропами для хайкинга. На северо-востоке, в густых лесах лаурисильвы в горах Анага, проложено множество классных трасс.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Чтобы почувствовать темп островной жизни, пройдите по стопам первых жителей Тенерифе, гуанчей. Около 500 лет назад они проложили сеть троп, перегоняя скот между летними и зимними пастбищами. Эти тропы называются каминос реалес (королевские дороги). Они служат отличными пешеходными маршрутами и помогают погрузиться в прошлое Тенерифе.</p>
<!-- /wp:paragraph -->
            </div>
        </div>
    </div>

    </div><?php }
}
