<?php
/* Smarty version 3.1.30, created on 2018-07-12 20:33:20
  from "C:\xampp\htdocs\shakti\wp-content\themes\wappsnet\modules\singlepost\index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b47bb109da2d0_78639443',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '9f2e3df2cfa3744f351d3ab8bef78472fb59a9ec' => 
    array (
      0 => 'C:\\xampp\\htdocs\\shakti\\wp-content\\themes\\wappsnet\\modules\\singlepost\\index.tpl',
      1 => 1530663029,
      2 => 'file',
    ),
  ),
  'cache_lifetime' => 86400,
),true)) {
function content_5b47bb109da2d0_78639443 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="module-single-post">
    <div class="app-fill1200">
        <div class="single-post-wrapper">
            <h4 class="post-category">
                Бранчи -лекции
            </h4>
            <h1 class="post-title">
                Миска риса с куркумой и черной фасолью
            </h1>
            <div class="post-share">
                <div class="share-plugin">
    <div class="app-rel app-box share-block">
        <div class="pluso"
             data-background="transparent"
             data-options="medium,square,line,horizontal,counter,theme=04"
             data-services="vkontakte,facebook,odnoklassniki">
        </div>
    </div>
</div>
            </div>
            <div class="post-date">
                2018-07-02 04:23:21
            </div>
            <div class="post-media">
                                    <iframe width="200" height="113" src="https://www.youtube.com/embed/y4WGlCk0jVQ?feature=oembed" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                            </div>
            <div class="post-author">
                <span class="label">Автор статьи:</span>
                <span class="value">mike</span>
            </div>
            <div class="post-excerpt">
                Эта миска источающего дерзкий аромат экзотического золотисто-желтого риса, покрытого черной фасолью, зернами граната и кусочками авокадо, до краев наполнена мощными питательными веществами. Кроме того, предлагаемое блюдо подходит для вегетарианцев и не содержит глютена.
            </div>
            <div class="post-content">
                Ингредиенты:
<ul>
 	<li>1 ст.л. <a href="https://www.iherb.com/c/olive-oil" target="_blank" rel="noopener">оливкового масла</a> холодного отжима (можно заменить <a href="https://www.iherb.com/pr/Vega-Antioxidant-Omega-Oil-Blend-17-fl-oz-500-ml/43613" target="_blank" rel="noopener">смесью антиоксидантных масел</a>)</li>
 	<li>1 чашка коричневого <a href="https://www.iherb.com/c/Basmati-Rice" target="_blank" rel="noopener">риса басмати</a>, сырого</li>
 	<li>1 ст.л. тертого свежего имбиря (или заменить <a href="https://www.iherb.com/pr/Now-Foods-Real-Food-Crystallized-Ginger-Dices-16-oz-454-g/22681" target="_blank" rel="noopener">кристаллизированным имбирем</a>)</li>
 	<li>2 зубчика чеснока, измельчить</li>
 	<li>2 ч.л. <a href="https://www.iherb.com/c/turmeric-spice" target="_blank" rel="noopener">куркумы</a></li>
 	<li>½ ч.л. свежемолотого черного <a href="https://www.iherb.com/c/pepper" target="_blank" rel="noopener">перца</a></li>
 	<li>2 чашки воды</li>
 	<li>1 кубик овощного <a href="https://www.iherb.com/c/Broths-Bouillon" target="_blank" rel="noopener">бульона</a></li>
 	<li>морская <a href="https://www.iherb.com/c/salt" target="_blank" rel="noopener">соль</a> (по желанию)</li>
 	<li>½ лимона, выжать сок</li>
 	<li>1 банка черной фасоли емкостью 15,5 унций (440 г), промыть и высушить (или используйте в качестве замены приготовленную <a href="https://www.iherb.com/pr/Bob-s-Red-Mill-Black-Turtle-Beans-26-oz-737-g/35665" target="_blank" rel="noopener">черную фасоль Прето)</a></li>
 	<li>1 болгарский перец, мелко нарезать</li>
 	<li>1 чашка свежих зерен граната (в качестве альтернативы попробуйте <a href="https://www.iherb.com/c/Goji-Berries" target="_blank" rel="noopener">ягоды годжи</a>)</li>
 	<li>1 авокадо, нарезать</li>
 	<li>3 стебля зеленого лука, нарезать</li>
 	<li>1 чашка свежей нарезанной кинзы</li>
</ul>
Инструкции:
<ol>
 	<li>Нагрейте оливковое масло в кастрюле средних размеров.</li>
 	<li>Добавьте рис басмати, имбирь, чеснок, куркуму и черный перец и пассеруйте в течение 2 минут.</li>
 	<li>Добавьте воду и овощной бульон, хорошо перемешайте, накройте крышкой и готовьте на среднем огне около 40 минут, пока рис не станет мягким, но не кашицеобразным. В процессе варки периодически помешивайте, чтобы предотвратить образование комков. Добавьте лимонный сок и приправьте морской солью. Хорошо перемешайте и нагревайте еще 1-2 минуты. Снимите с огня.</li>
 	<li>Разложите рис с куркумой по мискам (2 для больших порций или 4 для маленьких порций). Разделите добавки между порциями (2 или 4) и положите сверху риса: черную фасоль, болгарский перец, зерна граната, авокадо, зеленый лук и кинзу.</li>
</ol>
            </div>
        </div>
    </div>
</div><?php }
}
