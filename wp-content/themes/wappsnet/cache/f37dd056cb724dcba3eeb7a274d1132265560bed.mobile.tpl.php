<?php
/* Smarty version 3.1.30, created on 2018-07-12 22:06:57
  from "C:\xampp\htdocs\shakti\wp-content\themes\wappsnet\modules\header\mobile.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b47d101c60468_07699783',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'a2422cf3f518292bfd0cf6a82b8cddc690bc09a0' => 
    array (
      0 => 'C:\\xampp\\htdocs\\shakti\\wp-content\\themes\\wappsnet\\modules\\header\\mobile.tpl',
      1 => 1530792672,
      2 => 'file',
    ),
  ),
  'cache_lifetime' => 86400,
),true)) {
function content_5b47d101c60468_07699783 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="header-module">
    <div class="app-fill1200">
        <div class="header-tools">
            <div class="header-left">
                <a href="http://localhost/shakti/"
                   class="header-tool-item allow-hover">
                    <img src="http://localhost/shakti/wp-content/uploads/2018/07/logo-for-site2.png" class="custom-logo"/>
                    <img src="http://localhost/shakti/wp-content/uploads/2018/07/cropped-logo-icon4.png" class="custom-logo-icon"/>
                </a>
                <div class="header-tool-item">
                    <div id="global-search-plugin"
     class="plugin-search">
    <input id="global-search"
           placeholder="Поиск"
           class="search-input"
           name="search"
           type="search"/>
    <div id="global-search-label"
         class="search-label">
        <i class="fa fa-search"></i>
    </div>
</div>

<div id="search-results-wrapper"
     class="search-results-wrapper">
    <div class="app-fill1200">
        <div class="search-results-container">
            <div class="search-results-header">
                <div class="results-title">
                    Результаты поиска
                </div>
                <div id="close-search-results"
                     class="results-close">
                    <i class="fa fa-times"></i>
                </div>
            </div>
            <div id="search-results-box"
                 class="search-results-box">
            </div>
        </div>
    </div>
</div>
                </div>
            </div>

            <div class="header-right">
                <a href="http://localhost/shakti/profile/login"
                   class="header-tool-item allow-hover">
                    <span class="header-tool-icon"><i class="fa fa-user"></i></span>
                </a>

                <a href="http://localhost/shakti/profile/likes"
                   class="header-tool-item allow-hover">
                    <span class="header-tool-icon" id="like-icon"><i class="fa fa-heart"></i></span>
                    <span class="header-tool-count" id="like-count">0</span>
                </a>

                <a href="http://localhost/shakti/profile/basket"
                   class="header-tool-item allow-hover">
                    <span class="header-tool-icon" id="cart-icon"><i class="fa fa-shopping-bag"></i></span>
                    <span class="header-tool-count" id="cart-count">0</span>
                </a>
            </div>
        </div>
    </div>
</div><?php }
}
