<?php
/* Smarty version 3.1.30, created on 2018-06-17 23:41:57
  from "C:\xampp\htdocs\shakti\wp-content\themes\wappsnet\modules\subscribe\index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b26f1c51961c9_33395219',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'd358bb7c9596e3193f79b1da0d9d13628125d6d6' => 
    array (
      0 => 'C:\\xampp\\htdocs\\shakti\\wp-content\\themes\\wappsnet\\modules\\subscribe\\index.tpl',
      1 => 1497465869,
      2 => 'file',
    ),
  ),
  'cache_lifetime' => 86400,
),true)) {
function content_5b26f1c51961c9_33395219 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="module-subscribe">
    <div class="app-fill1200">
        <div class="app-rel app-box app-center subscribe-block">
            <div class="app-rel app-box subscribe-section">
                <div class="app-rel app-box subscribe-section-inner">
                    <div class="module-logo">
    <div class="app-rel app-box logo-block">
        <a href=""
           class="app-rel app-box logo-white logo-button">
        </a>
    </div>
</div>
                </div>
            </div>
            <div class="app-rel app-box subscribe-section">
                <div class="app-rel app-box subscribe-section-inner">
                    <input type="email"
                           data-type="email"
                           name="subscribe_email"
                           id="subscribe_email"
                           class="app-rel app-box subscribe-input"
                           placeholder="Введите Ваш email адрес"/>
                </div>
                <div class="app-rel app-box subscribe-section-inner">
                    <button id="subscribe-submit"
                            class="waves-effect waves-light btn app-rel app-box subscribe-button">
                        Подписаться
                    </button>
                </div>
            </div>
        </div>
        <div id="subscribe-messages"
             class="app-rel app-box app-tsn03 subscribe-messages-box">
        </div>
    </div>
</div><?php }
}
