<?php
/* Smarty version 3.1.30, created on 2018-07-23 20:57:29
  from "C:\xampp\htdocs\shakti\wp-content\themes\wappsnet\modules\usability\index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b56413964eb11_20402238',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '991d9d3182495e93189f44dd030ff13b4c9b4dac' => 
    array (
      0 => 'C:\\xampp\\htdocs\\shakti\\wp-content\\themes\\wappsnet\\modules\\usability\\index.tpl',
      1 => 1530494257,
      2 => 'file',
    ),
  ),
  'cache_lifetime' => 86400,
),true)) {
function content_5b56413964eb11_20402238 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="module-usability useful">
    <div class="app-fill1200">
        <div class="popular-block">
            <h2 class="popular-title">
                <span>Полезные Статьи</span>
            </h2>
            <div class="popular-content">
                <div class="plugin-carousel">
    <div class="carousel-block">
                    <div class="carousel-item">
                <div class="plugin-post">
    <a href="http://localhost/shakti/%d0%b5%d1%81%d1%82%d0%b5%d1%81%d1%82%d0%b2%d0%b5%d0%bd%d0%bd%d1%8b%d0%b5-%d0%bc%d0%b5%d1%82%d0%be%d0%b4%d1%8b-%d0%bb%d0%b5%d1%87%d0%b5%d0%bd%d0%b8%d1%8f-%d0%b0%d1%81%d1%82%d0%bc%d1%8b/"
       class="post-link">
        <div class="post-media">
                            <img src="http://localhost/shakti/wp-content/uploads/2018/07/natural-therapies-for-asthma-large.jpg"/>
                    </div>
    </a>

    <div class="post-category post-info-wrapper">
        <a href="http://localhost/shakti/3-%d1%80%d0%b5%d1%86%d0%b5%d0%bf%d1%82%d0%b0-%d1%81%d0%bc%d1%83%d0%b7%d0%b8-%d0%b1%d0%be%d1%83%d0%bb%d0%be%d0%b2-%d0%b4%d0%bb%d1%8f-%d0%b7%d0%b4%d0%be%d1%80%d0%be%d0%b2%d0%be%d0%b3%d0%be-%d0%b7%d0%b0/"
           class="post-category-link">
            Йога
        </a>
    </div>

    <a href="http://localhost/shakti/%d0%b5%d1%81%d1%82%d0%b5%d1%81%d1%82%d0%b2%d0%b5%d0%bd%d0%bd%d1%8b%d0%b5-%d0%bc%d0%b5%d1%82%d0%be%d0%b4%d1%8b-%d0%bb%d0%b5%d1%87%d0%b5%d0%bd%d0%b8%d1%8f-%d0%b0%d1%81%d1%82%d0%bc%d1%8b/"
       class="post-link">
        <div class="post-title post-info-wrapper">
            Естественные методы лечения астмы
        </div>

        <div class="post-date post-info-wrapper">
            2018-07-02 22:46:47
        </div>

        <div class="post-content post-info-wrapper">
            Астма — воспалительное заболевание легких, которое возникает в результате спазма мышц, окружающих ткани легкого. Более 300 миллионов человек во всем мире страдают этим заболеванием, а смертность от его...
        </div>
    </a>
</div>
            </div>
                    <div class="carousel-item">
                <div class="plugin-post">
    <a href="http://localhost/shakti/%d0%bf%d0%bb%d1%8e%d1%81%d1%8b-%d0%b8-%d0%bc%d0%b8%d0%bd%d1%83%d1%81%d1%8b-%d0%bd%d0%b0%d1%82%d1%83%d1%80%d0%b0%d0%bb%d1%8c%d0%bd%d1%8b%d1%85-%d0%b4%d0%b5%d0%b7%d0%be%d0%b4%d0%be%d1%80%d0%b0%d0%bd/"
       class="post-link">
        <div class="post-media">
                            <img src="http://localhost/shakti/wp-content/uploads/2018/06/pros-and-cons-of-natural-deodorants-large.jpg"/>
                    </div>
    </a>

    <div class="post-category post-info-wrapper">
        <a href="http://localhost/shakti/3-%d1%80%d0%b5%d1%86%d0%b5%d0%bf%d1%82%d0%b0-%d1%81%d0%bc%d1%83%d0%b7%d0%b8-%d0%b1%d0%be%d1%83%d0%bb%d0%be%d0%b2-%d0%b4%d0%bb%d1%8f-%d0%b7%d0%b4%d0%be%d1%80%d0%be%d0%b2%d0%be%d0%b3%d0%be-%d0%b7%d0%b0/"
           class="post-category-link">
            Йога
        </a>
    </div>

    <a href="http://localhost/shakti/%d0%bf%d0%bb%d1%8e%d1%81%d1%8b-%d0%b8-%d0%bc%d0%b8%d0%bd%d1%83%d1%81%d1%8b-%d0%bd%d0%b0%d1%82%d1%83%d1%80%d0%b0%d0%bb%d1%8c%d0%bd%d1%8b%d1%85-%d0%b4%d0%b5%d0%b7%d0%be%d0%b4%d0%be%d1%80%d0%b0%d0%bd/"
       class="post-link">
        <div class="post-title post-info-wrapper">
            Плюсы и минусы натуральных дезодорантов
        </div>

        <div class="post-date post-info-wrapper">
            2018-07-02 06:19:48
        </div>

        <div class="post-content post-info-wrapper">
            За последние годы натуральные дезодоранты стали весьма популярны. Многих беспокоят химикаты и потенциально вредные эффекты от обычных дезодорантов. Обычные дезодоранты, включая дезодоранты-антиперспиранты,...
        </div>
    </a>
</div>
            </div>
                    <div class="carousel-item">
                <div class="plugin-post">
    <a href="http://localhost/shakti/%d0%bc%d0%b8%d1%81%d0%ba%d0%b0-%d1%80%d0%b8%d1%81%d0%b0-%d1%81-%d0%ba%d1%83%d1%80%d0%ba%d1%83%d0%bc%d0%be%d0%b9-%d0%b8-%d1%87%d0%b5%d1%80%d0%bd%d0%be%d0%b9-%d1%84%d0%b0%d1%81%d0%be%d0%bb%d1%8c%d1%8e/"
       class="post-link">
        <div class="post-media">
                            <iframe width="200" height="113" src="https://www.youtube.com/embed/y4WGlCk0jVQ?feature=oembed" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                    </div>
    </a>

    <div class="post-category post-info-wrapper">
        <a href="http://localhost/shakti/3-%d1%80%d0%b5%d1%86%d0%b5%d0%bf%d1%82%d0%b0-%d1%81%d0%bc%d1%83%d0%b7%d0%b8-%d0%b1%d0%be%d1%83%d0%bb%d0%be%d0%b2-%d0%b4%d0%bb%d1%8f-%d0%b7%d0%b4%d0%be%d1%80%d0%be%d0%b2%d0%be%d0%b3%d0%be-%d0%b7%d0%b0/"
           class="post-category-link">
            Бранчи -лекции
        </a>
    </div>

    <a href="http://localhost/shakti/%d0%bc%d0%b8%d1%81%d0%ba%d0%b0-%d1%80%d0%b8%d1%81%d0%b0-%d1%81-%d0%ba%d1%83%d1%80%d0%ba%d1%83%d0%bc%d0%be%d0%b9-%d0%b8-%d1%87%d0%b5%d1%80%d0%bd%d0%be%d0%b9-%d1%84%d0%b0%d1%81%d0%be%d0%bb%d1%8c%d1%8e/"
       class="post-link">
        <div class="post-title post-info-wrapper">
            Миска риса с куркумой и черной фасолью
        </div>

        <div class="post-date post-info-wrapper">
            2018-07-02 04:23:21
        </div>

        <div class="post-content post-info-wrapper">
            Эта миска источающего дерзкий аромат экзотического золотисто-желтого риса, покрытого черной фасолью, зернами граната и кусочками авокадо, до краев наполнена мощными питательными веществами. Кроме того,...
        </div>
    </a>
</div>
            </div>
                    <div class="carousel-item">
                <div class="plugin-post">
    <a href="http://localhost/shakti/14-%d1%83%d0%b4%d0%b8%d0%b2%d0%b8%d1%82%d0%b5%d0%bb%d1%8c%d0%bd%d1%8b%d1%85-%d1%81%d0%bf%d0%be%d1%81%d0%be%d0%b1%d0%be%d0%b2-%d0%bf%d1%80%d0%b8%d0%bc%d0%b5%d0%bd%d0%b5%d0%bd%d0%b8%d1%8f-%d0%b3%d0%b0/"
       class="post-link">
        <div class="post-media">
                            <img src="http://localhost/shakti/wp-content/uploads/2018/07/14-amazing-uses-for-witch-hazel-large.jpg"/>
                    </div>
    </a>

    <div class="post-category post-info-wrapper">
        <a href="http://localhost/shakti/3-%d1%80%d0%b5%d1%86%d0%b5%d0%bf%d1%82%d0%b0-%d1%81%d0%bc%d1%83%d0%b7%d0%b8-%d0%b1%d0%be%d1%83%d0%bb%d0%be%d0%b2-%d0%b4%d0%bb%d1%8f-%d0%b7%d0%b4%d0%be%d1%80%d0%be%d0%b2%d0%be%d0%b3%d0%be-%d0%b7%d0%b0/"
           class="post-category-link">
            Йога
        </a>
    </div>

    <a href="http://localhost/shakti/14-%d1%83%d0%b4%d0%b8%d0%b2%d0%b8%d1%82%d0%b5%d0%bb%d1%8c%d0%bd%d1%8b%d1%85-%d1%81%d0%bf%d0%be%d1%81%d0%be%d0%b1%d0%be%d0%b2-%d0%bf%d1%80%d0%b8%d0%bc%d0%b5%d0%bd%d0%b5%d0%bd%d0%b8%d1%8f-%d0%b3%d0%b0/"
       class="post-link">
        <div class="post-title post-info-wrapper">
            14 удивительных способов применения гамамелиса
        </div>

        <div class="post-date post-info-wrapper">
            2018-07-02 01:32:45
        </div>

        <div class="post-content post-info-wrapper">
            К моменту прибытия отцов-пилигримов — в XVII веке — коренные американцы уже использовали гамамелис. Первопоселенцы вскоре узнали секрет его применения при многих нарушениях здоровья. Он представляет собой...
        </div>
    </a>
</div>
            </div>
        
    </div>
</div>
            </div>
        </div>
    </div>
</div><?php }
}
