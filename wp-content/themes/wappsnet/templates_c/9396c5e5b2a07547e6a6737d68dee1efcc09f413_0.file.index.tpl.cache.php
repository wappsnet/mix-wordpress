<?php
/* Smarty version 3.1.30, created on 2018-12-25 22:44:19
  from "C:\xampp\htdocs\mix\wp-content\themes\wappsnet\plugins\register\index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5c22b2c3485642_21312076',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '9396c5e5b2a07547e6a6737d68dee1efcc09f413' => 
    array (
      0 => 'C:\\xampp\\htdocs\\mix\\wp-content\\themes\\wappsnet\\plugins\\register\\index.tpl',
      1 => 1545692411,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c22b2c3485642_21312076 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->compiled->nocache_hash = '10909891485c22b2c347f360_28554881';
?>
<div class="plugin-register">
    <div class="auth-item">
        <div id="auth-info-messages"
             class="auth-info-messages">
        </div>

        <div class="auth-field">
            <div class="auth-icon">
                <i class="fa fa-user"></i>
            </div>
            <input class="auth-input user_register"
                   id="user_register_email"
                   name="email"
                   data-type="email"
                   placeholder="<?php echo $_smarty_tpl->tpl_vars['lang']->value['auth_email'];?>
"
                   type="email"/>
        </div>
        <div class="auth-field">
            <div class="auth-icon">
                <i class="fa fa-key"></i>
            </div>
            <input class="auth-input user_register"
                   id="user_register_password"
                   name="password"
                   data-type="password"
                   placeholder="<?php echo $_smarty_tpl->tpl_vars['lang']->value['auth_password'];?>
"
                   type="password"/>
        </div>
        <div class="auth-field">
            <input class="auth-input user_register"
                   id="user_register_first_name"
                   name="first_name"
                   data-type="name"
                   placeholder="<?php echo $_smarty_tpl->tpl_vars['lang']->value['auth_first_name'];?>
"
                   type="text"/>
        </div>
        <div class="auth-field">
            <input class="auth-input user_register"
                   id="user_register_last_name"
                   name="last_name"
                   data-type="name"
                   placeholder="<?php echo $_smarty_tpl->tpl_vars['lang']->value['auth_last_name'];?>
"
                   type="text"/>
        </div>
        <div class="auth-field">
            <div class="auth-icon">
                <i class="fa fa-phone"></i>
            </div>
            <input class="auth-input user_register"
                   id="user_register_phone"
                   name="phone"
                   data-type="phone"
                   placeholder="<?php echo $_smarty_tpl->tpl_vars['lang']->value['auth_phone'];?>
"
                   type="tel"/>
        </div>
        <div class="auth-footer">
            <a class="auth-button action-button waves-effect waves-light btn"
               data-action="user_register">
                <?php echo $_smarty_tpl->tpl_vars['lang']->value['registration'];?>

            </a>
            <a class="auth-button waves-effect waves-light btn"
               href="<?php echo $_smarty_tpl->tpl_vars['links']->value['login'];?>
">
                <?php echo $_smarty_tpl->tpl_vars['lang']->value['login'];?>

            </a>
        </div>
    </div>
</div><?php }
}
