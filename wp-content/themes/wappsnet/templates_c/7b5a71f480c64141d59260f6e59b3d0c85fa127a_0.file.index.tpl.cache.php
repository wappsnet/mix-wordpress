<?php
/* Smarty version 3.1.30, created on 2018-12-25 23:10:33
  from "C:\xampp\htdocs\mix\wp-content\themes\wappsnet\modules\popular\index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5c22b8e9ed42a2_59833079',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '7b5a71f480c64141d59260f6e59b3d0c85fa127a' => 
    array (
      0 => 'C:\\xampp\\htdocs\\mix\\wp-content\\themes\\wappsnet\\modules\\popular\\index.tpl',
      1 => 1544826682,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c22b8e9ed42a2_59833079 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->compiled->nocache_hash = '10721062145c22b8e9ece222_66179626';
?>
<div class="module-popular">
    <div class="app-fill">
        <div class="popular-wrapper">
            <h3 class="popular-title">
                <?php echo $_smarty_tpl->tpl_vars['lang']->value['subscribe_title'];?>

            </h3>
            <p class="popular-header">
                <?php echo $_smarty_tpl->tpl_vars['lang']->value['subscribe_description'];?>

            </p>
            <div class="popular-items">
                <div class="app-row">
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['items']->value, 'item');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
?>
                        <div class="app-column large-3 middle-6 small-12">
                            <div class="popular-item">
                                <?php echo $_smarty_tpl->tpl_vars['item']->value;?>

                            </div>
                        </div>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

                </div>
            </div>
        </div>
    </div>
</div><?php }
}
