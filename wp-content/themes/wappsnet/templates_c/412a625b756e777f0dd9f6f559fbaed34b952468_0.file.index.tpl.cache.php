<?php
/* Smarty version 3.1.30, created on 2018-12-25 23:43:39
  from "C:\xampp\htdocs\mix\wp-content\themes\wappsnet\modules\footer\index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5c22c0ab888050_27041427',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '412a625b756e777f0dd9f6f559fbaed34b952468' => 
    array (
      0 => 'C:\\xampp\\htdocs\\mix\\wp-content\\themes\\wappsnet\\modules\\footer\\index.tpl',
      1 => 1544371732,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c22c0ab888050_27041427 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->compiled->nocache_hash = '20263811265c22c0ab870eb5_61233092';
?>
<div class="footer-module">
    <div class="footer-social">
        <div class="app-fill">
            <div class="footer-social-wrapper">
                <?php echo $_smarty_tpl->tpl_vars['socialPart']->value;?>

            </div>
        </div>
    </div>

    <div class="footer-menu">
        <div class="app-fill">
            <div class="footer-menu-wrapper">
                <?php echo $_smarty_tpl->tpl_vars['navigationPart']->value;?>

            </div>
        </div>
    </div>

    <div class="footer-apps">
        <div class="app-fill">
            <div class="footer-apps-wrapper">
                <?php echo $_smarty_tpl->tpl_vars['mobilePart']->value;?>

            </div>
        </div>
    </div>

    <div class="footer-rights">
        <div class="app-fill">
            <div class="footer-rights-wrapper">
                <?php echo $_smarty_tpl->tpl_vars['rightsPart']->value;?>

            </div>
        </div>
    </div>
</div><?php }
}
