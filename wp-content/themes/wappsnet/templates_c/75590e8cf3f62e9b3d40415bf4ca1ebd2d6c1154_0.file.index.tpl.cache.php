<?php
/* Smarty version 3.1.30, created on 2018-12-25 23:10:33
  from "C:\xampp\htdocs\mix\wp-content\themes\wappsnet\plugins\post\index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5c22b8e9dc2a46_77305044',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '75590e8cf3f62e9b3d40415bf4ca1ebd2d6c1154' => 
    array (
      0 => 'C:\\xampp\\htdocs\\mix\\wp-content\\themes\\wappsnet\\plugins\\post\\index.tpl',
      1 => 1544660280,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c22b8e9dc2a46_77305044 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->compiled->nocache_hash = '1201531935c22b8e9dbcf94_34435778';
?>
<div class="plugin-post">
    <a href="<?php echo $_smarty_tpl->tpl_vars['post']->value['link'];?>
"
       class="post-wrapper">
        <div class="post-media">
            <?php if ($_smarty_tpl->tpl_vars['post']->value['media']['video']) {?>
                <?php echo $_smarty_tpl->tpl_vars['post']->value['media']['video'];?>

            <?php } else { ?>
                <img src="<?php echo $_smarty_tpl->tpl_vars['post']->value['media']['image'];?>
"/>
            <?php }?>
        </div>
        <div class="post-info">
            <div class="post-date post-info-wrapper">
                <?php echo $_smarty_tpl->tpl_vars['post']->value['data']->post_date;?>

            </div>

            <div class="post-title post-info-wrapper">
                <?php echo $_smarty_tpl->tpl_vars['post']->value['data']->post_title;?>

            </div>
        </div>
    </a>
</div><?php }
}
