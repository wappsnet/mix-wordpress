<?php
/* Smarty version 3.1.30, created on 2018-12-25 23:43:39
  from "C:\xampp\htdocs\mix\wp-content\themes\wappsnet\plugins\socialicons\index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5c22c0ab4ca346_29014597',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e7d8c4f6750f7ce7ba1cfdc5e560dde7b8ec5822' => 
    array (
      0 => 'C:\\xampp\\htdocs\\mix\\wp-content\\themes\\wappsnet\\plugins\\socialicons\\index.tpl',
      1 => 1545170668,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c22c0ab4ca346_29014597 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->compiled->nocache_hash = '11488503725c22c0ab4b6e04_92532436';
?>
<div class="plugin-social-icons">
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['items']->value, 'item');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
?>
        <a class="social-link"
           href="<?php echo $_smarty_tpl->tpl_vars['item']->value['link'];?>
">
            <i class="<?php echo $_smarty_tpl->tpl_vars['item']->value['icon'];?>
"></i>
        </a>
    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

</div><?php }
}
