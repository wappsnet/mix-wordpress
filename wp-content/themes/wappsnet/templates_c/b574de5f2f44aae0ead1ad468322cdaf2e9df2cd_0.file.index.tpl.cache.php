<?php
/* Smarty version 3.1.30, created on 2018-12-25 23:43:39
  from "C:\xampp\htdocs\mix\wp-content\themes\wappsnet\modules\profile\index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5c22c0ab2b5f16_22691121',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b574de5f2f44aae0ead1ad468322cdaf2e9df2cd' => 
    array (
      0 => 'C:\\xampp\\htdocs\\mix\\wp-content\\themes\\wappsnet\\modules\\profile\\index.tpl',
      1 => 1545777642,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c22c0ab2b5f16_22691121 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->compiled->nocache_hash = '1527389475c22c0ab2a1609_95685693';
?>
<div class="module-profile">
    <div class="app-fill">
        <div class="profile-content">
            <?php if ($_smarty_tpl->tpl_vars['post']->value->post_title) {?>
                <h1 class="profile-title">
                    <?php echo $_smarty_tpl->tpl_vars['post']->value->post_title;?>

                </h1>
            <?php }?>

            <?php if ($_smarty_tpl->tpl_vars['post']->value->post_content) {?>
                <div class="profile-text">
                    <?php echo $_smarty_tpl->tpl_vars['post']->value->post_content;?>

                </div>
            <?php }?>
        </div>

        <div class="profile-plugins">
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['plugins']->value, 'item');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
?>
                <div class="profile-plugin">
                    <?php echo $_smarty_tpl->tpl_vars['item']->value['plugin'];?>

                </div>
            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

        </div>
    </div>
</div><?php }
}
