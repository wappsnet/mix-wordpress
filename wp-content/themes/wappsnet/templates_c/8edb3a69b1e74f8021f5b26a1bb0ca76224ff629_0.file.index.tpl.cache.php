<?php
/* Smarty version 3.1.30, created on 2018-12-25 23:10:34
  from "C:\xampp\htdocs\mix\wp-content\themes\wappsnet\modules\subscribe\index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5c22b8ea3b5da9_86678952',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '8edb3a69b1e74f8021f5b26a1bb0ca76224ff629' => 
    array (
      0 => 'C:\\xampp\\htdocs\\mix\\wp-content\\themes\\wappsnet\\modules\\subscribe\\index.tpl',
      1 => 1544387565,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c22b8ea3b5da9_86678952 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->compiled->nocache_hash = '6361073315c22b8ea3b13d8_23813378';
?>
<div class="module-subscribe">
    <h3 class="subscribe-title">
        <?php echo $_smarty_tpl->tpl_vars['lang']->value['subscribe_title'];?>

    </h3>
    <p class="subscribe-header">
        <?php echo $_smarty_tpl->tpl_vars['lang']->value['subscribe_description'];?>

    </p>
    <div class="subscribe-wrapper">
        <div class="subscribe-block">
            <input type="email"
                   data-type="email"
                   name="subscribe_email"
                   id="subscribe_email"
                   class="mix-input subscribe-input"
                   placeholder="<?php echo $_smarty_tpl->tpl_vars['lang']->value['email_placeholder'];?>
"/>
            <button id="subscribe-submit"
                    class="waves-effect waves-light btn mix-button subscribe-button">
                <?php echo $_smarty_tpl->tpl_vars['lang']->value['subscribe'];?>

            </button>
        </div>
    </div>
    <p class="subscribe-footer">
        <?php echo $_smarty_tpl->tpl_vars['lang']->value['subscribe_notice'];?>

    </p>
</div><?php }
}
