<?php
/* Smarty version 3.1.30, created on 2018-12-25 23:43:39
  from "C:\xampp\htdocs\mix\wp-content\themes\wappsnet\modules\header\index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5c22c0ab0521e4_05120738',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '9a03b5b9aa70dfb87bc0cc84e782a281cf2e79e7' => 
    array (
      0 => 'C:\\xampp\\htdocs\\mix\\wp-content\\themes\\wappsnet\\modules\\header\\index.tpl',
      1 => 1545693686,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c22c0ab0521e4_05120738 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->compiled->nocache_hash = '3580874045c22c0aaeaeb03_34637957';
?>
<div class="header-module">
    <div class="app-fill">
        <div class="header-tools">
            <div class="header-left">
                <a href="<?php echo $_smarty_tpl->tpl_vars['links']->value['home'];?>
"
                   class="header-tool-item allow-hover">
                    <img src="<?php echo $_smarty_tpl->tpl_vars['logo']->value;?>
"/>
                </a>

                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['menu']->value, 'menuItem');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['menuItem']->value) {
?>
                    <?php if (!$_smarty_tpl->tpl_vars['menuItem']->value->menu_item_parent) {?>
                        <a href="<?php echo $_smarty_tpl->tpl_vars['menuItem']->value->url;?>
"
                           class="header-tool-item allow-hover">
                            <?php echo $_smarty_tpl->tpl_vars['menuItem']->value->title;?>

                        </a>
                    <?php }?>
                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

            </div>

            <div class="header-right">
                <?php if ($_smarty_tpl->tpl_vars['user']->value['user_id']) {?>
                    <a href="<?php echo $_smarty_tpl->tpl_vars['links']->value['cabinet'];?>
"
                       class="header-tool-item allow-hover">
                        <span class="header-tool-name"><?php echo $_smarty_tpl->tpl_vars['user']->value['user_name'];?>
</span>
                        <span class="header-tool-icon"><i class="fas fa-user-circle"></i></span>
                    </a>
                <?php } else { ?>
                    <a href="<?php echo $_smarty_tpl->tpl_vars['links']->value['login'];?>
"
                       class="header-tool-item allow-hover">
                        <span class="header-tool-icon"><i class="fa fa-user"></i></span>
                    </a>
                <?php }?>
            </div>
        </div>
    </div>
</div><?php }
}
