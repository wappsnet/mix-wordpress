<?php
/* Smarty version 3.1.30, created on 2018-12-25 23:10:04
  from "C:\xampp\htdocs\mix\wp-content\themes\wappsnet\modules\page\index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5c22b8cccd6346_71749940',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '9f4fc3243b78d8f3af4e2bdef4ef8c1f581f2abe' => 
    array (
      0 => 'C:\\xampp\\htdocs\\mix\\wp-content\\themes\\wappsnet\\modules\\page\\index.tpl',
      1 => 1544398191,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c22b8cccd6346_71749940 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->compiled->nocache_hash = '14490092435c22b8ccc51e20_85237124';
?>
<div class="module-page">
    <div class="page-header">
        <div class="page-header-wrapper">
            <h1 class="page-title">
                <?php echo $_smarty_tpl->tpl_vars['page']->value->post_title;?>

            </h1>
        </div>

        <?php if ($_smarty_tpl->tpl_vars['image']->value) {?>
            <img class="page-image" src="<?php echo $_smarty_tpl->tpl_vars['image']->value;?>
"/>
        <?php }?>
    </div>

    <div class="page-body">
        <div class="app-fill">
            <div class="page-content">
                <?php echo $_smarty_tpl->tpl_vars['page']->value->post_content;?>

            </div>
        </div>
    </div>

    <?php if ($_smarty_tpl->tpl_vars['children']->value) {?>
        <div class="child-pages">
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['children']->value, 'item');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
?>
                <a class="child-page" href="<?php echo $_smarty_tpl->tpl_vars['item']->value->post_link;?>
">
                    <?php echo $_smarty_tpl->tpl_vars['item']->value->post_title;?>

                </a>
            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

        </div>
    <?php }?>
</div><?php }
}
