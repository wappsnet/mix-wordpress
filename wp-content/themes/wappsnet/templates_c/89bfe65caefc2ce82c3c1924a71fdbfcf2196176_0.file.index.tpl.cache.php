<?php
/* Smarty version 3.1.30, created on 2018-12-25 23:10:33
  from "C:\xampp\htdocs\mix\wp-content\themes\wappsnet\modules\search\index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5c22b8e97e07f1_19706201',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '89bfe65caefc2ce82c3c1924a71fdbfcf2196176' => 
    array (
      0 => 'C:\\xampp\\htdocs\\mix\\wp-content\\themes\\wappsnet\\modules\\search\\index.tpl',
      1 => 1545431779,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c22b8e97e07f1_19706201 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->compiled->nocache_hash = '8480299515c22b8e97dc115_72069895';
?>
<div class="module-search">
    <div class="primary-content"
         style="background-image: url('<?php echo $_smarty_tpl->tpl_vars['primaryImage']->value['url'];?>
')">
        <div class="app-fill">
            <h1 class="primary-title"><?php echo $_smarty_tpl->tpl_vars['primaryTitle']->value;?>
</h1>
            <h2 class="secondary-title"><?php echo $_smarty_tpl->tpl_vars['secondaryTitle']->value;?>
</h2>
            <div id="mix-search"
                 class="search-widget">
            </div>
        </div>
    </div>
</div><?php }
}
