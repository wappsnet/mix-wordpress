<?php
/* Smarty version 3.1.30, created on 2018-12-24 18:30:14
  from "C:\xampp\htdocs\mix\wp-content\themes\wappsnet\modules\header\mobile.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5c2125b6861086_19962449',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '6dbb3e0571b649373752c5bfc8632cbb1dc04d7f' => 
    array (
      0 => 'C:\\xampp\\htdocs\\mix\\wp-content\\themes\\wappsnet\\modules\\header\\mobile.tpl',
      1 => 1545072342,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c2125b6861086_19962449 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->compiled->nocache_hash = '17390892995c2125b682a877_47893713';
?>
<div class="header-module">
    <div class="app-fill">
        <div class="header-tools">
            <div class="header-left">
                <div id="side-menu-toggle"
                     class="header-tool-item allow-hover"
                     data-activates="mobile-menu">
                    <span class="header-tool-icon"><i class="fa fa-bars"></i></span>
                </div>

                <a href="<?php echo $_smarty_tpl->tpl_vars['links']->value['home'];?>
"
                   class="header-tool-item allow-hover">
                    <img src="<?php echo $_smarty_tpl->tpl_vars['logo']->value;?>
"/>
                </a>
            </div>

            <div class="header-right">
                <a href="<?php echo $_smarty_tpl->tpl_vars['links']->value['cabinet'];?>
"
                   class="header-tool-item allow-hover">
                    <span class="header-tool-icon"><i class="fa fa-user"></i></span>
                </a>
            </div>
        </div>
    </div>
</div>

<div id="mobile-menu"
     class="side-nav app-side-nav side-menu-box">
    <div class="app-side-logo">
        <img src="<?php echo $_smarty_tpl->tpl_vars['logo']->value;?>
"/>
    </div>
    <div class="app-side-wrapper">
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['menu']->value, 'menuItem');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['menuItem']->value) {
?>
            <?php if (!$_smarty_tpl->tpl_vars['menuItem']->value->menu_item_parent) {?>
                <a href="<?php echo $_smarty_tpl->tpl_vars['menuItem']->value->url;?>
"
                   class="app-side-menu-item">
                    <?php echo $_smarty_tpl->tpl_vars['menuItem']->value->title;?>

                </a>
            <?php }?>
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

    </div>
</div>
<?php }
}
