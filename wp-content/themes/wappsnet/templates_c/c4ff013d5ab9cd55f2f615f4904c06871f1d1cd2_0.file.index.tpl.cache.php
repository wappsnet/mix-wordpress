<?php
/* Smarty version 3.1.30, created on 2018-12-25 22:52:30
  from "C:\xampp\htdocs\mix\wp-content\themes\wappsnet\plugins\login\index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5c22b4ae32a4a3_85284501',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c4ff013d5ab9cd55f2f615f4904c06871f1d1cd2' => 
    array (
      0 => 'C:\\xampp\\htdocs\\mix\\wp-content\\themes\\wappsnet\\plugins\\login\\index.tpl',
      1 => 1545685484,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c22b4ae32a4a3_85284501 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->compiled->nocache_hash = '7743361155c22b4ae325159_84789371';
?>
<div class="plugin-login">
    <div class="auth-item">
        <div id="auth-info-messages"
             class="auth-info-messages">
        </div>

        <div class="auth-field">
            <div class="auth-icon">
                <i class="fa fa-user"></i>
            </div>
            <input class="auth-input user_login"
                   id="user_login_email"
                   name="email"
                   data-type="email"
                   placeholder="<?php echo $_smarty_tpl->tpl_vars['lang']->value['auth_email'];?>
"
                   type="email"/>
        </div>
        <div class="auth-field">
            <div class="auth-icon">
                <i class="fa fa-key"></i>
            </div>
            <input class="auth-input user_login"
                   id="user_login_password"
                   name="password"
                   data-type="password"
                   placeholder="<?php echo $_smarty_tpl->tpl_vars['lang']->value['auth_password'];?>
"
                   type="password"/>
        </div>
        <div class="auth-footer">
            <a class="auth-button action-button waves-effect waves-light btn"
               data-action="user_login">
                <?php echo $_smarty_tpl->tpl_vars['lang']->value['login'];?>

            </a>
            <a class="auth-button waves-effect waves-light btn"
               href="<?php echo $_smarty_tpl->tpl_vars['links']->value['registration'];?>
">
                <?php echo $_smarty_tpl->tpl_vars['lang']->value['registration'];?>

            </a>
        </div>
    </div>
</div><?php }
}
