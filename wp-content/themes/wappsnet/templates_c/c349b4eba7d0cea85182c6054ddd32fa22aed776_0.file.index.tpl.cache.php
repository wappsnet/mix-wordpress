<?php
/* Smarty version 3.1.30, created on 2018-12-25 23:10:36
  from "C:\xampp\htdocs\mix\wp-content\themes\wappsnet\modules\post\index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5c22b8eceb1868_21602078',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c349b4eba7d0cea85182c6054ddd32fa22aed776' => 
    array (
      0 => 'C:\\xampp\\htdocs\\mix\\wp-content\\themes\\wappsnet\\modules\\post\\index.tpl',
      1 => 1545167000,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c22b8eceb1868_21602078 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->compiled->nocache_hash = '15882969375c22b8ece9fa77_72210162';
?>
<div class="module-post">
    <div class="post-header">
        <div class="post-header-wrapper">
            <h1 class="post-title">
                <?php echo $_smarty_tpl->tpl_vars['post']->value->post_title;?>

            </h1>
        </div>

        <?php if ($_smarty_tpl->tpl_vars['image']->value) {?>
            <img class="post-image" src="<?php echo $_smarty_tpl->tpl_vars['image']->value;?>
"/>
        <?php }?>
    </div>

    <div class="post-body">
        <div class="app-fill">
            <div class="post-date">
                <span class="label"><?php echo $_smarty_tpl->tpl_vars['lang']->value['published_label'];?>
</span>
                <span class="value"><?php echo $_smarty_tpl->tpl_vars['post']->value->post_date_gmt;?>
</span>
            </div>

            <div class="post-share">
                <span class="label"><?php echo $_smarty_tpl->tpl_vars['lang']->value['social_post_label'];?>
</span>
                <div class="value">
                    <?php echo $_smarty_tpl->tpl_vars['share']->value;?>

                </div>
            </div>

            <div class="post-content">
                <?php echo $_smarty_tpl->tpl_vars['post']->value->post_content;?>

            </div>
        </div>
    </div>

    <?php if ($_smarty_tpl->tpl_vars['children']->value) {?>
        <div class="child-posts">
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['children']->value, 'item');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
?>
                <a class="child-post" href="<?php echo $_smarty_tpl->tpl_vars['item']->value->post_link;?>
">
                    <?php echo $_smarty_tpl->tpl_vars['item']->value->post_title;?>

                </a>
            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

        </div>
    <?php }?>
</div><?php }
}
