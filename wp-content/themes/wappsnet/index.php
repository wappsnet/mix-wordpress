<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package wappsnet
 */

get_header();

\Wappsnet\Core\Render::load_layout('Header', [
    'widgets' => true
]);

\Wappsnet\Core\Render::load_module('Header');
\Wappsnet\Core\Render::load_module('Search');
\Wappsnet\Core\Render::load_module('Popular');
\Wappsnet\Core\Render::load_module('SubScribe');
\Wappsnet\Core\Render::load_module('Footer');

\Wappsnet\Core\Render::load_layout('Footer', [
    'widgets' => true
]);

get_footer();
