import Component from '@ember/component';
import layout from 'mix-typo/templates/components/mix-field/mix-checkbox';
import { computed } from '@ember/object';

export default Component.extend({
    layout,

    tagName: 'label',

    classNames: [
        'mix-checkbox'
    ],

    classNameBindings: [
        'stateClass',
        'sizeClass'
    ],

    checked: false,

    label: 'Label',

    labelType: 'top',

    state: 'default',

    stateClass: computed('state', function() {
        return 'mix-field-' + this.state;
    }),

    size: 'medium',

    sizeClass: computed('size', function() {
        return 'mix-field-' + this.size;
    }),

    actions: {
        changeValue() {
            this.set('checked', !this.checked);

            if(this.changeValue) {
                this.changeValue(this.value, this.checked)
            }
        }
    }
});
