import Component from '@ember/component';
import layout from 'mix-typo/templates/components/mix-field/mix-input';

export default Component.extend({
    layout,

    classNames: [
        'mix-input',
        'mix-number'
    ],

    value: null,

    name: null,

    type: 'number',

    changeValue: null,

    actions: {
        changeValue(value) {
            let optimalValue = Math.max(Math.min(value, this.max), this.min);

            if(value !== optimalValue) {
                this.set('value', optimalValue);
            }

            if(this.changeValue) {
                this.changeValue(optimalValue, this.name);
            }
        }
    }
});