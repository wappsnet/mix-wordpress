import Component from '@ember/component';
import layout from 'mix-typo/templates/components/mix-field/mix-select';
import { computed } from '@ember/object';

export default Component.extend({
    layout,

    classNames: [
        'mix-input',
        'mix-input-icon-right',
        'mix-select',
    ],

    attributeBindings: [
        'tabIndex'
    ],

    tabIndex: 1,

    type: 'select',

    iconType: 'right',

    icon: 'circle-down',

    placeholder: 'PlaceHolder',

    items: [],

    name: null,

    search: false,

    multi: false,

    opened: false,

    value: computed('selected', function () {
        let value = [];

        if(this.selected) {
            Object.keys(this.selected).map((selectedKey) => {
                value.push(this.items.findBy('value', selectedKey));
            });
        }

        return value;
    }),

    searchString: null,

    searchLength: 3,

    init() {
        this._super(...arguments);
        this.set('selected', {});
        this.set('options', this.items);
    },

    didInsertElement() {
        this._super(...arguments);

        document.addEventListener('mousedown', this._documentMouseDown = () => {
            this.toggleSelect(false);
        });

        this.element.addEventListener('focus', this._elementFocus = () => {
            this.toggleSelect(true);
        });
    },

    mouseDown(e) {
        e.stopPropagation();
        this.toggleSelect(true);
    },

    willDestroyElement() {
        document.removeEventListener('mousedown', this._documentMouseDown);
        this._super(...arguments);
    },

    toggleSelect(status) {
        this.set('opened', status);
    },

    findItems(string) {
        return this.items.filter((item) => {
            return (
                item.label.indexOf(string) === 0
            );
        });
    },

    actions: {
        changeValue(value) {
            if(value.length >= this.searchLength) {
                let findItems = this.findItems(this.searchString);

                this.set('options', findItems);
            } else {
                this.set('options', this.items);
            }
        },

        selectValue(value) {
            let selected = JSON.parse(JSON.stringify(this.selected));

            if(this.multi) {
                if(selected[value] !== undefined) {
                    delete selected[value];
                } else {
                    selected[value] = true;
                }
            } else {
                selected = {};
                selected[value] = true;
            }

            this.set('selected', selected);

            if(this.changeValue) {
                this.changeValue(this.value, this.name);
            }

            this.toggleSelect(false);
        },

        focusIn() {
            this.set('opened', true);
        },

        focusOut() {
            this.set('opened', false);
        }
    }
});

