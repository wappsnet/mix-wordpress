import Component from '@ember/component';
import layout from 'mix-typo/templates/components/mix-field/mix-range';
import { computed } from '@ember/object';
import { htmlSafe } from '@ember/string';

export default Component.extend({
    layout,

    classNames: [
        'mix-range'
    ],

    min: 0,

    max: 0,

    step: 0,

    value: 0,

    name: null,

    numberInput: false,

    filled: computed('value', 'max', function () {
        if(this.value > 0) {
            let filledPercent = this.value / this.max * 100;
            let filledStyles = `width: ${filledPercent}%`;
            return htmlSafe(filledStyles);
        }
    }),

    actions: {
        changeValue(value) {
            if(this.changeValue) {
                this.changeValue(value, this.name);
            }
        }
    }
});