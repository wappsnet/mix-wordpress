import Component from '@ember/component';
import layout from 'mix-typo/templates/components/mix-field/mix-input';
import { computed } from '@ember/object';

export default Component.extend({
    layout,

    classNames: [
        'mix-input'
    ],

    classNameBindings: [
        'iconClass'
    ],

    placeholder: 'Placeholder',

    value: null,

    iconName: null,

    iconPlace: null,

    name: null,

    type: 'text',

    observeValue: null,

    changeValue: null,

    iconClass: computed('icon.{place}', function () {
        if(this.icon && this.icon.place) {
            return 'mix-input-icon-' + this.icon.place;
        }
    }),

    actions: {
        changeValue(value) {
            if(this.changeValue) {
                this.changeValue(value, this.name);
            }
        }
    }
});