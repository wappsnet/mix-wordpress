import Component from '@ember/component';
import layout from 'mix-typo/templates/components/mix-field/mix-label';

export default Component.extend({
    layout,

    classNames: [
        'mix-field-label'
    ],
});
