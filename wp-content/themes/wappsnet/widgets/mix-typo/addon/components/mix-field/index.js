import Component from '@ember/component';
import layout from 'mix-typo/templates/components/mix-field/index';
import { computed } from '@ember/object';

export default Component.extend({
    layout,

    classNames: [
        'mix-field'
    ],

    classNameBindings: [
        'displayClass',
        'stateClass',
        'sizeClass',
    ],

    display: 'column',

    displayClass: computed('display', function () {
        return 'mix-field-' + this.display;
    }),

    state: 'inactive',

    stateClass: computed('state', function () {
        return 'mix-field-state-' + this.state;
    }),

    size: 'default',

    sizeClass: computed('size', function () {
        return 'mix-field-size-' + this.size;
    })
});
