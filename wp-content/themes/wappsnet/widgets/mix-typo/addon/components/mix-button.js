import Component from '@ember/component';
import layout from 'mix-typo/templates/components/mix-button';
import { computed } from '@ember/object';

export default Component.extend({
    layout,

    tagName: 'button',

    classNames: [
        'mix-btn'
    ],

    classNameBindings: [
        'sizeClass',
        'typeClass',
        'styleClass',
        'iconClass'
    ],

    size: 'medium',

    sizeClass: computed('size', function () {
        return 'btn-' + this.size;
    }),

    type: 'primary',

    typeClass: computed('type', function () {
        return 'btn-' + this.type;
    }),

    style: 'standard',

    styleClass: computed('style', function () {
        return 'btn-' + this.style;
    }),

    icon: null,

    iconClass: computed('icon', function () {
        if(this.icon) {
            return 'btn-icon-' + this.icon;
        }
    }),

    click() {
        if(this.clickBtn) {
            this.clickBtn()
        }
    }
});
