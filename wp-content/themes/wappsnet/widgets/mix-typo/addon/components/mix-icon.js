import Component from '@ember/component';
import layout from 'mix-typo/templates/components/mix-icon';
import { computed } from '@ember/object';

export default Component.extend({
  layout,

    tagName: 'svg',

    classNames: [
        'mix-icon'
    ],

    classNameBindings: [
        'name',
    ],

    size: 32,

    basePath: computed('size', function () {
        return 'assets/icons/icons-' + this.size + '.svg';
    }),
});
