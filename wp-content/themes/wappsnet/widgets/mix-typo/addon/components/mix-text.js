import Component from '@ember/component';
import layout from 'mix-typo/templates/components/mix-text';
import { computed } from '@ember/object';

export default Component.extend({
    layout,

    classNames: [
        "tp-text"
    ],

    classNameBindings: [
        'colorClass'
    ],

    color: 'dark',

    colorClass: computed('color', function () {
        return this.color + '-text';
    })
});

