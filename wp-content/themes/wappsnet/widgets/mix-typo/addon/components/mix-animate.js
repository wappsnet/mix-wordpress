import Component from '@ember/component';
import layout from 'mix-typo/templates/components/mix-animate';
import { computed } from '@ember/object';

export default Component.extend({
    layout,

    classNames: [
        'mix-animate'
    ],

    classNameBindings: [
        'typeClass',
        'directionClass',
        'durationClass',
    ],

    type: 'zoomIn',

    typeClass: computed('type', function () {
        if(this.type) {
            return 'animate-type-' + this.type;
        }
    }),

    directionClass: computed('direction', function () {
        if(this.direction) {
            return 'animate-direction-' + this.direction;
        }
    }),

    duration: 'default',

    durationClass: computed('duration', function () {
        if(this.duration) {
            return 'animate-duration-' + this.duration;
        }
    }),
});
