import Component from '@ember/component';
import layout from 'mix-typo/templates/components/mix-accordion';

export default Component.extend({
    layout,

    classNames: [
        'mix-accordion'
    ],

    expandable: true,

    didInsertElement() {
        this._super(...arguments);

        this.element.addEventListener('click', this._collapse = (e) => {
            if(e.target.classList.contains('mix-collapsible-header')) {
                if(e.target.parentNode.classList.contains('active')) {
                    e.target.parentNode.classList.remove('active');
                } else {
                    if(this.expandable === false) {
                        this.collapseAll();
                    }

                    e.target.parentNode.classList.add('active');
                }
            }
        });
    },

    collapseAll() {
        let collapses = this.element.querySelectorAll('.mix-collapsible');

        collapses.forEach((item) => {
            item.classList.remove('active');
        })
    }
});
