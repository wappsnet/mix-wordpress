import Component from '@ember/component';
import layout from 'mix-typo/templates/components/mix-chip';

export default Component.extend({
  layout,

  classNames: [
      'mix-chip'
  ]
});
