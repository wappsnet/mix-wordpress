import Component from '@ember/component';
import layout from 'mix-typo/templates/components/mix-tabs';
import { computed } from '@ember/object';

export default Component.extend({
    layout,

    classNames: [
        'mix-tabs'
    ],

    classNameBindings: [
        'sizeClass',
        'iconClass'
    ],

    size: 'large',

    sizeClass: computed('size', function() {
        return 'mix-tabs-' + this.size;
    }),

    activeIndex: 0,

    activeTab: computed('activeIndex', function() {
        return this.items[this.activeIndex];
    }),

    iconPlace: null,

    iconClass: computed('iconPlace', function() {
        if(this.iconPlace) {
            return 'mix-tabs-icon-' + this.iconPlace;
        }
    }),

    actions: {
        changeTab(index) {
            this.set('activeIndex', index);

            if(this.changeTab) {
                this.changeTab(this.activeTab);
            }
        }
    }
});