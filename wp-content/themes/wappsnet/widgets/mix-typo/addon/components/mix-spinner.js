import Component from '@ember/component';
import layout from 'mix-typo/templates/components/mix-spinner';
import { computed } from '@ember/object';

export default Component.extend({
    layout,

    classNames: [
        'mix-spinner'
    ],

    classNameBindings: [
        'sizeClass',
        'colorClass'
    ],

    size: 'responsive',

    sizeClass: computed('size', function() {
        return 'mix-spinner-' + this.size;
    }),

    color: 'grey',

    colorClass: computed('color', function() {
        return 'mix-spinner-' + this.color;
    })
});

