import Component from '@ember/component';
import layout from 'mix-typo/templates/components/mix-tooltip';
import { computed } from '@ember/object';
import { htmlSafe } from '@ember/string';
import { later, cancel } from '@ember/runloop';

export default Component.extend({
    layout,

    classNames: [
        'mix-tooltip-indicator'
    ],

    direction: 'top',

    position: null,

    wrapperId: 'tooltip-place',

    activate: 'hover',

    events: false,

    hideDuration: 300,

    styles: computed('position', 'direction', function () {
        if(this.position !== null) {
            let styles = '';

            switch (this.direction) {
                case 'top': {
                    styles += `left:${this.position.left + this.position.width / 2}px;`;
                    styles += `top:${this.position.top}px;`;

                    break;
                }
                case 'bottom': {
                    styles += `left:${this.position.left + this.position.width / 2}px;`;
                    styles += `top:${this.position.top + this.position.height}px;`;

                    break;
                }
                case 'left': {
                    styles += `left:${this.position.left}px;`;
                    styles += `top:${this.position.top + this.position.height/2}px;`;

                    break;
                }
                case 'right': {
                    styles += `left:${this.position.right}px;`;
                    styles += `top:${this.position.top + this.position.height/2}px;`;

                    break;
                }
            }

            return htmlSafe(styles);
        }
    }),

    init() {
        this._super(...arguments);

        this.initWrapper();
    },

    didInsertElement() {
        this._super(...arguments);

        this.initTooltip();
    },

    didReceiveAttrs() {
        this._super(...arguments);

        this.initTooltip();
    },


    didRender() {
        this._super(...arguments);

        this._toolHovered = false;

        if(this.events === true) {
            let toolTipNode = document.getElementById('tooltip-' + this.elementId);

            if (toolTipNode !== null) {
                toolTipNode.removeEventListener('mouseenter', this._toolMouseEnter);
                toolTipNode.removeEventListener('mouseleave', this._toolMouseLeave);

                toolTipNode.addEventListener('mouseenter', this._toolMouseEnter = () => {
                    this._toolHovered = true;

                    cancel(this._hideTimeoutId);

                    toolTipNode.addEventListener('mouseleave', this._toolMouseLeave = () => {
                        this.setPosition(null);
                    });
                });
            }
        }
    },

    willDestroyElement() {
        switch (this.activate) {
            case 'hover': {
                this.resetHover();
                break;
            }

            case 'click': {
                this.resetClick();
                break;
            }
        }

        this.resetToolTipHover();

        this._super(...arguments);
    },

    initTooltip() {
        if(this.element && this.element.parentNode !== null) {
            switch (this.activate) {
                case 'hover': {
                    this.resetClick();

                    this.element.parentNode.addEventListener('mouseenter', this._mouseEnter = (e) => {
                        this.setPosition(e.currentTarget.getBoundingClientRect());
                    });

                    this._hideTimeoutId = null;

                    this.element.parentNode.addEventListener('mouseleave', this._mouseOut = () => {
                        let hideDuration = 0;

                        if(this.events === true) {
                            hideDuration = this.hideDuration;
                        }

                        this._hideTimeoutId = later(() => {
                            if(this._toolHovered !== true) {
                                this.setPosition(null);
                            }
                        }, hideDuration);
                    });

                    break;
                }

                case 'click': {
                    this.resetHover();

                    this.element.parentNode.addEventListener('click', this._mouseEnter = (e) => {
                        e.stopPropagation();
                        this.setPosition(e.currentTarget.getBoundingClientRect());
                    });

                    document.addEventListener('click', this._mouseOut = () => {
                        this.setPosition(null);
                    });

                    break;
                }
            }
        }
    },

    setPosition(position) {
        if(position === null) {
            this.resetToolTipHover();
        }

        this.set('position', position);
    },

    resetToolTipHover() {
        cancel(this._hideTimeoutId);

        let toolTipNode = document.getElementById('tooltip-' + this.elementId);

        if (toolTipNode !== null) {
            toolTipNode.removeEventListener('mouseenter', this._toolMouseEnter);
            toolTipNode.removeEventListener('mouseleave', this._toolMouseLeave);
        }
    },

    resetHover() {
        if(!this.isDestroyed) {
            this.setPosition(null);
        }

        this.element.parentNode.removeEventListener('mouseenter', this._mouseEnter);
        this.element.parentNode.removeEventListener('mouseleave', this._mouseOut);
    },

    resetClick() {
        if(!this.isDestroyed) {
            this.setPosition(null);
        }

        this.element.parentNode.removeEventListener('click', this._mouseEnter);
        document.removeEventListener('mouseleave', this._mouseOut);
    },

    initWrapper() {
        let wrapperNode = document.querySelector('#' + this.wrapperId);

        if(wrapperNode === null) {
            wrapperNode = document.createElement('div');
            wrapperNode.id = this.wrapperId;

            document.body.appendChild(wrapperNode);
        }
    }
});

