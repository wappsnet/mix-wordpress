import Component from '@ember/component';
import { inject } from '@ember/service';

export default Component.extend({
    classNames: [
        'mix-form'
    ],

    routing: inject("-routing"),

    init() {
        this._super(...arguments);

        this.set('modules', [
            {
                label: 'Flights',
                value: 'flights'
            },
            {
                label: 'Hotels',
                value: 'hotels'
            }
        ])
    },

    actions: {
        preview() {
            console.log(this.routing);
            this.routing.router.transitionTo("preview");
        }
    }
});
