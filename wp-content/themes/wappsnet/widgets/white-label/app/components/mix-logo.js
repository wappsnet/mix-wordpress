import Component from '@ember/component';
import configs from 'white-label/config/environment';
import { computed } from '@ember/object';

export default Component.extend({
    classNames: [
        'mix-logo'
    ],

    mediaURL: configs.mediaURL,

    logoURL: computed('mediaURL', function () {
        return this.mediaURL + 'assets/images/logo.png';
    })
});
