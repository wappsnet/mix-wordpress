<?php
/**
 * Created by PhpStorm.
 * User: Администратор
 * Date: 17.06.2018
 * Time: 1:43
 */

namespace Wappsnet\Core;


class Blog {
	protected static $getCategoriesHtml = null;

	public static function getCategoriesHtml() {
		if(self::$getCategoriesHtml == null) {
			$optionsList = [
				'parent' => 0,
				'taxonomy' => 'archive',
				'type' => 'post'
			];

			$optionsList['currentId'] = get_queried_object_id();

			self::$getCategoriesHtml = Parser::listCategoriesHtml($optionsList);
		}

		return self::$getCategoriesHtml;
	}

	public static function getPostData($postId) {
		$postData = get_post($postId);

		//item media -------------------------------------
		$postMedia = self::getPostMedia($postId);
		//item media -------------------------------------

		//item category--------------------------------------
		$postCategory = self::getPostCategory($postId);
		//item category--------------------------------------

        //item author--------------------------------------
		$postAuthor = self::getPostAuthor($postData->post_author);
		//item author--------------------------------------


		$itemIfo = Array(
			'data' => $postData,
			'link' => get_permalink($postId),
			'media' => $postMedia,
			'author' => $postAuthor,
			'category' => $postCategory
		);

		return $itemIfo;
	}

	public static function getPostAuthor($authorId) {
		return get_user_by('id', $authorId);
	}

	public static function getPostMedia($itemId) {
		return array(
			'image' => get_the_post_thumbnail_url($itemId),
			'video' => get_field('video_link', $itemId)
		);
	}

	public static function getPostCategory($itemId) {
		$categoryObject = get_the_terms($itemId, 'archive');

		if(is_array($categoryObject)) {
			$categoryData = $categoryObject[0];
			return Array(
				'data' => $categoryData,
				'link' => get_permalink($categoryData->cat_ID)
			);
		}

		return false;
	}

	public static function getTermPosts($termData) {
		$arguments = array(
			'post_type' => 'post',
			'fields' => 'ids',
			'numberposts' => get_option('posts_per_page'),
			'tax_query' => array(
				array(
					'taxonomy' => $termData->taxonomy,
					'field' => 'slug',
					'terms' => $termData->slug,
				)
			)
		);

		return get_posts($arguments);
	}
}